
ci.png
size: 512,256
format: RGBA8888
filter: Linear,Linear
repeat: none
ci01
  rotate: false
  xy: 450, 172
  size: 59, 68
  orig: 59, 68
  offset: 0, 0
  index: -1
ci02
  rotate: false
  xy: 450, 130
  size: 32, 40
  orig: 32, 40
  offset: 0, 0
  index: -1
ci03
  rotate: false
  xy: 2, 2
  size: 238, 238
  orig: 238, 238
  offset: 0, 0
  index: -1
ci04
  rotate: false
  xy: 242, 34
  size: 206, 206
  orig: 206, 206
  offset: 0, 0
  index: -1
