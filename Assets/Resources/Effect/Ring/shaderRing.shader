﻿Shader "Unlit/shaderRing"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color( "Color" , Color ) = (0,1,1) 
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha  
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed3 _Color;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed fDis =  (i.uv.x - 0.5 ) * (i.uv.x - 0.5 ) + (i.uv.y - 0.5 ) *(i.uv.y - 0.5 );
				fixed fAlpha = 1;
 				if ( fDis < 0.25 )
				{
					fAlpha =  ( fDis - 0.15 ) / 0.25;
                }
				else
				{
					discard;
				}
				return fixed4( _Color, fAlpha );  
			}
			ENDCG
		}
	}
}
