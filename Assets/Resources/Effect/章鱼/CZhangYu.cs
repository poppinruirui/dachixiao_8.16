﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CZhangYu : MonoBehaviour {

    public float m_fZhaYanJianGe = 5f;
    public CFrameAnimationEffect m_faZhaYan;
    public SpriteRenderer _srBody;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        ZhaYanLoop();
    }

    float m_fZhaYanTimeElapse = 0f;
    void ZhaYanLoop()
    {
        m_fZhaYanTimeElapse += Time.deltaTime;
        if (m_fZhaYanTimeElapse < m_fZhaYanJianGe )
        {
            return;
        }
        m_fZhaYanTimeElapse = 0;
        m_faZhaYan.BeginPlay(false);
    }

    public void SetColor( Color color )
    {
        _srBody.color = color;
    }
}
