﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSpore : MonoBehaviour
{
    public enum eEjectMode
    {
        none,
        uniform  ,            // 匀速运动
        accelerated,          // 加速运动
    };

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempSize = new Vector3();

    int m_nPlayerId;
    uint m_uSporeId;
    float m_fVolume;
    float m_fRadius;

    Vector3 m_vDir = new Vector3();
    Vector2 m_vSpeed = new Vector2();
	Vector2 m_vInitSpeed = new Vector2();
    Vector2 m_vAcclerate = new Vector2();
    bool m_bFinishedX = false;
    bool m_bFinishedY = false;

    //   Vector3 m_vDestPos = new Vector3();
    float m_fTotalEjectDistance = 0f;
    eEjectMode m_eEjectingStatus =  eEjectMode.none;

    Ball m_Ball = null;

    public CircleCollider2D _Collider;
    public SpriteRenderer _srMain;

    public GameObject m_goContainer;

    // Use this for initialization
    void Start()
    {
        
    }

    // [to youhua]销毁的东西一定要把actvive设为false，不能让它存在于这个世上空转
    void Update()
    {

    }

    public void SetSprite( Sprite spr )
    {
        _srMain.sprite = spr;
    }

    private void FixedUpdate()
    {
        Ejecting_BySpeed();
    }

    public void ResetAll()
    {
        m_Ball = null;
        m_eEjectingStatus =  eEjectMode.none;
        SetDead(false);
        m_vSpeed.x = 0f;
        m_vSpeed.y = 0f;
        m_vAcclerate.x = 0;
        m_vAcclerate.y = 0;
        m_vDir.x = 0;
        m_vDir.y = 0;
        m_bFinishedX = false;
        m_bFinishedY = false;

        m_bIsEjecting = false;
        m_bAccelerating = false;
            
    }

    public void SetColor(string szColor)
    {

    }

    public void SetVolume(float fVolume)
    {
        m_fVolume = fVolume;
        float fSize = CyberTreeMath.Volume2Scale(m_fVolume);

        if ( fSize > CSkillSystem.s_Instance.m_SpitSporeParam.fSporeMaxShowRadius )
        {
            fSize = CSkillSystem.s_Instance.m_SpitSporeParam.fSporeMaxShowRadius;
        }

        m_fRadius = CyberTreeMath.Scale2Radius(fSize);

        SetSize(fSize);
        
    }

    public float GetHuanTingDistance()
    {
        return CSkillSystem.s_Instance.m_SpitSporeParam.fHuanTing;
    }

    public float GetVolume()
    {
        return m_fVolume;
    }

    public float GetRadius()
    {
        return m_fRadius;
    }

    public void SetSize(float fSize)
    {
        vecTempSize.x = fSize;
        vecTempSize.y = fSize;
        vecTempSize.z = 1f;
        this.transform.localScale = vecTempSize;
    }

    public float GetSize()
    {
        return this.transform.localScale.x;
    }

    public void BeginEject(float s, float t, Vector2 dir, eEjectMode eStatus )
    {
        m_vDir = dir;
        float v0 = CyberTreeMath.GetV0( s, t );
        float a = CyberTreeMath.GetA( s, t ) ;
        m_eEjectingStatus = eStatus;
        m_vSpeed.x = dir.x * v0;
        m_vSpeed.y = dir.y * v0;
		m_vInitSpeed = m_vSpeed;
        m_vAcclerate.x = dir.x * a;
        m_vAcclerate.y = dir.y * a;
        m_bFinishedX = false;
        m_bFinishedY = false;
    }

    public float GetRunDistance(float fMultiple, float fRadius)
    {
        float A = CSkillSystem.s_Instance.m_SpitSporeParam.fSporeA;
        float X = CSkillSystem.s_Instance.m_SpitSporeParam.fSporeX;
        float B = CSkillSystem.s_Instance.m_SpitSporeParam.fSporeB;
        float C = CSkillSystem.s_Instance.m_SpitSporeParam.fSporeC;
        float fDis = fMultiple * CyberTreeMath.KaiGenHao( A * fRadius, X) + B + fRadius * C;

        return fDis;
    }

    bool m_bIsEjecting = false;
    bool m_bAccelerating = false;
    Vector2 m_vecA = new Vector2();
    Vector2 m_vecEjectStartPos = new Vector2();
    public void BeginEject_BySpeed( float fBaseDistance, float fSpeed, Vector2 dir, float fMotherRadius)
    {
        m_vecEjectStartPos = GetPos();
        m_vDir = dir;
        m_vSpeed.x = fSpeed * dir.x;
        m_vSpeed.y = fSpeed * dir.y;
        m_fTotalEjectDistance = fBaseDistance;
        float s = fMotherRadius/*GetHuanTingDistance()*/;
        float v0 = fSpeed;
        float a = CyberTreeMath.Get_A_By_V0_And_S(s, v0);
        float fMinTime = 0.5f;

        /*
        float a1 = CyberTreeMath.Get_A_By_V0_And_t(s, v0, fMinTime);
        if (Mathf.Abs(a) > Mathf.Abs(a1))
        {
            a = a1;
        }
        */

        m_vecA.x = a * m_vDir.x;
        m_vecA.y = a * m_vDir.y;

        m_bIsEjecting = true;
        m_bAccelerating = false;
    }


    public void BeginEject_OtherClient(Vector2 vecDir, Vector2 vecSpeed, Vector2 vecAccelerate, float fTotalDistance, Vector2 vecStartPos )
    {
        m_vecEjectStartPos = vecStartPos;

        m_vDir = vecDir;
        m_vSpeed = vecSpeed;
        m_vecA = vecAccelerate;

        m_fTotalEjectDistance = fTotalDistance;

        m_bIsEjecting = true;
        m_bAccelerating = false;
    }


    // [to youhua]吐孢子目前还没做视野裁剪的优化
    public bool IsEjecting()
    {
        return m_bIsEjecting;
      //  return m_eEjectingStatus > eEjectMode.none;
    }

    void Ejecting_BySpeed()
    {


        if (!m_bIsEjecting)
        {
            return;
        }


     

        float fDeltaX = 0f;
        float fDeltaY = 0f;
        vecTempPos = GetPos();

        float fCurDis = 0;
        if (!m_bAccelerating)
        {
            
            fCurDis = Vector2.Distance(vecTempPos, m_vecEjectStartPos);
            if ((fCurDis >= m_fTotalEjectDistance))
            {
                m_bAccelerating = true;
            }
        }


        if (m_bAccelerating)
        {
            m_vSpeed += Time.fixedDeltaTime * m_vecA;

            bool bCompletedX = false;
            bool bCompletedY = false;
            if (m_vecA.x > 0)
            {
                if (m_vSpeed.x >= 0)
                {
                    bCompletedX = true;
                }
            }
            else if (m_vecA.x < 0)
            {
                if (m_vSpeed.x <= 0)
                {
                    bCompletedX = true;
                }
            }
            else
            {
                bCompletedX = true;
            }

            if (m_vecA.y > 0)
            {
                if (m_vSpeed.y >= 0)
                {
                    bCompletedY = true;
                }
            }
            else if (m_vecA.y < 0)
            {
                if (m_vSpeed.y <= 0)
                {
                    bCompletedY = true;
                }
            }
            else
            {
                bCompletedY = true;
            }

            if (bCompletedX && bCompletedY)
            {
                EndEject_BySpeed();
                return;
            }
        } // end  if (m_bAccelerating)

        vecTempPos.x += Time.fixedDeltaTime * m_vSpeed.x;
        vecTempPos.y += Time.fixedDeltaTime * m_vSpeed.y;

        SetPos(vecTempPos);
    }

    void EndEject_BySpeed()
    {
        m_bIsEjecting = false;

    }

    // 减速运动
    void Ejecting_Accelerated()
    {
        if (!IsEjecting())
        {
            return;
        }

        vecTempPos = GetPos();


        if (m_vDir.x > 0)
        {
            if ( m_vSpeed.x > 0 )
            {
                vecTempPos.x += m_vSpeed.x * Time.fixedDeltaTime;
            }
            else
            {
                m_bFinishedX = true;
            }
        }
        else if (m_vDir.x < 0)
        {
            if (m_vSpeed.x < 0)
            {
                vecTempPos.x += m_vSpeed.x * Time.fixedDeltaTime;
            }
            else
            {
                m_bFinishedX = true;
            }
        }
        else
        {
            m_bFinishedX = true;
        }

        if (m_vDir.y > 0)
        {
            if (m_vSpeed.y > 0)
            {
                vecTempPos.y += m_vSpeed.y * Time.fixedDeltaTime;
            }
            else
            {
                m_bFinishedY = true;
            }
        }
        else if (m_vDir.y < 0)
        {
            if (m_vSpeed.y < 0)
            {
                vecTempPos.y += m_vSpeed.y * Time.fixedDeltaTime;
            }
            else
            {
                m_bFinishedY = true;
            }
        }
        else
        {
            m_bFinishedY = true;
        }

        if (m_bFinishedX && m_bFinishedY)
        {
            EndEject();
            return;
        }

        SetPos(vecTempPos);
    }

    /*
    void Ejecting()
    {
        if (!IsEjecting())
        {
            return;
        }


        bool bXFinished = false;
        bool bYFinished = false;
        if (m_vSpeed.x == 0 || (m_vSpeed.x > 0 && GetPos().x >= m_vDestPos.x) || (m_vSpeed.x < 0 && GetPos().x <= m_vDestPos.x))
        {
            bXFinished = true;
        }

        if (m_vSpeed.y == 0 || (m_vSpeed.y > 0 && GetPos().y >= m_vDestPos.y) || (m_vSpeed.y < 0 && GetPos().y <= m_vDestPos.y))
        {
            bYFinished = true;
        }

        if (bXFinished && bYFinished)
        {
            EndEject();
        }

        vecTempPos = GetPos();

        if (!bXFinished)
        {
            vecTempPos.x += m_vSpeed.x * Time.fixedDeltaTime;
        }

        if (!bYFinished)
        {
            vecTempPos.y += m_vSpeed.y * Time.fixedDeltaTime;
        }

        SetPos(vecTempPos);
    }
    */

    public void EndEject()
    {
        m_eEjectingStatus =  eEjectMode.none;
        EndEject_BySpeed();
    }

    public void SetSporeId(uint nSporeId)
    {
        m_uSporeId = nSporeId;
    }

    public uint GetSporeId()
    {
        return m_uSporeId;
    }

    public void SetPlayerId(int nPlayerId)
    {
        m_nPlayerId = nPlayerId;
    }

    public void SetColor( Color color )
    {
        _srMain.color = color;
    }

    public string GetKey()
    {
        return GetPlayerId() + "_" + GetSporeId();
    }

    public int GetPlayerId()
    {
        return m_nPlayerId;
    }

    public void SetBall(Ball ball)
    {
        m_Ball = ball;
    }

    public Ball GetBall()
    {
        return m_Ball;
    }

    public float GetTotalEjectDistance()
    {
        return m_fTotalEjectDistance;
    }

    public Vector2 GetEjectStartPos()
    {
        return m_vecEjectStartPos;
    }

    /*
    public void SetDest(Vector2 dest)
    {
        m_vDestPos = dest;

        m_vSpeed = m_vDestPos - GetPos();
        m_vSpeed.Normalize();
        m_vDir = m_vSpeed;
        m_vSpeed *= CSkillSystem.s_Instance.m_SpitSporeParam.fEjectSpeed;
    }

    public Vector3 GetDest()
    {
        return m_vDestPos;
    }
    */

    public Vector3 GetPos()
    {
        return this.transform.position;
    }

    public void SetPos(Vector3 pos)
    {
        this.transform.position = pos;
    }

    bool m_bDead = false;
    public bool isDead()
    {
        return m_bDead;
    }

    public void SetDead(bool bDead)
    {
        m_bDead = bDead;
       // this.gameObject.SetActive(!m_bDead);
       m_goContainer.SetActive(!m_bDead);
        _Collider.enabled = !m_bDead;
    }

    public Vector3 GetDir()
    {
        return m_vDir;
    }

    public Vector2 GetSpeed()
    {
        return m_vSpeed;
    }

	public Vector2 GetInitSpeed()
	{
		return m_vInitSpeed;
	}

	public Vector2 GetAccelerate()
	{
        return m_vecA;
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        
        if ( !IsEjecting() )
        {
            return;
        }
        if (other.transform.gameObject.tag != "thorn")
        {
            return;
        }
        CMonster thorn = other.transform.gameObject.GetComponent<CMonster>();
        if (thorn == null)
        {
            return;
        }

        this.EndEject();

        if (m_Ball == null)
        {
            return;
        }
        if (!m_Ball._Player.IsMainPlayer())
        {
            return;
        }
        vecTempPos = thorn.GetPos() - this.GetPos();
        vecTempPos.Normalize();
        m_Ball._Player.PushThorn( this.GetRadius(), thorn.GetGuid(), thorn.GetPos(), vecTempPos, this.GetDir());
    }
}
