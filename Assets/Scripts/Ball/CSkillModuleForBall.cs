﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSkillModuleForBall : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public GameObject m_goYinLangChiXuteXiao;

    public Ball _ball;

    public GameObject _goEffectContainer;
    CCosmosEffect[] m_aryActiveEffect_QianYao = new CCosmosEffect[(int)CSkillSystem.eSkillId.total_num];

    CCosmosEffect[] m_aryActiveEffect_ChiXu = new CCosmosEffect[(int)CSkillSystem.eSkillId.total_num];
    GameObject[] m_aryNewActiveEffect_ChiXu = new GameObject[(int)CSkillSystem.eSkillId.total_num];

    CCosmosEffect[] m_aryActiveEffect_JieShu = new CCosmosEffect[(int)CSkillSystem.eSkillId.total_num];

    //short[] m_arySkillStatus = new short[(int)CSkillSystem.eSkillId.total_num];
    short m_nSkillStatus = 0;
    CSkillSystem.eSkillId m_eSkillId = CSkillSystem.eSkillId.r_sneak;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GetCurEffects(ref CCosmosEffect[] aryQianYao, ref CCosmosEffect[] aryChiXu)
    {
        aryQianYao = m_aryActiveEffect_QianYao;
        aryChiXu = m_aryActiveEffect_ChiXu;
    }

    public void ResetAllSkillStatus()
    {
        /*
        for ( int i = 0; i < m_arySkillStatus.Length; i++ )
        {
            m_arySkillStatus[i] = 0;
        }
        */
        m_nSkillStatus = 0;
        Clear();
    }

    public void SetCurSkillStatus(/*CSkillSystem.eSkillId eSkillId, */short nStatus )
    {
        //m_arySkillStatus[(int)eSkillId] = nStatus;
        m_nSkillStatus = nStatus;
    }

    
    public short GetCurSkillStatus(/*CSkillSystem.eSkillId eSkillId*/)
    {
        // return m_arySkillStatus[(int)eSkillId];
        return m_nSkillStatus;
    }

    public void UpdateSkillStatus(CSkillSystem.eSkillId eSkillId, short nSkillStatus)
    {
        m_eSkillId = eSkillId;

       if (m_nSkillStatus == nSkillStatus )
       {
            return;
       }

        if (m_nSkillStatus == 1 && nSkillStatus != 1 ) // 清除前摇特效
        {
            CCosmosEffect effect = m_aryActiveEffect_QianYao[(int)eSkillId];
            m_aryActiveEffect_QianYao[(int)eSkillId] = null;
            if (effect)
            {
                GameObject.Destroy(effect.gameObject);
            }
            else
            {
                Debug.Log("bug, 为啥前摇特效为空");
            }

			//CEffectManager.s_Instance.DeleteSkillEffect(effect, eSkillId, CEffectManager.eSkillEffectType.qianyao);
        }

        if (m_nSkillStatus == 2 && nSkillStatus != 2 ) // 清除持续特效
        {
            CCosmosEffect effect = m_aryActiveEffect_ChiXu[(int)eSkillId];
            m_aryActiveEffect_ChiXu[(int)eSkillId] = null;
            CEffectManager.s_Instance.DeleteSkillEffect(effect, eSkillId, CEffectManager.eSkillEffectType.chixu);

            if (eSkillId == CSkillSystem.eSkillId.r_sneak)
            {
                _ball.SetAlpha(1f);
            }
        }


        if (m_nSkillStatus == 3 && nSkillStatus == 0) // 清除结束特效
        {
            CCosmosEffect effect = m_aryActiveEffect_JieShu[(int)eSkillId];
            m_aryActiveEffect_JieShu[(int)eSkillId] = null;
            if (effect)
            {
                GameObject.Destroy(effect.gameObject);
            }
            else
            {
                Debug.Log( "bug, 为啥结束特效为空" );
            }
           // CEffectManager.s_Instance.DeleteSkillEffect(effect, eSkillId, CEffectManager.eSkillEffectType.qianyao); // 暂时就用前摇特效作结束特效
        }


        /*

        if (m_nSkillStatus == 3 )// 清除持续特效 
        {
            CCosmosEffect effect = m_aryActiveEffect_ChiXu[(int)eSkillId];
            m_aryActiveEffect_ChiXu[(int)eSkillId] = null;
			CEffectManager.s_Instance.DeleteSkillEffect(effect, eSkillId, CEffectManager.eSkillEffectType.chixu);

            if (eSkillId == CSkillSystem.eSkillId.r_sneak)
            {
                _ball.SetAlpha(1f);
            }
  
        }
        */
      
        

        if (nSkillStatus == 1)// 播放前摇特效
        {
            _ball.ChangeRoleFace( CRoleManager.eRoleFaceType.using_skill );


            CCosmosEffect effect = null;
			
                effect  = GameObject.Instantiate(CEffectManager.s_Instance._effectQianYao).GetComponent<CCosmosEffect>();//NewSkillEffect(eSkillId, CEffectManager.eSkillEffectType.qianyao);
                effect.SetColor( CSkillSystem.s_Instance.GetSkillColorById(eSkillId  ) /*CEffectManager.s_Instance.GetQianYaoEffectColorBySkillId(eSkillId)*/); 
				SetParent(effect, eSkillId);
				effect.SetLocalPos(Vector3.zero);
                effect.SetScale( CEffectManager.s_Instance.m_fQianYaoScale ) ;//CEffectManager.s_Instance.GetSkillEffectScale(eSkillId, CEffectManager.eSkillEffectType.qianyao));

         
			if ( eSkillId == CSkillSystem.eSkillId.t_become_thorn )
			{
                // poppin test

				CSkeletonAnimation skeleton_ani = (CSkeletonAnimation)effect;
				skeleton_ani.PlayAnimation (0, "ani04", 1, false);
                // mpb = new MaterialPropertyBlock();
                // MaterialPropertyBlock mpb = new MaterialPropertyBlock(); ;// GetMPB(Color.red);
                // mpb.SetColor("_FillColor", Color.red);
                //skeleton_ani.gameObject.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.red);
   
            }
           
     
			
            	CFrameAnimationEffect frame_ani_effect = (CFrameAnimationEffect)effect;
				frame_ani_effect.BeginPlay(false);
		
            
            m_aryActiveEffect_QianYao[(int)eSkillId] = effect;
        }

        if (nSkillStatus == 2) // 播放持续特效
        {
            _ball.ChangeRoleFace(CRoleManager.eRoleFaceType.using_skill);

            CCosmosEffect effect = CEffectManager.s_Instance.NewSkillEffect(eSkillId, CEffectManager.eSkillEffectType.chixu);
            GameObject goChiXuEffect = CEffectManager.s_Instance.NewSkillEffect_ChiXu( eSkillId );
            CCosmosPatricleSys patricleSys = goChiXuEffect.GetComponent<CCosmosPatricleSys>();
            if ( patricleSys )
            {
                patricleSys._ball = _ball;
            }
            CCyberTreeEffect cbEffect = goChiXuEffect.GetComponent<CCyberTreeEffect>();
            if ( cbEffect && cbEffect._cosmosPS )
            {
                cbEffect._cosmosPS._ball = _ball;
            }


            goChiXuEffect.transform.SetParent( _goEffectContainer.transform );
            goChiXuEffect.transform.localPosition = Vector3.zero;
          //  float fScale = CEffectManager.s_Instance.GetSkillEffectScale(eSkillId, CEffectManager.eSkillEffectType.chixu);
            vecTempScale.x = 1;
            vecTempScale.y = 1;
            vecTempScale.z = 1;
            goChiXuEffect.transform.localScale = vecTempScale;
                                                     /*
            SetParent(effect, eSkillId);
            effect.SetLocalPos( Vector3.zero );
            if (eSkillId == CSkillSystem.eSkillId.p_gold)
            {
                vecTempPos.x = -0.05f;
                vecTempPos.y = 0f;
                vecTempPos.z = 0f;
                effect.SetLocalPos(vecTempPos);
            }
            effect.SetScale( CEffectManager.s_Instance.GetSkillEffectScale(eSkillId, CEffectManager.eSkillEffectType.chixu) );
            */


			if (eSkillId == CSkillSystem.eSkillId.t_become_thorn) {
				CSkeletonAnimation skeleton_ani = (CSkeletonAnimation)effect;
				skeleton_ani.PlayAnimation (0, "ani04_1", 0.2f, true);
			} else if (eSkillId == CSkillSystem.eSkillId.i_merge) {
				effect.BeginPlayRotation (50f);
			}
            else if (eSkillId == CSkillSystem.eSkillId.r_sneak)
            {
               // _ball.SetAlpha(0.3f);
            }
            else if (eSkillId == CSkillSystem.eSkillId.p_gold)
            {
         //       m_goYinLangChiXuteXiao.SetActive( true );
              
            }
            else if (eSkillId == CSkillSystem.eSkillId.y_annihilate)
            {
                
            }
            /*
            else if (eSkillId == CSkillSystem.eSkillId.o_fenzy)
            {
                _ball._containerRoleBall.SetActive(true);
            }
            */
            else
			{
                /*
				CFrameAnimationEffect frame_ani_effect = (CFrameAnimationEffect)effect;
				frame_ani_effect.BeginPlay (true);
                */
			}
            m_aryActiveEffect_ChiXu[(int)eSkillId] = effect;
            goChiXuEffect.name = "worinima_111";
            m_aryNewActiveEffect_ChiXu[(int)eSkillId] = goChiXuEffect;
	
        }

        // poppin test “播放结束特效”阶段
        if (nSkillStatus == 3)// 
        {
            _ball.ChangeRoleFace(CRoleManager.eRoleFaceType.common);

            CCosmosEffect effect = null;
            if (eSkillId == CSkillSystem.eSkillId.i_merge)
            {

            }
            else if (eSkillId == CSkillSystem.eSkillId.p_gold)
            {

          
                    _ball._TriggerGold.enabled = false;
              
            }
      
                // 现在用统一的前摇特效 right here
                effect = GameObject.Instantiate( CEffectManager.s_Instance._effectQianYao ).GetComponent<CCosmosEffect>() ;//NewSkillEffect(eSkillId, CEffectManager.eSkillEffectType.qianyao);
                SetParent(effect, eSkillId);
                effect.SetLocalPos(Vector3.zero);
            effect.SetColor(CSkillSystem.s_Instance.GetSkillColorById(eSkillId) );
                effect.SetScale(CEffectManager.s_Instance.m_fQianYaoScale);//(CEffectManager.s_Instance.GetSkillEffectScale(eSkillId, CEffectManager.eSkillEffectType.qianyao));
         


                CFrameAnimationEffect frame_ani_effect = (CFrameAnimationEffect)effect;
                frame_ani_effect.SetReverse(true);
                frame_ani_effect.BeginPlay(false);


            m_aryActiveEffect_JieShu[(int)eSkillId] = effect;


            // 移除持续特效
            CEffectManager.s_Instance.DeleteSkillEffect_ChiXu( m_aryNewActiveEffect_ChiXu[(int)eSkillId] );
        } // end 播放结束特效


        //// 以上为技能特效 

        //// 以下为一些逻辑功能
        switch(eSkillId)
        {
            case CSkillSystem.eSkillId.r_sneak:
                {

                        if ( nSkillStatus == 0)
                        {
							_ball.SetColliderDustEnable (true);
                        }
                        else
                        {
                          

                            _ball.SetColliderDustEnable(false);
                        }

                }
                break;


        } // end switch
          //// end 逻辑功能


        // 设置最新的状态
        SetCurSkillStatus(nSkillStatus);
    }

    public void SetParent( CCosmosEffect efffect, CSkillSystem.eSkillId eSkillId)
    {
        efffect.transform.SetParent(_goEffectContainer.transform);
    }

    public void Clear()
    {
        m_nSkillStatus = 0;
      
      //  m_goYinLangChiXuteXiao.SetActive( false );

        //// 扩张技能的特效
        _ball._goEffectContainer_QianYao.gameObject.SetActive(false);
        _ball._effectUnfoldQianYao.gameObject.SetActive(false);
        ////

        // 新版持续特效
        for (int i = 0; i < m_aryNewActiveEffect_ChiXu.Length; i++)
        {
            if ( i == (int)CSkillSystem.eSkillId.u_magicshield )
            {
                int nShit = 0;
            }

            GameObject effect = m_aryNewActiveEffect_ChiXu[i];
            if (effect == null)
            {
                continue;
            }
            m_aryNewActiveEffect_ChiXu[i] = null;
            CEffectManager.s_Instance.DeleteSkillEffect_ChiXu(effect);
        }


        for (int i = 0; i < m_aryActiveEffect_QianYao.Length; i++)
        {
            CCosmosEffect effect = m_aryActiveEffect_QianYao[i];
            if (effect == null)
            {
                continue;
            }
            m_aryActiveEffect_QianYao[i] = null;
            CEffectManager.s_Instance.DeleteSkillEffect(effect, (CSkillSystem.eSkillId)i, CEffectManager.eSkillEffectType.qianyao);
        }


        for (int i = 0; i < m_aryActiveEffect_JieShu.Length; i++)
        {
            CCosmosEffect effect = m_aryActiveEffect_JieShu[i];
            if (effect == null)
            {
                continue;
            }
            m_aryActiveEffect_JieShu[i] = null;
            CEffectManager.s_Instance.DeleteSkillEffect(effect, (CSkillSystem.eSkillId)i, CEffectManager.eSkillEffectType.qianyao);
        }

        for (int i = 0; i < m_aryActiveEffect_ChiXu.Length; i++)
        {
            CCosmosEffect effect = m_aryActiveEffect_ChiXu[i];
            if (effect == null)
            {
                continue;
            }
            m_aryActiveEffect_ChiXu[i] = null;
            CEffectManager.s_Instance.DeleteSkillEffect(effect, (CSkillSystem.eSkillId)i, CEffectManager.eSkillEffectType.chixu);
        }
    }

    public void SetYinLangBaoFaPercent(  float fPercent )
    {
        float fRadius = CSkillSystem.s_Instance.m_fYinLangChiXuRadius +  fPercent * ( CSkillSystem.s_Instance.m_fYinLangBaoFaRadius - CSkillSystem.s_Instance.m_fYinLangChiXuRadius );
        _ball._TriggerGold.radius = fRadius;
    }

    public void YinLangBaoFa()
    {
        GameObject effect = CEffectManager.s_Instance.NewYinLangBaoFaEffect();
        effect.transform.SetParent( _ball._containerEffect.transform );
        vecTempPos.x = 0.75f;
        vecTempPos.y = 0.75f;
        vecTempPos.z = 0.75f;
        effect.transform.localScale = vecTempPos;

        vecTempPos.x = 0f;
        vecTempPos.y = 0f;
        vecTempPos.z = 0f;
        effect.transform.localPosition = vecTempPos;

        _ball._TriggerGold.enabled = true;
        _ball._TriggerGold.radius = 0;
        _ball._rigid.bodyType = RigidbodyType2D.Kinematic;

    }

    public void EndYinLangBaoFa()
    {
        _ball._TriggerGold.radius = CSkillSystem.s_Instance.m_fYinLangChiXuRadius;
        _ball._rigid.bodyType = RigidbodyType2D.Dynamic;

    }

} // end class
