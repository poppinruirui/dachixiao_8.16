﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CWave : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();
    static Color colorTemp = new Color();

    int m_nIndex = 0;

    public SpriteRenderer _srMain;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetScale( float fScale )
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    public void SetPos( Vector3 pos )
    {
        this.transform.localPosition = pos;
    }

    public void SetIndex( int nIndex )
    {
        m_nIndex = nIndex;
    }

    public int GetIndex()
    {
        return m_nIndex;
    }

    public void SetAlpha( float fAlpha )
    {
        colorTemp = _srMain.color;
        colorTemp.a = fAlpha;
        _srMain.color = colorTemp;
    }
}
