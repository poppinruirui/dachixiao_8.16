﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CGameOverManager : MonoBehaviour
{

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public static CGameOverManager s_Instance = null;
    public GameObject _goPanel;

    public float m_fCountingBorderMinFill = 1f;
    public float m_fCountingBorderMaxFill = 3f;
    float m_fFillSpeed = 0f;


    public float m_fRankTextMaxScale = 3f;
    public float m_fRankTextMinScale = 0.5f;
    public float m_fRankTextAnimationTime = 1f;
    bool m_bRankTextAnimation = false;
    float m_fRankTextAnimationScaleV = 0f;

    public GameObject _goTextRank;
    public GameObject _goTextRank_CanYing;
    public GameObject _goTextTitle;
    public GameObject _goTextTitle_CanYing;

    public float m_fOneTuoWeiTotalTime = 0.3f;
    public float m_fOneTuoWeiTotalTimeMiddle = 0.3f;

    public Text _txtRank; // 我的本场排名
    public Text _txtRank_CanYing;

    public GameObject _containerRank;

    public static bool s_bNowGameOvering = false;

    const float c_fGameOverPanelMinTime = 3f;
    float m_fTimeElpase = 0f;

    public CBiShuaTuoWei[] m_aryTuoWei;

    public GameObject _panelCounting;
    public Text _txtCounting;
    public Image _imgCountingBorder;

    public enum eGameOverSubPanelType
    {
        e_counting, // 倒计时
        e_tuowei,   // 拖尾
        e_rank,     // 显示排名
    };

    public GameObject[] m_arySubPanels;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        float s = m_fRankTextMinScale - m_fRankTextMaxScale;
        m_fRankTextAnimationScaleV = s / m_fRankTextAnimationTime;

        m_fFillSpeed = (m_fCountingBorderMaxFill - m_fCountingBorderMinFill) / 0.5f;
    }

    private void FixedUpdate()
    {
        TuoWeiLoop();
        RankTextAnimationLoop();
        CountingBorderAnimationLoop();
    }

    // Update is called once per frame
    void Update()
    {

        if (!s_bNowGameOvering)
        {
            return;
        }

        TimeLoop();


        if (Input.GetMouseButtonDown(0) && m_fTimeElpase > c_fGameOverPanelMinTime)
        {
            s_bNowGameOvering = false;
            //StartCoroutine("LoadScene", "SelectCaster");
            CJIeSuanSystem.s_Instance.SetJieSuanPanelVisible(CJIeSuanSystem.eJieSuanType.e_gerenjiesuan, true);
            SetGameOverPanelVisible( false );

        }
    }

    //异步加载场景  
    IEnumerator LoadScene(string scene_name)
    {
        AccountData.s_Instance.asyncLoad = SceneManager.LoadSceneAsync(scene_name);

        while (!AccountData.s_Instance.asyncLoad.isDone)
        {
            yield return null;
        }


    }

    public void SetGameOverPanelVisible(bool bVisible)
    {
        _goPanel.SetActive(bVisible);

    }

    public void SetGameOverSubPanelVisible(eGameOverSubPanelType eType, bool bVisible)
    {
        m_arySubPanels[(int)eType].SetActive(bVisible);


    }
    public void SetRankInfo( int nRank )
    {
        _txtRank.text = nRank.ToString();
        _txtRank_CanYing.text = _txtRank.text;
    }

    public void BeginGameOver()
    {
        CAudioManager.s_Instance.StopMainBg(CAudioManager.eMainBg.zhandou);
        CAudioManager.s_Instance.PlayMainBg(CAudioManager.eMainBg.jieshu);

        m_fTimeElpase = 0;
        s_bNowGameOvering = true;

        SetGameOverPanelVisible(true);
        SetGameOverSubPanelVisible(eGameOverSubPanelType.e_counting, false);
        SetGameOverSubPanelVisible(eGameOverSubPanelType.e_tuowei, true);
        BeginTuoWei();
        CPaiHangBang_Mobile.s_Instance.RefreshPaiHangbangPlayerInfo(true);
      
     

        UIManager_New.s_Instance.HideSomeUiWhenGameOver();
        CVisionManaer.s_Instance.SetBlurEnabled(true);
    }

    public void EndGameOver()
    {

    }

    void TimeLoop()
    {
        m_fTimeElpase += Time.deltaTime;
    }

    int m_nTuoWeiStatus = 0; // 0 - None  1 - 第一条拖尾  2 - 第二条拖尾  3 - 第三条拖尾  4 - 结束
    float m_fTuoWeiTimeElapse = 0f;
    void BeginTuoWei()
    {
        m_nTuoWeiStatus = 1;
        m_fTuoWeiTimeElapse = 0;
    }

    void TuoWeiLoop()
    {
        if (m_nTuoWeiStatus < 1 || m_nTuoWeiStatus > 3)
        {
            return;
        }

        int nIndex = m_nTuoWeiStatus - 1;
        CBiShuaTuoWei tuoWei = m_aryTuoWei[nIndex];
        m_fTuoWeiTimeElapse += Time.fixedDeltaTime;
        float fPercent = 0;
        if (m_nTuoWeiStatus == 2)
        {
            fPercent = m_fTuoWeiTimeElapse / m_fOneTuoWeiTotalTimeMiddle;
        }
        else
        {
            fPercent = m_fTuoWeiTimeElapse / m_fOneTuoWeiTotalTime;
        }
        tuoWei.SetMaskPercent(fPercent);
        if (fPercent >= 1f)
        {
            m_nTuoWeiStatus++;
            m_fTuoWeiTimeElapse = 0;
        }

        if (m_nTuoWeiStatus > 3)
        {
            EndTuoWei();
        }
    }

    void EndTuoWei()
    {
        BeginRankTextAnimation();
    }

    void BeginRankTextAnimation()
    {
        m_bRankTextAnimation = true;

        SetGameOverSubPanelVisible(eGameOverSubPanelType.e_rank, true);

        vecTempScale.x = m_fRankTextMaxScale;
        vecTempScale.y = m_fRankTextMaxScale;
        vecTempScale.z = 1;

        _goTextRank.transform.localScale = vecTempScale;
        _goTextRank_CanYing.transform.localScale = vecTempScale;
        _goTextTitle.transform.localScale = vecTempScale;
        _goTextTitle_CanYing.transform.localScale = vecTempScale;

        m_bTitleAnimationStarted = true;

        m_nFrameCount = 0;
    }

    bool m_bRankFirstFrame = true;
    bool m_bTitleFirstFrame = true;
    int m_nFrameCount = 0;
    bool m_bRankAnimationStarted = false;
    bool m_bTitleAnimationStarted = false;
    void RankTextAnimationLoop()
    {
        if (!m_bRankTextAnimation)
        {
            return;
        }

        float fAmount = m_fRankTextAnimationScaleV * Time.fixedDeltaTime;
        vecTempScale = _goTextTitle.transform.localScale;
        vecTempScale.x += fAmount;
        vecTempScale.y += fAmount;
        if (vecTempScale.x < m_fRankTextMinScale)
        {
            vecTempScale.x = m_fRankTextMinScale;
        }
        if (vecTempScale.y < m_fRankTextMinScale)
        {
            vecTempScale.y = m_fRankTextMinScale;
        }
        _goTextTitle.transform.localScale = vecTempScale;

        if (m_nFrameCount >= 3)
        {
            vecTempScale = _goTextTitle_CanYing.transform.localScale;
            vecTempScale.x += fAmount;
            vecTempScale.y += fAmount;
            if (vecTempScale.x < m_fRankTextMinScale || vecTempScale.y < m_fRankTextMinScale)
            {
                _goTextTitle_CanYing.SetActive(false);
            }
            else
            {
                _goTextTitle_CanYing.SetActive(true);
            }

            _goTextTitle_CanYing.transform.localScale = vecTempScale;
        }

        if (m_nFrameCount >= 15)
        {
            _goTextRank.SetActive(true);
            vecTempScale = _goTextRank.transform.localScale;
            vecTempScale.x += fAmount;
            vecTempScale.y += fAmount;
            if (vecTempScale.x < m_fRankTextMinScale)
            {
                vecTempScale.x = m_fRankTextMinScale;
            }
            if (vecTempScale.y < m_fRankTextMinScale)
            {
                vecTempScale.y = m_fRankTextMinScale;
            }
            _goTextRank.transform.localScale = vecTempScale;


            if (m_nFrameCount >= 18)
            {

                vecTempScale = _goTextRank_CanYing.transform.localScale;
                vecTempScale.x += fAmount;
                vecTempScale.y += fAmount;
                if (vecTempScale.x < m_fRankTextMinScale || vecTempScale.y < m_fRankTextMinScale)
                {
                    _goTextRank_CanYing.SetActive(false);
                }
                else
                {
                    _goTextRank_CanYing.SetActive(true);

                }

                _goTextRank_CanYing.transform.localScale = vecTempScale;
            }
        }

        /*
       
        vecTempScale = m_arySubPanels[(int)eGameOverSubPanelType.e_rank].transform.localScale;
        vecTempScale.x += fAmount;
        vecTempScale.y += fAmount;
        m_arySubPanels[(int)eGameOverSubPanelType.e_rank].transform.localScale = vecTempScale;
        if ( vecTempScale.x <= m_fRankTextMinScale )
        {
            m_bRankTextAnimation = false;
        }
        */

        m_nFrameCount++;
    }

    public void SetCountingPanelVisible(bool bVisible)
    {
        _panelCounting.SetActive(bVisible);
    }

    public void SetLeftTime(string szLeftTime)
    {
        _txtCounting.text = szLeftTime;

        BeginCountingBorderAnimation();
    }

    float m_fCurFillAmount = 0f;
    public void BeginCountingBorderAnimation()
    {
        if (m_nCountingBorderStatus > 0)
        {
            return;
        }

        m_fCurFillAmount = m_fCountingBorderMaxFill;
        SetCountingBorderFill(m_fCurFillAmount);

        m_nCountingBorderStatus = 2;
    }

    int m_nCountingBorderStatus = 0;
    void CountingBorderAnimationLoop()
    {
        if (m_nCountingBorderStatus == 0)
        {
            return;
        }

        if (m_nCountingBorderStatus == 1)
        {
            m_fCurFillAmount += m_fFillSpeed * Time.fixedDeltaTime;
            if (m_fCurFillAmount >= m_fCountingBorderMaxFill)
            {
                m_fCurFillAmount = m_fCountingBorderMaxFill;
                m_nCountingBorderStatus = 2;
            }
        }
        else if (m_nCountingBorderStatus == 2)
        {
            m_fCurFillAmount -= m_fFillSpeed * Time.fixedDeltaTime;
            if (m_fCurFillAmount <= m_fCountingBorderMinFill)
            {
                m_fCurFillAmount = m_fCountingBorderMinFill;
                m_nCountingBorderStatus = 1;
            }
        }

        SetCountingBorderFill(m_fCurFillAmount);

    }

    void SetCountingBorderFill(float fFillAmount)
    {
        _imgCountingBorder.material.SetFloat("_FillAmount", fFillAmount);
    }

    public void SetCountingBorderColor(Color color)
    {
        _imgCountingBorder.color = color;

    }

} // end class
