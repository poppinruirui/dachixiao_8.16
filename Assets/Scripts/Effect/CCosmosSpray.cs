﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCosmosSpray : MonoBehaviour {

    public Ball _ball;
    public GameObject _goMyContainer;
    public ParticleSystem _particleSystem;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		

        if ( _goMyContainer && _ball )
        {
            float fAngle = CyberTreeMath.Dir2Angle( _ball.GetDir().x, _ball.GetDir().y ) - 90;
            _goMyContainer.transform.localRotation = Quaternion.identity;
            _goMyContainer.transform.Rotate(0.0f, 0.0f, fAngle);

            if ( _ball.GetRealTimeSpeed() > 0 )
            {
                _particleSystem.enableEmission = true;
            }
            else
            {
                _particleSystem.enableEmission = false;
            }
        }


	}
}
