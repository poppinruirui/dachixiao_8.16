﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CXingKongManager : MonoBehaviour {

    static Vector3 vecTempScale = new Vector3();

    public float m_fCamera2ScaleXiShu = 0.188f;
    public float m_fparaallaxScale0XiShu = 0.25f;
    public float m_fparaallaxScale1XiShu = 0.1f;
    public float m_fparaallaxScale2XiShu = 0.2f;
                                            

    public BackgroundMovingController _xingkong;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        return;
        float fScale = Camera.main.orthographicSize * m_fCamera2ScaleXiShu;
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        _xingkong.transform.localScale = vecTempScale;

        _xingkong.backgroundLayers[0].parallaxScale = Camera.main.orthographicSize * m_fparaallaxScale0XiShu;
       _xingkong.backgroundLayers[1].parallaxScale = Camera.main.orthographicSize * m_fparaallaxScale1XiShu;
        _xingkong.backgroundLayers[2].parallaxScale = Camera.main.orthographicSize * m_fparaallaxScale2XiShu;
                
	}


}
