﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCosmosPatricleSys : MonoBehaviour {

    public Ball _ball;
    public ParticleSystem _particleSystem;
    public float m_fScaleXiShu = 1f;
    public GameObject _goMyContainer;

    private void Awake()
    {
      
    }

    // Use this for initialization
    void Start () {
        if (_ball && _particleSystem)
        {
            _particleSystem.startSize = _ball.GetSize() * m_fScaleXiShu;
        }
	}
	
	// Update is called once per frame
	void Update () {

        if (_ball && _particleSystem)
        {
            _particleSystem.startSize = _ball.GetSize() * m_fScaleXiShu;  
        }

        Rotate();
	}

    void Rotate()
    {
        if (_goMyContainer && _ball)
        {
            float fAngle = CyberTreeMath.Dir2Angle(_ball.GetDir().x, _ball.GetDir().y) - 90;
            _goMyContainer.transform.localRotation = Quaternion.identity;
            _goMyContainer.transform.Rotate(0.0f, 0.0f, fAngle);
            /*
            if ( _particleSystem && _ball.GetRealTimeSpeed() > 0)
            {
                _particleSystem.enableEmission = true;
            }
            else
            {
                _particleSystem.enableEmission = false;
            }
            */
        }

    }
}
