﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEffectManager : MonoBehaviour
{
    public CFrameAnimationEffect _effectQianYao;
    public float m_fQianYaoScale = 0.6f;

    public Color[] m_arySkillQianYaoColor;
 
    public GameObject[] m_aryChiXuEffectPrefabs;

	public static CEffectManager s_Instance = null;

    public GameObject m_goYinLangBaoFaEffect;

    /// <summary>
    /// prefab
    /// </summary>
    public GameObject[] m_arySkillEffect_QianYao;
    public GameObject[] m_arySkillEffect_ChiXu;
    /// end prefab

    public enum eSkillEffectType
    {
        qianyao,  // 前摇
        chixu,    // 持续
    }

    public float[] m_arySkillEffectScale_QianYao;
    public float[] m_arySkillEffectScale_ChiXu;

    Dictionary<CSkillSystem.eSkillId, List<CCosmosEffect>> m_dicRecycledSkillEffect_QianYao = new Dictionary<CSkillSystem.eSkillId, List<CCosmosEffect>>();
    Dictionary<CSkillSystem.eSkillId, List<CCosmosEffect>> m_dicRecycledSkillEffect_ChiXu = new Dictionary<CSkillSystem.eSkillId, List<CCosmosEffect>>();


    public enum eMainFightEffectType
    {
        zha_ci_pen_jian, // 炸刺喷溅
        chong_sheng,      // 重生
    };

    public CCosmosEffect[] m_aryMainFightEffects;
    public float[] m_aryMainFightEffects_FrameInterval;
    public float[] m_aryMainFightEffects_Scale;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Color GetQianYaoEffectColorBySkillId( CSkillSystem.eSkillId eId )
    {
        return m_arySkillQianYaoColor[(int)eId];
    }

    // [to youhua] 这里应该做回收池
    public void DeleteEffect( CCosmosEffect effect )
    {
        GameObject.Destroy( effect.gameObject );
    }

    public GameObject NewSkillEffect_ChiXu( CSkillSystem.eSkillId eSkillId )
    {
        return GameObject.Instantiate( m_aryChiXuEffectPrefabs[(int)eSkillId] );
    }

    public void DeleteSkillEffect_ChiXu( GameObject effect )
    {
        if ( effect == null )
        {
            return;
        }
      
        GameObject.Destroy( effect );
    }

    int m_nNewTimes = 0;
    public CCosmosEffect NewSkillEffect(CSkillSystem.eSkillId eSkillId, eSkillEffectType eType)
    {
		CCosmosEffect effect = null;
	
     
        GameObject[] aryPrefab = null;

        Dictionary<CSkillSystem.eSkillId, List<CCosmosEffect>> dic = null;
        if (eType == eSkillEffectType.qianyao)
        {
            dic = m_dicRecycledSkillEffect_QianYao;
            aryPrefab = m_arySkillEffect_QianYao;
        }
        else if (eType == eSkillEffectType.chixu)
        {
            dic = m_dicRecycledSkillEffect_ChiXu;
            aryPrefab = m_arySkillEffect_ChiXu;
        }
        else
        {
            return null;
        }

        if (eType == eSkillEffectType.chixu)
        {
            m_nNewTimes++;
        }
		effect = GameObject.Instantiate(aryPrefab[(int)eSkillId]).GetComponent<CCosmosEffect>();
 	    return effect;


        bool bExist = false;

        List<CCosmosEffect> lst = null;
        if (dic.TryGetValue(eSkillId, out lst))
        {
			if ( lst != null && lst.Count > 0)
            {
                effect = lst[0];
                effect.gameObject.SetActive(true);
                lst.RemoveAt(0);
                bExist = true;
            }
        }
        else
        {
            lst = new List<CCosmosEffect>();
            dic[eSkillId] = lst;
        }


        if (!bExist)
        {
            effect = GameObject.Instantiate(aryPrefab[(int)eSkillId]).GetComponent<CCosmosEffect>();
			//effect.gameObject.name = eSkillId + "_" + (m_nGuid++).ToString ();
        }

        return effect;
    }

	int m_nGuid = 0;
    int m_nDelTimes = 0;
    public void DeleteSkillEffect(CCosmosEffect effect, CSkillSystem.eSkillId eSkillId, eSkillEffectType eType)
    {
        if (effect == null)
        {
            return;
        }

       
        GameObject.Destroy ( effect.gameObject );


		return;

        Dictionary<CSkillSystem.eSkillId, List<CCosmosEffect>> dic = null;
        if (eType == eSkillEffectType.qianyao)
        {
            dic = m_dicRecycledSkillEffect_QianYao;
        }
        else if (eType == eSkillEffectType.chixu)
        {
            dic = m_dicRecycledSkillEffect_ChiXu;
        }

        List<CCosmosEffect> lst = null;
        if (dic.TryGetValue(eSkillId, out lst))
        {

        }
        else
        {
            lst = new List<CCosmosEffect>();
            dic[eSkillId] = lst;
        }
        effect.transform.SetParent( this.transform );
        effect.gameObject.SetActive( false );
        lst.Add(effect);
		dic [eSkillId] = lst;
    }

    public float GetSkillEffectScale( CSkillSystem.eSkillId eSkillId, CEffectManager.eSkillEffectType eType )
    {
        if ( eType == eSkillEffectType.qianyao )
        {
            return m_arySkillEffectScale_QianYao[(int)eSkillId];
        }
        else if (eType == eSkillEffectType.chixu)
        {
            return m_arySkillEffectScale_ChiXu[(int)eSkillId];
        }

        return 0;
    }

    public void RecycleEffectsFromBall( Ball ball )
    {
        CCosmosEffect[] aryQianYao = null;
        CCosmosEffect[] aryChiXu = null;
        ball.GetCurEffects( ref aryQianYao, ref aryChiXu);
        for ( int i = 0; i < aryQianYao.Length; i++ )
        {
            CCosmosEffect effect = aryQianYao[i];
            if ( effect == null )
            {
                continue;
            }
            aryQianYao[i] = null;
            DeleteSkillEffect(effect, (CSkillSystem.eSkillId)i, eSkillEffectType.qianyao);
        }

        for (int i = 0; i < aryChiXu.Length; i++)
        {
            CCosmosEffect effect = aryChiXu[i];
            if (effect == null)
            {
                continue;
            }
            aryChiXu[i] = null;
            DeleteSkillEffect(effect, (CSkillSystem.eSkillId)i, eSkillEffectType.chixu);
        }
    }


    ///////////// main-fight effects

    // [to youhua] 暂时没有做对象回收池
    Dictionary<eMainFightEffectType, List<CCosmosEffect>> m_dicMainFightEffefcts_Recycled = new Dictionary<eMainFightEffectType, List<CCosmosEffect>>();

    public float GetMainFightEffectsScale(eMainFightEffectType type)
    {
        return m_aryMainFightEffects_Scale[(int)type];
    }

    public float GetMainFightEffectsFrameInterval(eMainFightEffectType type)
    {
        return m_aryMainFightEffects_FrameInterval[(int)type];
    }


    public CCosmosEffect NewEffect_MainFight(eMainFightEffectType type )
    {
        CCosmosEffect effect = null;

        effect = GameObject.Instantiate( m_aryMainFightEffects[(int)type] ).GetComponent<CCosmosEffect>();
        
        return effect;
    }

    public void DeleteEffect_MainFight(eMainFightEffectType type, CCosmosEffect effect)
    {
        GameObject.Destroy(effect.gameObject);
    }

    ///////////// end  main-fight effects


    public GameObject NewYinLangBaoFaEffect()
    {
        return GameObject.Instantiate( m_goYinLangBaoFaEffect );
    }


}



