﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBling : MonoBehaviour {

    static Vector3 vecTempScale = new Vector3();

    public SpriteRenderer _srMain;

    public float m_fMaxScale = 0;
    public float m_fMinScale = 0;
    public float m_fSpeed = 0;

    protected int m_nCurDir = 1;

	// Use this for initialization
	void Start () {
       //m_fSpeed *= Time.fixedDeltaTime;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        BlingLoop();
    }

    protected void BlingLoop()
    {
        float fScale = this.transform.localScale.x;
        float fSpeed = m_fSpeed * Time.fixedDeltaTime;
        if ( m_nCurDir == 1 )
        {
            fScale += fSpeed;

            if ( fScale > m_fMaxScale )
            {
                m_nCurDir = -1;
            }
        }
        else if ( m_nCurDir == -1 )
        {
            fScale -= fSpeed;
            if (fScale < m_fMinScale)
            {
                m_nCurDir = 1;
            }
        }

        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;

    }
}
