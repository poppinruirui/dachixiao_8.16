﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class CCosmosEffect : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public CanvasGroup _canvasGroup;

    public Image _imgMain; // 用在UI上
    public SpriteRenderer _sprMain; // 用在非UI上

    public bool m_bUI = true;
	// 旋转特效
	public SpriteRenderer _srRotationPic;
    public Image _imgRotationPic;

	public bool m_bRotating = false;
    public float m_fRotatingSpeed = 0f;
	float m_fRotatingAngle = 0f;

    public bool m_bAlphaChange = false;
    public float m_fMaxAlpha = 1f;
    public float m_fMinAlpha = 0f;
    public float m_fAlphaChangeSpeed = 1f;

	public void ResetAll()
	{
		m_bRotating = false;
	}

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Rotating ();
        DoChangeAlpha();
	}

    public void SetPos( Vector3 pos )
    {
        this.transform.position = pos;
    }

    public void SetLocalPos( Vector3 pos )
    {
        this.transform.localPosition = pos;
    }

    public void SetColor( Color color )
    {
        if ( _imgMain )
        {
            _imgMain.color = color;
        }

        if ( _sprMain )
        {
            _sprMain.color = color;
        }
    }

    public void SetScale( float fScale )
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    public void Rotating( )
	{
		if (!m_bRotating) {
			return;
		}
		m_fRotatingAngle += Time.deltaTime * m_fRotatingSpeed;
		this.transform.localRotation = Quaternion.identity;
		this.transform.Rotate(0.0f, 0.0f, m_fRotatingAngle);


	}

    void DoChangeAlpha()
    {
        _canvasGroup.alpha += m_fAlphaChangeSpeed * Time.deltaTime;
        if ((m_fAlphaChangeSpeed < 0 && _canvasGroup.alpha <= m_fMinAlpha) ||
             (m_fAlphaChangeSpeed > 0 && _canvasGroup.alpha >= m_fMaxAlpha)
           )
        {
            m_fAlphaChangeSpeed = -m_fAlphaChangeSpeed;
        }


    }

  
	public void BeginPlayRotation( float fRotatingSpeed )
	{
		m_bRotating = true;
		m_fRotatingSpeed = fRotatingSpeed;
	}
}
