﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCosmosRotateEffect : MonoBehaviour {

    public float m_fRotateSpeed = 1;
    float m_fAngle = 0; 

	// Use this for initialization
	void Start () {
		

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        m_fAngle += m_fRotateSpeed * Time.fixedDeltaTime;
        this.transform.localRotation = Quaternion.identity;
        this.transform.Rotate(0.0f, 0.0f, m_fAngle);
    }
}
