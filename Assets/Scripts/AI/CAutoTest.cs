﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAutoTest : MonoBehaviour {

    public static CAutoTest s_Instance;

    Player m_Player;
    public static bool m_bAutoTesing = false;
    static Vector3 vecTempPos = new Vector3();
    static Vector2 vecTempDir = new Vector2();

    public static Vector2 s_vecMoveDir = new Vector2( 0.5f, 0.5f );

    Vector3 m_vecWorldCursorPos = new Vector3( 1000f, 0f, 0f );

    bool m_bAutotestInited = false;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {

     
	}

    public static bool IsAutoTesting()
    {
        return m_bAutoTesing;
    }


    float m_fChangeMoveDirTimeElapse = 0;
    float m_fUseSkillTimeElapse = 0;
	
	// Update is called once per frame
	void Update () {


 


		if ( !m_bAutoTesing)
        {
            m_bAutotestInited = true;
            return;
        }


        if ( !m_bAutotestInited )
        {
            if ( Main.s_Instance && Main.s_Instance.m_MainPlayer && MapEditor.s_Instance && MapEditor.s_Instance.IsMapInitCompleted() )
            {
                BeginPlayerAutoTest( Main.s_Instance.m_MainPlayer );
            }
        }



        AutoMove();
        AutoSpitSpore();
        AutoUseSkill();
        AutoAddVolume();
        AutoSpitBall();
        ForceSpitLoop();
    }

    public void BeginPlayerAutoTest( Player player )
    {
        m_Player = player;
        m_bAutoTesing = true;
        ChangeMoveDir();
        Main.s_Instance.m_MainPlayer.SetMoving(true);
        CSkillSystem.s_Instance.SetTotalPoint(100);
        for (int i = 0; i < 10; i++)
        {
            CSkillSystem.s_Instance.AddPoint(i, true);
        }

        Main.s_Instance.m_MainPlayer.SetMoving( true );

        m_bAutotestInited = true;
    }

    void ChangeMoveDir()
    {
        m_vecWorldCursorPos.x = (float)UnityEngine.Random.Range(MapEditor.s_Instance.GetWorldLeft(), MapEditor.s_Instance.GetWorldRight());
        m_vecWorldCursorPos.y = (float)UnityEngine.Random.Range(MapEditor.s_Instance.GetWorldBottom(), MapEditor.s_Instance.GetWorldTop());
        m_vecWorldCursorPos.Normalize();
        m_vecWorldCursorPos *= MapEditor.s_Instance.GetWorldSize();
    }

    float m_fSpitSporeTimeElaspe = 0f;
    int m_nSpitSporeStatus = 0;
    void AutoSpitSpore()
    {
        m_fSpitSporeTimeElaspe += Time.deltaTime;
        if ( m_nSpitSporeStatus == 0 )
        {
            if ( m_fSpitSporeTimeElaspe > 10f )
            {
                m_fSpitSporeTimeElaspe = 0;
                m_nSpitSporeStatus = 1;
                Main.s_Instance.m_MainPlayer.BeginSpitSpore();

            }
        }
        else
        {
            if (m_fSpitSporeTimeElaspe > 5f)
            {
                m_fSpitSporeTimeElaspe = 0;
                m_nSpitSporeStatus = 0;
                Main.s_Instance.m_MainPlayer.EndSpitSpore();

            }
        }



    }

    void AutoUseSkill()
    {
        m_fUseSkillTimeElapse += Time.deltaTime;
        if ( m_fUseSkillTimeElapse > 10 )
        {
            m_fUseSkillTimeElapse = 0;

            Main.s_Instance.Cast_R_Skill();
        }
    }

    float m_fChangeMoveDirInterval = 0f;
    void AutoMove()
    {
        if ( !Main.s_Instance.m_MainPlayer.IsMoving() )
        {
            Main.s_Instance.m_MainPlayer.SetMoving( true );
        }

        m_fChangeMoveDirTimeElapse += Time.deltaTime;
        if ( m_fChangeMoveDirTimeElapse > 10f )
        {
            m_fChangeMoveDirTimeElapse = 0;

            s_vecMoveDir.x = (float)UnityEngine.Random.Range(-10, 10) * 0.1f;
            s_vecMoveDir.y = (float)UnityEngine.Random.Range(-10, 10) * 0.1f;
        }


        return;

        if ( PlayerAction.s_Instance.balls_min_position.x <= MapEditor.s_Instance.GetWorldLeft() + 50f )
        {
            if (m_vecWorldCursorPos.x < 0f)
            {
                m_vecWorldCursorPos.x = -m_vecWorldCursorPos.x;
            }
        }

        if (PlayerAction.s_Instance.balls_max_position.x >= MapEditor.s_Instance.GetWorldRight() - 50f)
        {
            if (m_vecWorldCursorPos.x > 0f)
            {
                m_vecWorldCursorPos.x = -m_vecWorldCursorPos.x;
            }
        }

        if (PlayerAction.s_Instance.balls_min_position.y <= MapEditor.s_Instance.GetWorldBottom() + 50f)
        {
            if (m_vecWorldCursorPos.y < 0f)
            {
                m_vecWorldCursorPos.y = -m_vecWorldCursorPos.y;
            }
        }

        if (PlayerAction.s_Instance.balls_max_position.y >= MapEditor.s_Instance.GetWorldTop() - 50f)
        {
            if (m_vecWorldCursorPos.y > 0f)
            {
                m_vecWorldCursorPos.y = -m_vecWorldCursorPos.y;
            }
        }

        m_fChangeMoveDirInterval += Time.deltaTime;
        if (m_fChangeMoveDirInterval >= 20f)
        {
            ChangeMoveDir();
            m_fChangeMoveDirInterval = 0f;
        }
    }

    float m_fAddVolumeInterval = 0f;
    void AutoAddVolume()
    {
        m_fAddVolumeInterval += Time.deltaTime;
        if (m_fAddVolumeInterval < 5f)
        {
            return;
        }
        m_fAddVolumeInterval = 0f;

        if ( m_Player.GetTotalVolume() < 3000f )
        {
            m_Player.Test_AddAllBallSize(500);
        }
    }

    float m_fSpitBallInterval = 0f;
    void AutoSpitBall()
    {
        m_fSpitBallInterval += Time.deltaTime;
        if (m_fSpitBallInterval < 5f)
        {
            return;
        }
        m_fSpitBallInterval = 0f;
        if ( m_Player.GetCurLiveBallNum() < Main.s_Instance.m_fMaxBallNumPerPlayer )
        {
            /*
            m_Player.SetSpitPercent( 0.5f );
            m_Player.SetSpecialDirection(vecTempDir);
            m_Player.SetSpitParams( 4f, 60f, 10f );
            vecTempDir.x = 0.5f;
            vecTempDir.y = 0.5f;
            */
            if (m_bForceSpiting == false)
            {
                BeginForceSpit();
            }
        }
    }

    bool m_bForceSpiting = false;
    void BeginForceSpit()
    {
        m_bForceSpiting = true;
        Main.s_Instance.BeginSpit();
    }

    float m_fForceSpitingTimeElapse = 0f;
    void ForceSpitLoop()
    {
        if ( !m_bForceSpiting)
        {
            return;
        }

        m_fForceSpitingTimeElapse += Time.deltaTime;
        if (m_fForceSpitingTimeElapse >= 2f)
        {
            m_fForceSpitingTimeElapse = 0;
            m_bForceSpiting = false;
        }
    }


    public float GetPlayerMoveSpeed()
    {
        return 50f;
    }

    public Vector3 GetWorldCursorPosition()
    {
        return m_vecWorldCursorPos;
    }
}
