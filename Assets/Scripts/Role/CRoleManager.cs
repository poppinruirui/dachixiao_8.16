﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRoleManager : MonoBehaviour {

    public static CRoleManager s_Instance = null;

    public Sprite[] m_aryBgPlateSprites;

    public GameObject[] m_aryRolePrefabs;

    public Vector2[] m_aryRoleLocalPos;
    public Vector2[] m_aryRoleLocalScale;



    public enum eRoleFaceType
    {
        common,          // 常规
        using_skill,     // 使用技能
        being_affected,  // 被影响

        num,
    };

    // “英雄”的类型
    public enum eHeroType
    {
        yinglang,   // 音浪（音浪）
        aying,      // 暗影（潜行）
        yanmie,      // 阿丽卡（湮灭）
        guiwan,     // 鬼丸
        luoman,     // 罗曼二世

        num,
    };

    public float m_fEarShakeInterval = 0.1f;
    public float m_fEarShakeAmount = 5f;

    public float m_fHairShakeAmount = 5f;

    /// <summary>
    /// / Face Sprite
    /// </summary>
    public Dictionary<eHeroType, Sprite[]> m_dicHeroFace = new Dictionary<eHeroType, Sprite[]>();
    public Sprite[] m_aryFaceSprites_YinLang; // “音浪”角色的脸部皮肤
    public Sprite[] m_aryFaceSprites_AnYing; //  阿影：“疾跑飞步”角色的脸部皮肤
    public Sprite[] m_aryFaceSprites_YanMie; //  “湮灭”角色的脸部皮肤
    public Sprite[] m_aryFaceSprites_GuiWan; //  鬼丸：“潜行”角色的脸部皮肤s
    /// end Sprite

    //// Eye Mask Sprites
    public Sprite[] m_aryEyeMaskSprites_Common;
    public Sprite[] m_aryEyeMaskSprites_Skill;
    public Sprite[] m_aryEyeMaskSprites_Affected;
    public Vector2[] m_aryEyeMaskPos_Common;
    public Vector2[] m_aryEyeMaskPos_Skill;
    public Vector2[] m_aryEyeMaskPos_Affected;

    //// end Eye Mask Sprites

    //// Eye Container LocalPos
    public Vector2[] m_aryEye0ContainerLocalPos_Common;
    public Vector2[] m_aryEye0ContainerLocalPos_Skill;
    public Vector2[] m_aryEye0ContainerLocalPos_Affected;

    //// nose pos 
    public Vector2[] m_vecNosePos_Common;
    public Vector2[] m_vecNosePos_Skill;
    public Vector2[] m_vecNosePos_Affected;
    // end nose pos



    public Vector2 m_vecEar0_CommonPos = new Vector2();
    public float m_fEar0_CommonRotation = 0;
    public Vector2 m_vecEar1_CommonPos = new Vector2();
    public float m_fEar1_CommonRotation = 0;

    public Vector2 m_vecEar0_AffectedPos = new Vector2();
    public float m_fEar0_AffectedRotation = 0;
    public Vector2 m_vecEar1_AffectedPos = new Vector2();
    public float m_fEar1_AffectedRotation = 0;

    public Vector2 m_vecEar0_SkillPos = new Vector2();
    public float m_fEar0_SkillRotation = 0;
    public Vector2 m_vecEar1_SkillPos = new Vector2();
    public float m_fEar1_SkillRotation = 0;
   

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {

        // 初始化脸部皮肤资源
        m_dicHeroFace[eHeroType.yinglang] = m_aryFaceSprites_YinLang;
        m_dicHeroFace[eHeroType.aying] = m_aryFaceSprites_AnYing;
        m_dicHeroFace[eHeroType.yanmie] = m_aryFaceSprites_YanMie;
        m_dicHeroFace[eHeroType.guiwan] = m_aryFaceSprites_GuiWan;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public CRoleBall InstantiateRoleBall( CRoleManager.eHeroType hero_type )
    {
        return GameObject.Instantiate( m_aryRolePrefabs[(int)hero_type] ).GetComponent<CRoleBall>();
    }

    Dictionary<CRoleManager.eHeroType, List<CRoleBall>> m_dicRecycledRoleBalls = new Dictionary<CRoleManager.eHeroType, List<CRoleBall>> ();
    public void DeleteRoleBall( CRoleBall role_ball )
    {
        List<CRoleBall> lst = null;
        CRoleManager.eHeroType hero_type = role_ball.GetHeroType();
        if ( !m_dicRecycledRoleBalls.TryGetValue( hero_type, out lst ) )
        {
            lst = new List<CRoleBall>();
        }
        lst.Add( role_ball );
        m_dicRecycledRoleBalls[hero_type] = lst;

        role_ball.transform.SetParent( this.transform );
        role_ball.gameObject.SetActive( false );
    }

    public CRoleBall NewRoleBall(  CRoleManager.eHeroType hero_type )
    {
        CRoleBall role_ball = null;
        List<CRoleBall> lst = null;
        if (!m_dicRecycledRoleBalls.TryGetValue(hero_type, out lst))
        {
            lst = new List<CRoleBall>();
        }
       
        if ( lst.Count > 0 )
        {
            role_ball = lst[0];
            role_ball.gameObject.SetActive( true );
            lst.RemoveAt(0);
        }
        else
        {
            role_ball = InstantiateRoleBall( hero_type );
        }

        m_dicRecycledRoleBalls[hero_type] = lst;
        return role_ball;
    }

    public Sprite GetEyeMaskSprite( eHeroType hero_type, eRoleFaceType status  )
    {
        if (status == eRoleFaceType.common)
        {
            return m_aryEyeMaskSprites_Common[(int)hero_type];
        }
        else if (status == eRoleFaceType.using_skill)
        {
            return m_aryEyeMaskSprites_Skill[(int)hero_type];
        }
        else if (status == eRoleFaceType.being_affected)
        {
            return m_aryEyeMaskSprites_Affected[(int)hero_type];
        }

        return null;
    }

    public Sprite GetFaceSpriteByType( eHeroType hero_type, eRoleFaceType face_type )
    {
        Sprite[] aryFaceSprites = null;
        if( !m_dicHeroFace.TryGetValue( hero_type, out aryFaceSprites )   ) 
        {
            return null;
        }

        return aryFaceSprites[(int)face_type];
    }


    public Sprite GetBgPlateSpriteByIndex( int nIndex )
    {
        nIndex = nIndex % m_aryBgPlateSprites.Length;
        return m_aryBgPlateSprites[nIndex];
    }


}
