﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CSettingsManager : MonoBehaviour {

    public static CSettingsManager s_Instance = null;

    public float m_fVolumeChangeAmount = 0.05f;

    public Slider _sliderMusic;
    public Slider[] m_arySliderMusicVolume;
    public Slider _sliderAudio;
    public Slider[] m_arySliderAudioVolume;

    public CProgressBar[] m_aryMusicVolume;
    public CProgressBar[] m_aryAudioVolume;

    float m_fMusicVolume = 0f;
    float m_fAudioVolume = 0f;

    public enum eCtrlType
    {
        slider_music_volume,
        slider_audio_volume,
    };

    public UnityEngine.Object GetCtrl( eCtrlType eType)
    {
        int nDeviceType = (int)CAdaptiveManager.s_Instance.GetCurDeviceType();

        switch (eType)
        {
            case eCtrlType.slider_music_volume:
                {
                    //return m_arySliderMusicVolume[nDeviceType];
                    return m_aryMusicVolume[nDeviceType];
                }
                break;
            case eCtrlType.slider_audio_volume:
                {
                    //return m_arySliderAudioVolume[nDeviceType];
                    return m_aryAudioVolume[nDeviceType];
                }
                break;
        } // end switch

        return null;
    }

    private void Awake()
    {
        s_Instance = this;
    }


    // Use this for initialization
    void Start () {

        //// 音乐音效
        float fMusicVolume = PlayerPrefs.GetFloat("MusicVolume");
        if (fMusicVolume == 0) // 首次登入，还没设置过
        {
            fMusicVolume = 0.2f;
        }
        if (fMusicVolume == -1) // 真正的0
        {
            fMusicVolume = 0f;
        }
        m_fMusicVolume = fMusicVolume;

        //(GetCtrl(eCtrlType.slider_music_volume) as Slider).value = m_fMusicVolume;
        (GetCtrl(eCtrlType.slider_music_volume) as CProgressBar).SetfillAmount( m_fMusicVolume );

        CAudioManager.s_Instance.SetMusicVolume(fMusicVolume);

        float fAudioVolume = PlayerPrefs.GetFloat("AudioVolume");
        if (fAudioVolume == 0) // 首次登入，还没设置过
        {
            fAudioVolume = 0.8f;
        }
        if (fAudioVolume == -1) // 真正的0
        {
            fAudioVolume = 0.8f;
        }
        m_fAudioVolume = fAudioVolume;

        // (GetCtrl(eCtrlType.slider_audio_volume) as Slider).value = m_fAudioVolume;
        (GetCtrl(eCtrlType.slider_audio_volume) as CProgressBar).SetfillAmount(m_fAudioVolume);


        CAudioManager.s_Instance.SetAudioVolume(m_fAudioVolume);

        //// end 音乐音效
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void OnSliderValueChange_Music()
    {
        m_fMusicVolume = (GetCtrl(eCtrlType.slider_music_volume) as Slider)/*_sliderMusic*/.value;
        CAudioManager.s_Instance.SetMusicVolume(m_fMusicVolume);
        if (m_fMusicVolume == 0)
        {
            m_fMusicVolume = -1f;
        }
        PlayerPrefs.SetFloat("MusicVolume", m_fMusicVolume);
    }

    public void OnSliderValueChange_Audio()
    {
        m_fAudioVolume = (GetCtrl(eCtrlType.slider_audio_volume) as Slider)/*_sliderAudio*/.value;
        CAudioManager.s_Instance.SetAudioVolume(m_fAudioVolume);
        if (m_fAudioVolume == 0)
        {
            m_fAudioVolume = -1;
        }
        PlayerPrefs.SetFloat("AudioVolume", m_fAudioVolume);
    }

    public void OnClickButton_CloseMe()
    {
        CAccountSystem.s_Instance.SetPanelSettingsVisible( false );
    }

    public void OnClickButton_IncMusic()
    {
        m_fMusicVolume = (GetCtrl(eCtrlType.slider_music_volume) as CProgressBar).GetValue();
        m_fMusicVolume += m_fVolumeChangeAmount;
        if ( m_fMusicVolume > 1f )
        {
            m_fMusicVolume = 1f;
        }

        (GetCtrl(eCtrlType.slider_music_volume) as CProgressBar).SetfillAmount(m_fMusicVolume);
        CAudioManager.s_Instance.SetMusicVolume(m_fMusicVolume);

        if (m_fMusicVolume == 0)
        {
            m_fMusicVolume = -1f;
        }
        PlayerPrefs.SetFloat("MusicVolume", m_fMusicVolume);
    }

    public void OnClickButton_DecMusic()
    {
        m_fMusicVolume = (GetCtrl(eCtrlType.slider_music_volume) as CProgressBar).GetValue();
        m_fMusicVolume -= m_fVolumeChangeAmount;
        if (m_fMusicVolume < 0f)
        {
            m_fMusicVolume = 0f;
        }

        (GetCtrl(eCtrlType.slider_music_volume) as CProgressBar).SetfillAmount(m_fMusicVolume);

        CAudioManager.s_Instance.SetMusicVolume(m_fMusicVolume);

        if (m_fMusicVolume == 0)
        {
            m_fMusicVolume = -1f;
        }
        PlayerPrefs.SetFloat("MusicVolume", m_fMusicVolume);
    }

    public void OnClickButton_IncAudio()
    {
        m_fAudioVolume = (GetCtrl(eCtrlType.slider_audio_volume) as CProgressBar).GetValue();
        m_fAudioVolume += m_fVolumeChangeAmount;
        if (m_fAudioVolume > 1f)
        {
            m_fAudioVolume = 1f;
        }

        (GetCtrl(eCtrlType.slider_audio_volume) as CProgressBar).SetfillAmount(m_fAudioVolume);
        CAudioManager.s_Instance.SetAudioVolume(m_fAudioVolume);

        if (m_fAudioVolume == 0)
        {
            m_fAudioVolume = -1f;
        }
        PlayerPrefs.SetFloat("AudioVolume", m_fAudioVolume);
    }


    public void OnClickButton_DecAudio()
    {
        m_fAudioVolume = (GetCtrl(eCtrlType.slider_audio_volume) as CProgressBar).GetValue();
        m_fAudioVolume -= m_fVolumeChangeAmount;
        if (m_fAudioVolume < 0f)
        {
            m_fAudioVolume = 0f;
        }

        (GetCtrl(eCtrlType.slider_audio_volume) as CProgressBar).SetfillAmount(m_fAudioVolume);
        CAudioManager.s_Instance.SetAudioVolume(m_fAudioVolume);

        if (m_fAudioVolume == 0)
        {
            m_fAudioVolume = -1f;
        }
        PlayerPrefs.SetFloat("AudioVolume", m_fAudioVolume);
    }









}
