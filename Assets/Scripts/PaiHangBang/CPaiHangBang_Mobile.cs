﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CPaiHangBang_Mobile : MonoBehaviour {

    public static CPaiHangBang_Mobile s_Instance;

    public GameObject m_preItem;

    public GameObject m_goHighLight;

    public GameObject _panelPaiHangBang;
    public GameObject goList;
    public GameObject goItemContainer;


    public float m_fMoveSpeed = 1f;
    public float m_fVerticalSpace = 36;
    public int m_nTotalNum = 10;
    public float m_fListHeight = 600;
    

    bool m_bMovingDown = false;
    bool m_bMovingUp = false;



    static Vector3 vecTemp = new Vector3();

    List<CPaiHangBangItem_Mobile> m_lstItems = new List<CPaiHangBangItem_Mobile>();

    public CPaiHangBangRecord[] m_aryRecords;

    public Sprite m_sprMainPlayer_Left;
    public Sprite m_sprMainPlayer_Right;
    public Sprite m_sprOtherClient_Left;
    public Sprite m_sprOtherClient_Right;


    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        
        for ( int i = 0; i < m_aryRecords.Length; i++ )
        {
            m_aryRecords[i].SetIndex(i);
        }
	}

    // Update is called once per frame
    float m_fRefreshTimeCount = 0;
	void Update () {

        // 每5秒统计一次排行榜

            m_fRefreshTimeCount += Time.deltaTime;
            if (m_fRefreshTimeCount > 5)
            {
                RefreshPaiHangbangPlayerInfo();
                m_fRefreshTimeCount = 0;
            }
    }

    public void ShowPaiHangBang()
    {
        // BeginMove_Down();
        RefreshPaiHangbangPlayerInfo();
        _panelPaiHangBang.SetActive( true );
    }

    public void HidePaiHangBang()
    {
        //BeginMove_Up();
        _panelPaiHangBang.SetActive(false);
    }

    public void BeginMove_Down()
    {
        m_bMovingDown = true;

        EndMove_Up();
    }

    void Moving_Down()
    {
        if ( !m_bMovingDown)
        {
            return;
        }

        vecTemp = goList.transform.localPosition;
        vecTemp.y -= m_fMoveSpeed * Time.deltaTime;
        if (vecTemp.y <= 0f )
        {
            vecTemp.y = 0f;
            EndMove_Down();
        }
       goList.transform.localPosition = vecTemp;
    }

    void EndMove_Down()
    {
        m_bMovingDown = false;
    }

    public void BeginMove_Up()
    {
        m_bMovingUp = true;

        EndMove_Down();
    }

    void Moving_Up()
    {
        if (!m_bMovingUp)
        {
            return;
        }

        vecTemp = goList.transform.localPosition;
        vecTemp.y += m_fMoveSpeed * Time.deltaTime;
        if (vecTemp.y >= m_fListHeight)
        {
            vecTemp.y = m_fListHeight;
            EndMove_Up();
        }
        goList.transform.localPosition = vecTemp;

    }

    void EndMove_Up()
    {
        m_bMovingUp = false;
    }


    public struct sPlayerAccount
    {
        public string szName;
        public int nOwnerId;
        public float fTotalVolume;
        public int nLevel;
        public int nEatThornNum;
        public int nKillNum;
        public int nBeKilledNum;
        public int nAssistNum;
        public int nItem0Num;
        public int nItem1Num;
        public int nItem2Num;
        public int nItem3Num;
        public int nSelectedSkillId;
        public int nSelectedSkillLevel;
        public bool bIsMainPlayer;
        public bool bDead;
        public int nSkinId;
        public int nRank;
    };
    float c_fRefreshPlayerListInterval = 5f;
    float m_fRefreshPlayerListTimeCount = 0f;
    List<sPlayerAccount> lstTempPlayerAccount = new List<sPlayerAccount>();

    public List<sPlayerAccount> GetPaiHangBangPlayerList()
    {
        return lstTempPlayerAccount;
    }

    public Dictionary<int, sPlayerAccount> GetPaiHangBangPlayerDic()
    {
        return m_dicPaiHangBangPlayerInfo;
    }

    List<sPlayerAccount> m_lstFakePlayerInfo = new List<sPlayerAccount>();
    static int s_nFakePlayerId = 1000;
    public void AddOneFakePlayer()
    {
        sPlayerAccount info;
        info.szName = "Player" + s_nFakePlayerId;
        info.nOwnerId = s_nFakePlayerId;
        info.fTotalVolume = UnityEngine.Random.Range( 100f, 3000f );
        info.nLevel = (int)UnityEngine.Random.Range( 1f, 13f);
        info.nKillNum = 1;
        info.nBeKilledNum = 1;
        info.nAssistNum = 1;
        info.nItem0Num = 0;
        info.nItem1Num = 0;
        info.nItem2Num = 0;
        info.nItem3Num = 0;
        info.nEatThornNum = 0;
        info.nSelectedSkillLevel = 1;
        info.nSelectedSkillId = (int)UnityEngine.Random.Range(3, 9);
        info.bIsMainPlayer = false;
        info.bDead = false;
        info.nSkinId = 0;
        info.nRank = 0;
        m_lstFakePlayerInfo.Add( info );
        s_nFakePlayerId++;
    }

    byte[] _bytesRanks = new byte[1024];
    public void SyncRanks()
    {
        RefreshPaiHangbangPlayerInfo();
        StringManager.BeginPushData(_bytesRanks);
        StringManager.PushData_Short( (short)lstTempPlayerAccount.Count );
        for (int i = 0; i < lstTempPlayerAccount.Count; i++ )
        {
            StringManager.PushData_Short( (short)lstTempPlayerAccount[i].nOwnerId );
            StringManager.PushData_Short((short)lstTempPlayerAccount[i].nRank);
        }
        Main.s_Instance.m_MainPlayer.SyncRanks( _bytesRanks );
    }

    int m_nMainPlayerRank = 0;
    Dictionary<int, sPlayerAccount> m_dicPaiHangBangPlayerInfo = new Dictionary<int, sPlayerAccount>();
    public void RefreshPaiHangbangPlayerInfo( bool bGameOver = false )
    {
        if (bGameOver)
        {

        }
        else
        {
            m_dicPaiHangBangPlayerInfo.Clear();
            for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
            {
                PhotonPlayer photon_player = PhotonNetwork.playerList[i];
                Player player = CPlayerManager.s_Instance.GetPlayer(photon_player.ID);
                if (player == null)
                {
                    continue;
                }
                sPlayerAccount info;
                info.szName = photon_player.NickName;
                info.nOwnerId = photon_player.ID;
                info.fTotalVolume = player.GetTotalVolume();
                info.nLevel = player.GetLevel();
                info.nKillNum = player.GetKillCount();
                info.nBeKilledNum = player.GetBeKilledCount();
                info.nAssistNum = player.GetAssistAttackCount();
                info.nItem0Num = player.GetItemNum(0);
                info.nItem1Num = player.GetItemNum(1);
                info.nItem2Num = player.GetItemNum(2);
                info.nItem3Num = player.GetItemNum(3);
                info.nEatThornNum = player.GetEatThornNum();
                info.nSelectedSkillLevel = player.GetSelectedSkillLevel();
                info.nSelectedSkillId = player.GetSelectedSkillId();
                info.bIsMainPlayer = player.IsMainPlayer();
                info.bDead = player.IsDead();
                info.nSkinId = player.GetSkinId();
                info.nRank = 0;
                m_dicPaiHangBangPlayerInfo[photon_player.ID] = info;
            }
        }

        for ( int i = 0; i < m_lstFakePlayerInfo.Count; i++ )
        {
            sPlayerAccount info = m_lstFakePlayerInfo[i];
            m_dicPaiHangBangPlayerInfo[info.nOwnerId] = info;
        }

        for ( int i = 0; i < m_aryRecords.Length; i++ )
        {
            CPaiHangBangRecord record = m_aryRecords[i];
            record.gameObject.SetActive( false );
        }

        lstTempPlayerAccount.Clear();
        foreach( KeyValuePair<int, sPlayerAccount> pair in m_dicPaiHangBangPlayerInfo )
        {
            bool bInserted = false;

            
            if ( pair.Value.nSelectedSkillId == 0 )
            {
                continue;
            }
            

            for (  int i = 0; i < lstTempPlayerAccount.Count; i++  )
            {
                if ( pair.Value.fTotalVolume > lstTempPlayerAccount[i].fTotalVolume)
                {
                    lstTempPlayerAccount.Insert(i, pair.Value);
                    bInserted = true;
                    break;
                }
            } // end for
            if (!bInserted)
            {
                lstTempPlayerAccount.Add( pair.Value );
            }
        }

        int nRank = 0;
        
        int nMainPlayerIndex = 0;
        for ( int i = 0; i < lstTempPlayerAccount.Count; i++ )
        {
            nRank = i + 1;
            sPlayerAccount info = lstTempPlayerAccount[i];
            info.nRank = nRank;
            lstTempPlayerAccount[i] = info;
            if (lstTempPlayerAccount[i].nOwnerId == Main.s_Instance.m_MainPlayer.GetOwnerId())
            {
                m_nMainPlayerRank = nRank;
            }
            if ( i >= m_aryRecords.Length)
            {
                continue;
            }
            CPaiHangBangRecord record = m_aryRecords[i];
            record.gameObject.SetActive(true);
            record.SetInfo(lstTempPlayerAccount[i]);
        }

        if (m_nMainPlayerRank > 10 ) // 如果主角排在第十名时候，则把主角显示在第十名的位置
        {
            CPaiHangBangRecord record = m_aryRecords[9];
            record.gameObject.SetActive(true);
            record.SetInfo(lstTempPlayerAccount[m_nMainPlayerRank]);
        }

        CGrowSystem.s_Instance.SetRank(m_nMainPlayerRank, lstTempPlayerAccount.Count);
    }

    public void RefreshItemsInfo( bool bDoInmmediately = false )
    {
        /*
        if (!bDoInmmediately)
        {
            m_fRefreshPlayerListTimeCount += Time.deltaTime;
            if (m_fRefreshPlayerListTimeCount < c_fRefreshPlayerListInterval)
            {
                return;
            }
            m_fRefreshPlayerListTimeCount = 0f;
        }

        lstTempPlayerAccount.Clear();

        for (int i = 0; i < m_aryItems.Length; i++)
        {
            CPaiHangBangItem_Mobile item = m_aryItems[i];
            item.gameObject.SetActive(false);
        }

     //   m_goHighLight.SetActive( false );

        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            PhotonPlayer photon_player = PhotonNetwork.playerList[i];
            sPlayerAccount info;
            info.szName = photon_player.NickName;
            info.nOwnerId = photon_player.ID;
            Player player = CPlayerManager.s_Instance.GetPlayer(photon_player.ID);
            if (player == null)
            {
                continue;
            }
            int nNumOfLiveBalls = 0;
            info.fTotalVolume = player.GetTotalVolume();
            info.nLevel = player.GetLevel();
            info.nEatThornNum = player.GetEatThornNum();
            info.nKillNum = player.GetKillCount();
            info.nBeKilledNum = player.GetBeKilledCount();
            info.nAssistNum = player.GetAssistAttackCount();
            info.nItem0Num = player.GetItemNum(0);
            info.nItem1Num = player.GetItemNum(1);
            info.nItem2Num = player.GetItemNum(2);
            info.nItem3Num = player.GetItemNum(3);
            // insert sort
            bool bInserted = false;
            for (int j = 0; j < lstTempPlayerAccount.Count; j++)
            {
                if (info.fTotalVolume > lstTempPlayerAccount[j].fTotalVolume)
                {
                    lstTempPlayerAccount.Insert(j, info);
                    bInserted = true;
                    break;
                }
            } // end j
            if (!bInserted)
            {
                lstTempPlayerAccount.Add(info);
            }

        } // end i

        int nMainPlayerRank = 0;
        
        for (int i = 0; i < lstTempPlayerAccount.Count; i++)
        {
            if (i >= m_aryItems.Length)
            {
                continue;
            }
            if (Main.s_Instance && Main.s_Instance.m_MainPlayer)
            {
                if (lstTempPlayerAccount[i].nOwnerId == Main.s_Instance.m_MainPlayer.GetOwnerId())
                {
                    nMainPlayerRank = i + 1;
                   // vecTemp = m_goHighLight.transform.localPosition;
                   // vecTemp.y = -i * m_fVerticalSpace;
                   // m_goHighLight.SetActive(true);
                }
            }
            CPaiHangBangItem_Mobile item = m_aryItems[i];
            item.gameObject.SetActive(true);
            item.SetPlayerName(lstTempPlayerAccount[i].szName);
        }
        

        CGrowSystem.s_Instance.SetRank(nMainPlayerRank, lstTempPlayerAccount.Count);
        */
    }

    public int GetMyRank()
    {
        return m_nMainPlayerRank;
    }
}
