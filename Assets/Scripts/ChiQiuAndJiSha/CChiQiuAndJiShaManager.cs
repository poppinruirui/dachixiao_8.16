﻿/*
 * GM指令格式:  
 * 参数0: 必须填“3”，表示走这套流程 
 * 参数1: 动作。 0代表“吃球”，1代表“终结（击杀）”
 * 参数2:吃球(或击杀)者的PlayerID
 * 参数3:被吃球(或被击杀)者的PlayerID
 * 参数4:这一波吃球的个数(仅针对参数1为“吃球”的情况)
 * 参数5:这条信息是否与自己相关。0 - 不相关  1 - 相关   “相关”的定义：吃球、被吃球、击杀、被击杀。
 * 3 0 1 2 5 0
 * 3 1 1 2 5 1 
 * 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CChiQiuAndJiShaManager : MonoBehaviour {

    public static CChiQiuAndJiShaManager s_Instance = null;

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public Color m_colorMe;
    public Color m_colorOthers;

    public GameObject m_preChiQiuAndJiShaItem;
    public Sprite[] m_aryActionIcon;

    public GameObject m_goContainer;

    public float m_fQueueTime = 5f;
    public float m_fItemGap = 42f;
    public int m_nMaxItemNum = 5;
    public float m_fItemShowTime = 5f; // 每条记录展示时间(展示时间结束就开始消失)
    public float m_fCalculateChiQiuInterval = 5f; // 统计吃球数的时间间隔
    public float m_fMoveItemsSpeed = 40f;

    // 滑出
    public float m_fSlideTime = 0.5f; // 滑出时间
    public float m_fSlideDistance = 100f; // 滑出距离
    public float m_fSlideV0 = 0f;
    public float m_fSlideA = 0f;

    // 跳动
    public float m_fJumpDistance = 40f;
    public float m_fJumpTime = 0.5f;
    public float m_fJumpV0 = 0f;
    public float m_fJumpA = 0f;
    public float m_fJumpFadeSpeed = 0;

    public float m_fFadeSpeed = 0f;

    List<CChiQiuAndJiShaItem> m_lstItems = new List<CChiQiuAndJiShaItem>(); // 显示列表
    List<CChiQiuAndJiShaItem> m_lstToShowQueue = new List<CChiQiuAndJiShaItem>(); // “待显示”队列

        

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        m_fFadeSpeed = 1f / m_fItemShowTime;
        m_fSlideV0 = CyberTreeMath.GetV0( m_fSlideDistance,  m_fSlideTime);
        m_fSlideA = CyberTreeMath.GetA(m_fSlideDistance, m_fSlideTime);

        m_fJumpV0 = CyberTreeMath.GetV0( m_fJumpDistance, m_fJumpTime );
        m_fJumpA = CyberTreeMath.GetA( m_fJumpDistance, m_fJumpTime );
        m_fJumpFadeSpeed = 1 / m_fJumpTime;
	}
	
	// Update is called once per frame
	void Update () {
        MoveAllExistItems();
        ToShoQueueLoop();
	}

    List<CChiQiuAndJiShaItem> m_lstRecycledItems = new List<CChiQiuAndJiShaItem>();
    public CChiQiuAndJiShaItem NewItem()
    {
        CChiQiuAndJiShaItem item = null;

        if ( m_lstRecycledItems.Count > 0 )
        {
            item = m_lstRecycledItems[0];
            item.gameObject.SetActive( true );
            m_lstRecycledItems.Remove(item);
            item.Reset();
        }
        else
        {
            item = GameObject.Instantiate( m_preChiQiuAndJiShaItem ).GetComponent<CChiQiuAndJiShaItem>();
        }
        item.gameObject.SetActive( false );
        return item;
    }

    public void DeleteItem(CChiQiuAndJiShaItem item)
    {
        item.gameObject.SetActive(false);
        m_lstRecycledItems.Add( item );
    }

    public void RemoveItem(CChiQiuAndJiShaItem item)
    {
        m_lstItems.Remove( item );
        DeleteItem( item );
    }


    public void PreAddItem(int nActionId, string szEaterName, string szDeadMeatName, int nEatBallNum, int nRelated)
    {

        CChiQiuAndJiShaItem item = null;
        if ( nActionId == 1 ) // 击杀
        {
            item = NewItem();
            item.SetInfo(nActionId, szEaterName, szDeadMeatName, nEatBallNum, nRelated);
            DoAddItem(nActionId, item);
            return;
        }

        string szKey = szEaterName + "|" + szDeadMeatName;

        // 先判断是不是已经存在了
        /*
        for (int i = m_lstToShowQueue.Count - 1; i >= 0; i-- )
        {
            CChiQiuAndJiShaItem node_item = m_lstToShowQueue[i];
          //  Debug.Log( szKey + "     " +  node_item.GetKey());
            if ( szKey == node_item.GetKey() )
            {
                item = node_item;
                break;
            }
        }
        */
        if (item == null)
        {
            item = NewItem();
            item.SetKey( szKey );
            item.SetInfo(nActionId, szEaterName, szDeadMeatName, nEatBallNum, nRelated);
            DoAddItem(nActionId, item);
           // m_lstToShowQueue.Add( item );
        }
        item.IncNum();

    }

    void ToShoQueueLoop()
    {
        /*
        CChiQiuAndJiShaItem item = null;
        for (int i = m_lstToShowQueue.Count - 1; i >= 0; i--)
        {
            item = m_lstToShowQueue[i];
            float fQueueTimeElapse = item.Loop();
            if ( fQueueTimeElapse >= 3 )
            {
                DoAddItem( item );
                m_lstToShowQueue.Remove( item );
            }
        } // end for
        */
    }
   
    public void DoAddItem( int nActionId, CChiQiuAndJiShaItem item )
    {
        // 如果当前显示的条数超过了上限，则要剔除最早的
        if ( m_lstItems.Count >= m_nMaxItemNum )
        {
            int nNum = m_lstItems.Count - m_nMaxItemNum + 1;
            for (int i = 0; i < nNum; i++)
            {
                m_lstItems[i].SetStatus(2);
            }

        }

        item.gameObject.SetActive( true );
        item.SetStatus(0);
        item.transform.SetParent(m_goContainer.transform);

        vecTempPos = GetStartPos();
      
        if (nActionId == 1)
        {
            vecTempScale.x = 1.2f;
            vecTempScale.y = 1.2f;
            vecTempScale.z = 1f;

        }
        else
        {
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
        }
        item.transform.localScale = vecTempScale;
        item.transform.localPosition = vecTempPos;

        AddMoveDistanceOneTime();

        m_lstItems.Add(item);
        /*
        CChiQiuAndJiShaItem item = NewItem();

        item.SetInfo( nActionId, szEaterName, szDeadMeatName, nEatBallNum, nRelated );
     
        item.transform.SetParent(  m_goContainer.transform);
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        item.transform.localScale = vecTempScale;
        item.transform.localPosition = GetStartPos();

        AddMoveDistanceOneTime();

        m_lstItems.Add( item );
        */
    }

    float m_fTotalMoveDistance = 0;
    void AddMoveDistanceOneTime()
    {
        m_fTotalMoveDistance += m_fItemGap;
    }

    Vector3 GetStartPos()
    {
        vecTempPos.x = -CChiQiuAndJiShaManager.s_Instance.m_fSlideDistance;
        vecTempPos.z = 0;
        if ( m_lstItems.Count == 0 )
        {
            vecTempPos.y = -m_fItemGap;
        }
        else
        {
            vecTempPos.y = GetNewestItem().transform.localPosition.y - m_fItemGap;
        }

        return vecTempPos;
    }


    void MoveAllExistItems()
    {
        if ( m_fTotalMoveDistance <= 0 )
        {
            return;
        }

        float fMoveDis = m_fMoveItemsSpeed * Time.deltaTime;

        for (int i = 0; i < m_lstItems.Count; i++)
        {
            CChiQiuAndJiShaItem item = m_lstItems[i];
            vecTempPos = item.transform.localPosition;
            vecTempPos.y += fMoveDis;
            item.transform.localPosition = vecTempPos;
        }

        m_fTotalMoveDistance -= fMoveDis;
        if (m_fTotalMoveDistance < 0)
        {
            m_fTotalMoveDistance = 0;
        }
    }

    CChiQiuAndJiShaItem GetNewestItem()
    {
        if (m_lstItems.Count == 0 )
        {
            return null;
        }
        return m_lstItems[m_lstItems.Count - 1];
    }


    // 0 - 吃球   1 - 终结
    public Sprite GetActionIcon( int nActionId )
    {
        return m_aryActionIcon[nActionId];
    }
}
