﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CChiQiuAndJiShaItem : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();

    protected int m_nNum = 0;
    protected string m_szKey = "";
    protected int m_nStatus = -1; // -1: 等待阶段  0:滑出阶段  1:持续显示阶段  2:跳动并渐隐阶段
    protected float m_fQueueTimeElapse = 0; // 等待了多长时间了
    protected int m_nActionId = 0;
    protected string m_szDeadMeatName = "";

    public Text _txtEaterName;
    public Text _txtDeadMeatName;
    public Image _imgActionIcon;
    public Image _imgBgBar;
    public CanvasGroup _CanvasGroup;


   

    float m_fTimeElapse = 0f;
    float m_fSlideV0 = 0f;
    float m_fJumpV0 = 0f;

    public void Reset()
    {
        m_nNum = 0;
        m_szKey = "";
        m_nStatus = -1;
        m_fQueueTimeElapse = 0;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    private void FixedUpdate()
    {
        Counting();
    }

    public void IncNum()
    {
        m_nNum++;
        SetNum();
    }

    public void SetNum( )
    {
        if (m_nActionId == 0)
        {
            _txtDeadMeatName.text = m_szDeadMeatName + " X" + m_nNum;
        }
        else
        {
            _txtDeadMeatName.text = m_szDeadMeatName;
        }
    }

    public void SetInfo(int nActionId, string szEaterName, string szDeadMeatName, int nEatBallNum, int nRelated)
    {
        _txtEaterName.text = szEaterName;
       
        _imgActionIcon.sprite = CChiQiuAndJiShaManager.s_Instance.GetActionIcon( nActionId );
        if (nRelated == 0)
        {
            _imgBgBar.color = CChiQiuAndJiShaManager.s_Instance.m_colorOthers;
        }
        else
        {
            _imgBgBar.color = CChiQiuAndJiShaManager.s_Instance.m_colorMe;
        }
        m_szDeadMeatName = szDeadMeatName;
        _txtDeadMeatName.text = m_szDeadMeatName;
        m_nActionId = nActionId;
        _CanvasGroup.alpha = 1f;
        m_fTimeElapse = 0f;
        m_nStatus = 0;
        m_fSlideV0 = CChiQiuAndJiShaManager.s_Instance.m_fSlideV0;
        m_fJumpV0 = CChiQiuAndJiShaManager.s_Instance.m_fJumpV0;
    }

    public void SetKey( string szKey )
    {
        m_szKey = szKey;
    }

    public string GetKey()
    {
        return m_szKey;
    }

    public void SetStatus( int nStatus )
    {
        m_nStatus = nStatus;
    }

    public float Loop()
    {
        m_fQueueTimeElapse += Time.deltaTime;
        return m_fQueueTimeElapse;

    }

    void Counting()
    {
        if ( m_nStatus < 0 )
        {
            return;
        }

        m_fTimeElapse += Time.fixedDeltaTime; // 这个变量一直计时，中途不要清零
        /*
        _CanvasGroup.alpha -= CChiQiuAndJiShaManager.s_Instance.m_fFadeSpeed * Time.deltaTime;
        if ( m_fTimeElapse >= CChiQiuAndJiShaManager.s_Instance.m_fItemShowTime )
        {
            
          CChiQiuAndJiShaManager.s_Instance.RemoveItem( this );
        }
        */

        if (m_fTimeElapse >= CChiQiuAndJiShaManager.s_Instance.m_fItemShowTime)
        {
            m_nStatus = 2;
        }

  

        if ( m_nStatus == 0 ) // 滑出阶段
        {
            vecTempPos = this.transform.localPosition;
            vecTempPos.x += m_fSlideV0 * Time.fixedDeltaTime;
            this.transform.localPosition = vecTempPos;
            m_fSlideV0 += CChiQiuAndJiShaManager.s_Instance.m_fSlideA * Time.fixedDeltaTime;
            if ( m_fSlideV0 <= 0 )
            {
                vecTempPos = this.transform.localPosition;
                vecTempPos.x = 0;
                this.transform.localPosition = vecTempPos;
                m_nStatus = 1;
            }
        }

        if ( m_nStatus == 2 ) // 跳动渐隐阶段
        {
            vecTempPos = this.transform.localPosition;
            vecTempPos.y += m_fJumpV0 * Time.fixedDeltaTime;
            this.transform.localPosition = vecTempPos;
            m_fJumpV0 += CChiQiuAndJiShaManager.s_Instance.m_fJumpA * Time.fixedDeltaTime;

            _CanvasGroup.alpha -= CChiQiuAndJiShaManager.s_Instance.m_fJumpFadeSpeed * Time.fixedDeltaTime;
                            
            if ( m_fJumpV0 <= 0 )
            {
                CChiQiuAndJiShaManager.s_Instance.RemoveItem(this);
            }

        }
    }
}
