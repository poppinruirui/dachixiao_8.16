﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;



public class CAudioManager : MonoBehaviour {
    
    public static CAudioManager s_Instance;

    

    public AudioSource audio_main_bg;
    public AudioSource audio_login_bgm;

    public AudioSource[] aryAudio;

    public enum eAudioId
    {
        e_audio_explode = 0,
        e_audio_spit,
        e_audio_unfold,
        e_audio_sneak,
        e_audio_levelup,
        e_audio_dead,
        e_audio_spore,
        e_audio_eat,
		e_audio_enter_game, // 进入游戏
		e_audio_flip_page,  // 翻页
		e_audio_mouse_click_button, // 点击
        e_audio_kid_laugh, // 小孩笑声


    };

    public enum eVoice
    {
        e_voice_game_start,
    };
    public AudioSource[] m_aryVoice;

    public enum eSE
    {
        none,
        bianci,
        chiqiu,
        heqiu,
        jinengshengji,
        yanmie,
        qianxing,
        kuangbao,
        wanjiashengji,
        zhaci,
        jinke,
        modun,
        anniu,
        yinlang, // 音浪
    };

    public enum eMainBg
    {
        none,
        dating,
        zhandou,
        jieshu,
    };
    eMainBg m_eCurMainBg = eMainBg.dating;

    public AudioSource[] m_aryMainBg;

    public void PlaySE(eSE id )
    {
        aryAudio[(int)id].Play();
    }

    public void PlayVoice( eVoice id )
    {
        m_aryVoice[(int)id].Play();
    }

    public void PlayMainBg(eMainBg id )
    {
        if (m_eCurMainBg == id && m_eCurMainBg != eMainBg.none)
        {
            return;
        }

        m_eCurMainBg = id;
        if ((int)m_eCurMainBg >= m_aryMainBg.Length)
        {
            return;
        }
        m_aryMainBg[(int)m_eCurMainBg].Play();
    }

    public void StopMainBg(eMainBg id)
    {
        if ((int)id >= m_aryMainBg.Length)
        {
            return;
        }
        m_aryMainBg[(int)id].Stop();
    }

    void Awake()
    {
        //// DontDestroyOnLoad很坑，每当回到这个场景，该对象会再创建一次，导致这个对象可能变得无限多个。所以要处理一下
        if (s_Instance != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }
        s_Instance = this;
        GameObject.DontDestroyOnLoad(this);
        //// 


    }

    // Use this for initialization
    void Start () {



    }

    void UpdateAudioVolume(float fVolume)
    {
        for (int i = 0; i < aryAudio.Length; i++)
        {
            if (aryAudio[i] == null)
            {
                continue;
            }

            aryAudio[i].volume = fVolume;
        }
    }

    void UpdateMusicVolume( float fVolume )
    {
        for ( int i = 0; i < m_aryMainBg.Length; i++ )
        {
            if (m_aryMainBg[i] == null)
            {
                continue;
            }

            m_aryMainBg[i].volume = fVolume;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void PlayAudio( eAudioId id )
    {
        return;

        int nIndex = (int)id;
        if (aryAudio == null || nIndex >= aryAudio.Length || aryAudio[nIndex] == null)
        {
            return;
        }
        aryAudio[nIndex].Play();
    }

    public void SetMusicVolume( float fVolume )
    {
        UpdateMusicVolume(fVolume);
    }

    public void SetAudioVolume( float fVolume )
    {
        UpdateAudioVolume(fVolume);
    }


}
