﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBeanManager : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();

    public GameObject m_goBeanContainer;

    public static CBeanManager s_Instance;

    short m_nBeanGuid = 0;

    List<CBean> m_lstEatBean = new List<CBean>();

    const float EAT_BEAN_INTERVAL = 1f;
    float m_fEatBeanCount = 0;

    byte[] _bytesEatBeanData = new byte[10000];

    Dictionary<short, CBean> m_dicBeans = new Dictionary<short, CBean>();


    const float BEAN_REBORN_INTERVAL = 1.0f;
    float m_fBeanRebornCount = 0f;

    public int m_nTestParam;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        WaitToRebornLoop();
        EatBeanLoop();

        TestEatLoop();

        InitingBeans();


    }


    public struct sInitBeanNode
    {
        public CMonsterEditor.sThornConfig config;
        public string[] aryPos;
        public CClassEditor.sThornOfThisClassConfig class_config;
		public int nClassId;
    };

    List<sInitBeanNode> m_lstBeanToInit = new List<sInitBeanNode>();
	public void AddOneBeanToInit(ref CMonsterEditor.sThornConfig config, ref string[] aryPos, ref CClassEditor.sThornOfThisClassConfig class_config, int nClassId)
    {
        sInitBeanNode node;
        node.config = config;
        node.aryPos = aryPos;
        node.class_config = class_config;
		node.nClassId = nClassId;
        m_lstBeanToInit.Add( node );
    }

    int m_nInitBeanIndex = 0;
	int m_nInitBeanSubIndex = 0;
    bool m_bBeanInitCompleted = false;
    void InitingBeans()
    {
        if (m_bBeanInitCompleted)
        {
            return;
        }

        if (m_lstBeanToInit.Count == 0)
        {
            return;
        }

		if (m_nInitBeanIndex >= m_lstBeanToInit.Count) {
			return;
		}

		sInitBeanNode node = m_lstBeanToInit [m_nInitBeanIndex];
		int nCount = 0;
		while (nCount < 10) {
			if (m_nInitBeanSubIndex >= node.aryPos.Length - 1) {
				m_nInitBeanSubIndex = 0;
				m_nInitBeanIndex++;
				break;
			}

			float fPosX = float.Parse(node.aryPos[m_nInitBeanSubIndex]);
			float fPosY = float.Parse(node.aryPos[m_nInitBeanSubIndex+1]);
			m_nInitBeanSubIndex+= 2;

			CreanBean_New ( ref node.config, ref node.class_config, node.nClassId, fPosX, fPosY );

			nCount++;
		} // end while

		if (m_nInitBeanIndex >= m_lstBeanToInit.Count) {
			m_bBeanInitCompleted = true;
			Debug.Log ( "total bean=" + m_nBeanGuid );
			return;
		}
    }

	public void CreanBean_New(ref CMonsterEditor.sThornConfig config, ref CClassEditor.sThornOfThisClassConfig class_config, int nClassId, float fPosX, float fPosY )
	{
		CBean bean = ResourceManager.s_Instance.NewBean();

		if (nClassId == 0 )
		{
			bean.SetSprite( CMonsterEditor.s_Instance.m_aryBeanPic[0] );
		}
		else
		{
			bean.SetSprite(CMonsterEditor.s_Instance.m_aryBeanPic[1]);
		}

		float fAngle = UnityEngine.Random.Range(0, 360f);
		bean.transform.localRotation = Quaternion.identity;
		bean.transform.Rotate(0.0f, 0.0f, fAngle);

		vecTempPos.x = fPosX;
		vecTempPos.y = fPosY;
		vecTempPos.z = 0f;
		bean.transform.position = vecTempPos;
		bean.transform.parent = m_goBeanContainer.transform;
		bean.SetGuid(m_nBeanGuid);
		bean.SetColor( CColorManager.s_Instance.GetColorByString( config.szColor  ));
		bean.SetConfigId(config.szId);
		bean.SetClassConfig(class_config);
		m_dicBeans[m_nBeanGuid] = bean;
		m_nBeanGuid++;

	}

    public void CreateBean( ref CMonsterEditor.sThornConfig config, ref string[] aryPos, ref CClassEditor.sThornOfThisClassConfig class_config, int nClassId )
    {
        for (int i = 0; i < aryPos.Length; i += 2)
        {
            float fPosX = float.Parse(aryPos[i]);
            float fPosY = float.Parse(aryPos[i+1]);
            CBean bean = ResourceManager.s_Instance.NewBean();

            if (nClassId == 0 )
            {
                bean.SetSprite( CMonsterEditor.s_Instance.m_aryBeanPic[0] );
            }
            else
            {
                bean.SetSprite(CMonsterEditor.s_Instance.m_aryBeanPic[1]);
            }

			float fAngle = UnityEngine.Random.Range(0, 360f);
            bean.transform.localRotation = Quaternion.identity;
            bean.transform.Rotate(0.0f, 0.0f, fAngle);

            vecTempPos.x = fPosX;
            vecTempPos.y = fPosY;
            vecTempPos.z = 0f;
            bean.transform.position = vecTempPos;
            bean.transform.parent = m_goBeanContainer.transform;
            bean.SetGuid(m_nBeanGuid);
            bean.SetColor( CColorManager.s_Instance.GetColorByString( config.szColor  ));
            bean.SetConfigId(config.szId);
            bean.SetClassConfig(class_config);
            m_dicBeans[m_nBeanGuid] = bean;
            m_nBeanGuid++;
        }
	
    }

	public List<CBean> GetBeansListByTime( uint uTime )
	{
		List<CBean> lstBeans = null;
		if (!m_dicBeansToBeReborn.TryGetValue (uTime, out lstBeans)) {
			lstBeans = new List<CBean> ();
			m_dicBeansToBeReborn [uTime] = lstBeans;
		}
		return lstBeans;
	}

	public CBean GetBeanByGuid( short nGuid )
	{
		CBean bean = null;
		if (m_dicBeans.TryGetValue (nGuid, out bean)) {
			// to to 还是作点容错处理, 不要放任不管
		}
		return bean;
	}

	public void AnalyzeEatBeanData( byte[] bytes )
	{
		uint uCurTime = (uint)Main.GetTime ();
		List<CBean> lstBeans = null;
		lstBeans = GetBeansListByTime (uCurTime);
			
		StringManager.BeginPopData(bytes);
		int num = StringManager.PopData_Short();
		for (int i = 0; i < num; i++)
		{
			short nGuid = StringManager.PopData_Short();
			CBean bean = GetBeanByGuid (nGuid);
			if (bean == null) {
				continue;
			}
            bean.SetActive( false );
            lstBeans.Add ( bean );
		}
	}
    public void AddToEatBeanList( CBean bean )
    {
        bean.SetActive( false );
        m_lstEatBean.Add(bean);
    }

    void EatBeanLoop()
    {
        if (m_lstEatBean.Count == 0)
        {
            return;
        }

        m_fEatBeanCount += Time.deltaTime;
        if (m_fEatBeanCount < EAT_BEAN_INTERVAL)
        {
            return;
        }
        m_fEatBeanCount = 0;

        StringManager.BeginPushData(_bytesEatBeanData );
        StringManager.PushData_Short( (short)m_lstEatBean.Count);
        for ( int i = 0; i < m_lstEatBean.Count; i++ )
        {
            CBean bean = m_lstEatBean[i];
            StringManager.PushData_Short(bean.GetGuid());
        }
        m_lstEatBean.Clear();

        Main.s_Instance.m_MainPlayer.SyncEatBeanData(_bytesEatBeanData, StringManager.GetBlobSize());
    }

    public void EatBean( short nGuid, float fDeadTime )
    {
        CBean bean = null;
        if (!m_dicBeans.TryGetValue(nGuid, out bean))
        {
            return;
        }

        bean.SetActive( false );
        bean.SetDeadTime(fDeadTime);
        AddBeanToWaitToRebornList( bean );
    }

	Dictionary<uint, List<CBean>> m_dicBeansToBeReborn = new Dictionary<uint, List<CBean>>();
    void AddBeanToWaitToRebornList( CBean bean)
    {
		// poppin test 623
        // m_lstBeanWaitToRebornList.Add( bean );


		uint uCurTime = (uint)Main.GetTime();
		List<CBean> lstBeans = GetBeansListByTime (uCurTime);
		lstBeans.Add (bean);
    }

	List<uint> m_lstTempReborn = new List<uint>();
    void WaitToRebornLoop()
	{
		/*
         if (m_lstBeanWaitToRebornList.Count == 0)
        {
            return;
        }

        m_fBeanRebornCount += Time.deltaTime;
        if (m_fBeanRebornCount < BEAN_REBORN_INTERVAL)
        {
            return;
        }
        m_fBeanRebornCount = 0;


        for ( int i = m_lstBeanWaitToRebornList.Count - 1; i >= 0; i-- )
        {
            CBean bean = m_lstBeanWaitToRebornList[i];
            if ( Main.GetTime() - bean.GetDeadTime() >= bean.GetClassConfig().fRebornTime )
            {
                RebornBean( bean );
                m_lstBeanWaitToRebornList.RemoveAt(i);
            }
        }
		*/

		m_fBeanRebornCount += Time.deltaTime;
		if (m_fBeanRebornCount < BEAN_REBORN_INTERVAL)
		{
			return;
		}
		m_fBeanRebornCount = 0;

		m_lstTempReborn.Clear ();

		foreach (KeyValuePair<uint, List<CBean>> pair in m_dicBeansToBeReborn) {
			float fTimeElapse = Main.GetTime () - pair.Key;
			if (fTimeElapse > 20f) { // "20f"这个值暂定，仅供测试

				m_lstTempReborn.Add ( pair.Key );

				for (int i = 0; i < pair.Value.Count; i++) {
					RebornBean (pair.Value[i]);
				}
			}
		} // end foreach

		for (int i = 0; i < m_lstTempReborn.Count; i++) {
			m_dicBeansToBeReborn.Remove ( m_lstTempReborn[i] );
		}
    }

    void RebornBean( CBean bean )
    {
        bean.SetActive( true );
    }

    public void Test_Eat()
    {
        m_bTesting = true;
    }

    bool m_bTesting = false;
    void TestEatLoop()
    {
        if ( !m_bTesting)
        {
            return;
        }

        for (int i = 0; i < m_nTestParam; i++)
        {
            foreach (KeyValuePair<short, CBean> pair in m_dicBeans)
            {
                CBean bean = pair.Value;
                if (!bean.GetActive())
                {
                    continue;
                }
                // AddToEatBeanList(bean);
                Main.s_Instance.m_MainPlayer.EatBean_Test( bean );
                break;
            }
        }
    }

}
