﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CRoom : MonoBehaviour {

    static Vector3 vecTempScale = new Vector3();

    /// <summary>
    /// / UI
    /// </summary>
    public Text _txtRoomName;
    public Image _imgAvatar;
    public Text _txtCurPlayerCount;
    public Button _btnEnter;
    public Image _imgBg;

    int m_nIndex = 0;
    bool m_bSelected = false;

	// Use this for initialization
	void Start () {
        _imgBg.material = new Material(Shader.Find("Sprites/FilterColor"));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetSelected( bool bSelected )
    {
        m_bSelected = bSelected;

        //_btnEnter.gameObject.SetActive(bSelected);
        if (bSelected)
        {
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            this.transform.localScale = vecTempScale;
            //_imgBg.sprite = CSelectRoomManager.s_Instance.m_sprSelected;
            // _imgBg.color = CSelectRoomManager.s_Instance.m_colorSelected;
            SetButtonSelectedColor( true );
        }
        else
        {
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            this.transform.localScale = vecTempScale;
            //_imgBg.sprite = CSelectRoomManager.s_Instance.m_sprNotSelected;
            //_imgBg.color = Color.white;
            SetButtonSelectedColor(false);
        }
    }

    public void SetButtonSelectedColor( bool bSelected )
    {
        if ( bSelected )
        {
            _imgBg.material.SetFloat( "_Filtering", 1 );
            _imgBg.material.SetColor("_FilterColor", Color.white);
            _imgBg.material.SetColor("_DestColor", CSelectRoomManager.s_Instance.m_colorSelected);
        }else
        {
            _imgBg.material.SetFloat("_Filtering", 0);
        }
    }

    public void SetRoomIndex( int nIndex )
    {
        m_nIndex = nIndex;
    }

    public int GetRoomIndex()
    {
        return m_nIndex;
    }

    public void SetAvatar( Sprite sprAvatar )
    {
        _imgAvatar.sprite = sprAvatar;
    }

    public void SetRoomName( string txtRoomName )
    {
        _txtRoomName.text = txtRoomName;
    }

    public string GetRoomName()
    {
        return _txtRoomName.text;
    }

    public void SetPlayerCount( int nCurCount, int nMaxCount )
    {
        _txtCurPlayerCount.text = nCurCount + "/" + nMaxCount;
    }

    public void OnClickButton_EnterThisRoom()
    {
        CSelectRoomManager.s_Instance.EnterRoom( GetRoomIndex() );
    }

    public void OnClickButton_PickMe()
    {
        CSelectRoomManager.s_Instance.SelectRoom(this);


    }
}
