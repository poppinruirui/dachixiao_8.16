﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CReconnectManager  : Photon.PunBehaviour { 

	public static CReconnectManager s_Instance = null;


	void Awake()
	{
		s_Instance = this;
	}


	// Use this for initialization
	void Start () {
        Reconnect();
        m_fDisconnectedTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
       
    }
    float m_fDisconnectedTime = 0;

	public void Reconnect()
    {
        StartCoroutine("LoadScene", "SelectCaster");
        return;


        if (!PhotonNetwork.connected)
        {
            PhotonNetwork.ConnectToRegion(CloudRegionCode.cn, AccountManager. _gameVersion);

        }
        else
        {
            
        }

        m_fDisconnectedTime += Time.deltaTime;
        if (m_fDisconnectedTime > 3)
        {
            StartCoroutine("LoadScene", "SelectCaster");
        }

        /*
		if (!PhotonNetwork.connected) {
			PhotonNetwork.ConnectToRegion (CloudRegionCode.cn, AccountManager._gameVersion);
		} else {
			OnConnectedToMaster ();
		
		}
        */
       // StartCoroutine("LoadScene", "SelectCaster");
    }



    //异步加载场景  
    IEnumerator LoadScene(string scene_name)
    {
        AccountData.s_Instance.asyncLoad = SceneManager.LoadSceneAsync(scene_name);

        while (!AccountData.s_Instance.asyncLoad.isDone)
        {
            yield return null;
        }


    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();

        Debug.Log( "reconnect scene OnJoinedLobby....." );

        AccountManager.ReallyPlayRoom();

      
 	}

	public void OnClickButton_Reconnect()
	{
		
	}

}
