﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WebAuthAPI;

public class main : MonoBehaviour {

    public InputField registerPanelInputFieldPhoneNumber;
    public InputField registerPanelInputFieldSMSCode;
    public InputField registerPanelInputFieldPassword;
    public Button     registerPanelButtonRegister;
    public Button     registerPanelButtonGetSMSCode;

    public InputField passwordLoginPanelInputFieldPhoneNumber;
    public InputField passwordLoginPanelInputFieldPassword;
    public Button     passwordLoginPanelButtonLogin;

    public InputField resetPanelInputFieldPhoneNumber;
    public InputField resetPanelInputFieldSMSCode;
    public InputField resetPanelInputFieldPassword;
    public Button     resetPanelButtonReset;
    public Button     resetPanelButtonGetSMSCode;

    public Button     sessionVerifyPanelButtonVerify;
    public Button     sessionVerifyPanelButtonExecute;    
    public Button     sessionVerifyPanelButtonLogout;
    
    Authenticator authenticator;
    Account account;
    Charge  charge;
    Market  market;
    
	void Start()
    {
        this.authenticator = new Authenticator(this);
        this.authenticator.init(delegate(int code) {
                if (code == ServErr.SERV_ERR_SUCCESSFUL) {
                    Debug.Log("authenticator inited!");
                }
                else {
                    Debug.Log("authenticator init failed with: " + code + "!");
                }
            });
        this.account = new Account(this.authenticator);
        this.charge = new Charge(this.authenticator);
        this.market = new Market(this.authenticator);
        this.registerPanelButtonRegister.onClick.AddListener(this.onRegisterPanelButtonRegisterClick);
        this.registerPanelButtonGetSMSCode.onClick.AddListener(this.onRegisterPanelButtonGetSMSCodeClick);
        this.passwordLoginPanelButtonLogin.onClick.AddListener(this.onPasswordLoginPanelButtonLoginClick);
        this.resetPanelButtonReset.onClick.AddListener(this.onResetPanelButtonResetClick);
        this.resetPanelButtonGetSMSCode.onClick.AddListener(this.onResetPanelButtonGetSMSCodeClick);
        this.sessionVerifyPanelButtonVerify.onClick.AddListener(this.onSessionVerifyPanelButtonVerifyClick);
        this.sessionVerifyPanelButtonExecute.onClick.AddListener(this.onSessionVerifyPanelButtonExecuteClick);            
        this.sessionVerifyPanelButtonLogout.onClick.AddListener(this.onSessionVerifyPanelButtonLogoutClick);
	}

    void OnDestroy()
    {
        this.authenticator.fini();
    }

    void onRegisterPanelButtonRegisterClick()
    {
        if (this.authenticator.isLoaded()) {
            this.authenticator.register(this.registerPanelInputFieldPhoneNumber.text,
                                        this.registerPanelInputFieldSMSCode.text,
                                        this.registerPanelInputFieldPassword.text, delegate(int code) {
                                            Debug.Log(code);
                                        });
        }
    }

    void onRegisterPanelButtonGetSMSCodeClick()
    {
        if (this.authenticator.isLoaded()) {
            this.StartCoroutine(this.authenticator.getSMSCode(this.registerPanelInputFieldPhoneNumber.text,
                                                              Authenticator.SMS_TYPE_REGISTER, delegate(int code) {
                                                                  Debug.Log(code);
                                                              }));
        }
    }
    
    void onPasswordLoginPanelButtonLoginClick()
    {
        if (this.authenticator.isLoaded()) {
            this.authenticator.passwordLogin(this.passwordLoginPanelInputFieldPhoneNumber.text,
                                             this.passwordLoginPanelInputFieldPassword.text, delegate(int code) {
                                                 Debug.Log(code);
                                             });
        }
    }

    void onResetPanelButtonResetClick()
    {
        if (this.authenticator.isLoaded()) {
            this.authenticator.resetPassword(this.resetPanelInputFieldPhoneNumber.text,
                                             this.resetPanelInputFieldSMSCode.text,
                                             this.resetPanelInputFieldPassword.text, delegate(int code) {
                                                 Debug.Log(code);
                                             });
        }
    }

    void onResetPanelButtonGetSMSCodeClick()
    {
        if (this.authenticator.isLoaded()) {
            this.StartCoroutine(this.authenticator.getSMSCode(this.resetPanelInputFieldPhoneNumber.text,
                                                              Authenticator.SMS_TYPE_INITPASS, delegate(int code) {
                                                                  Debug.Log(code);
                                                              }));
        }
    }

    void onSessionVerifyPanelButtonVerifyClick()
    {
        if (this.authenticator.isLoaded()) {
            this.authenticator.sessionVerify(Authenticator.sessionVerifyPostForm, null, delegate(int code, object data) {
                    Debug.Log(code);
                });
        }
    }

    void onSessionVerifyPanelButtonExecuteClick()
    {
        /* getAccountInfo 
        this.account.getAccountInfo(delegate(int code, object data) {
                if (code == ServErr.SERV_ERR_SUCCESSFUL) {
                    AccountJSON json = (AccountJSON)data;
                    Debug.Log("phonenum: " + json.phonenum + ", " +
                              "rolename: " + json.rolename + ", " +
                              "coins: " + json.coins + ", " +
                              "diamonds: " + json.diamonds + ", " +
                              "forbidden: " + json.forbidden);
                }
                else {
                    Debug.Log(code);
                }
            });
        */
        
        /* updateRoleName
        this.account.updateRoleName("happytree", delegate(int code, object data) {
                Debug.Log(code);
            });
        */

        /* getChargeList
        this.charge.getChargeList(delegate(int code, object data) {
                if (code == ServErr.SERV_ERR_SUCCESSFUL) {
                    ChargeProductJSON[] products = (ChargeProductJSON[])data;
                    for (var i=0; i<products.Length; ++i) {
                        ChargeProductJSON product = products[i];
                        Debug.Log("productguid: " + product.productguid + ", " +
                                  "productname: " + product.productname + ", " +
                                  "productdesc: " + product.productdesc + ", " +
                                  "earncoins: " + product.earncoins + ", " +
                                  "earndiamonds: " + product.earndiamonds);
                    }
                }
                else {
                    Debug.Log(code);
                }
            });
         */
        
        /* chargeGet
        this.charge.chargeGet("1000coins", delegate(int code, object data) {
                if (code == ServErr.SERV_ERR_SUCCESSFUL) {
                    ChargeProductJSON product = (ChargeProductJSON)data;
                    Debug.Log("productguid: " + product.productguid + ", " +
                              "productname: " + product.productname + ", " +
                              "productdesc: " + product.productdesc + ", " +
                              "earncoins: " + product.earncoins + ", " +
                              "earndiamonds: " + product.earndiamonds);
                }
                else {
                    Debug.Log(code);
                }
            });
         */
        
        /* chargeAdd
        {
            ChargeProductJSON product = new ChargeProductJSON();
            product.productguid = "1000coins";
            product.productname = "1000个金币";
            product.productdesc = "6元1000个金币";
            product.earncoins = 1000;
            product.earndiamonds = 0;
            this.charge.chargeAdd(product, delegate(int code, object data) {
                    Debug.Log(code);
                });
        }
        */
        
        /* chargeDel
        this.charge.chargeDel("1000coins", delegate(int code, object data) {
                Debug.Log(code);
            });
        */
        
        /* chargeCharge
        this.charge.chargeCharge("1000coins", 2, delegate(int code, object data) {
                Debug.Log(code);
            });
        */

        /* receiptValidate
        this.charge.receiptValidate("invalid contents", delegate(int code, object data) {
                Debug.Log(code);
            });
        */
        
        /* getProductList
        this.market.getProductList(delegate(int code, object data) {
                if (code == ServErr.SERV_ERR_SUCCESSFUL) {
                    MarketProductJSON[] products = (MarketProductJSON[])data;
                    for (var i=0; i<products.Length; ++i) {
                        MarketProductJSON product = products[i];
                        Debug.Log("productguid: " + product.productguid + ", " +
                                  "productname: " + product.productname + ", " +                                  
                                  "producttype: " + product.producttype + ", " +
                                  "forbidden: " + product.forbidden + ", " +
                                  "productdesc: " + product.productdesc + ", " +
                                  "productimage: " + product.productimage + ", " +
                                  "costcoins: " + product.costcoins + ", " +
                                  "costdiamonds: " + product.costdiamonds + ", " +
                                  "productnumattr1: " + product.productnumattr1 + ", " +
                                  "productnumattr2: " + product.productnumattr2 + ", " +
                                  "producttxtattr1: " + product.producttxtattr1 + ", " +
                                  "producttxtattr2: " + product.producttxtattr2);
                    }
                }
                else {
                    Debug.Log(code);
                }
            });
        */
        
        /* marketGet
        this.market.marketGet("skin_00000001", delegate(int code, object data) {
                if (code == ServErr.SERV_ERR_SUCCESSFUL) {
                    MarketProductJSON product = (MarketProductJSON)data;
                    Debug.Log("productguid: " + product.productguid + ", " +
                              "productname: " + product.productname + ", " +
                              "producttype: " + product.producttype + ", " +
                              "forbidden: " + product.forbidden + ", " +
                              "productdesc: " + product.productdesc + ", " +
                              "productimage: " + product.productimage + ", " +
                              "costcoins: " + product.costcoins + ", " +
                              "costdiamonds: " + product.costdiamonds + ", " +
                              "productnumattr1: " + product.productnumattr1 + ", " +
                              "productnumattr2: " + product.productnumattr2 + ", " +
                              "producttxtattr1: " + product.producttxtattr1 + ", " +
                              "producttxtattr2: " + product.producttxtattr2);
                }
                else {
                    Debug.Log(code);
                }
            });
         */
        
        /* marketAdd
        {
            MarketProductJSON product = new MarketProductJSON();
            product.productguid = "skin_00000001";
            product.productname = "皮肤1";
            product.producttype = "skin";
            product.forbidden = "1";
            product.productdesc = "皮肤1";
            product.productimage = "images/skin_00000001.png";
            product.costcoins = 1000;
            product.costdiamonds = 0;
            product.productnumattr1 = 0;
            product.productnumattr2 = 0;
            product.producttxtattr1 = "";
            product.producttxtattr2 = "";

            this.market.marketAdd(product, delegate(int code, object data) {
                    Debug.Log(code);
                });
        }
         */
        
        /* marketDel
        this.market.marketDel("skin_00000001", delegate(int code, object data) {
                Debug.Log(code);
            });
        */

        /* marketSet
        {
            MarketProductJSON product = new MarketProductJSON();
            product.productguid = "skin_00000001";
            product.productname = "皮肤1";
            product.producttype = "skin";
            product.forbidden = "0";
            product.productdesc = "皮肤1";
            product.productimage = "images/skin_00000001.png";
            product.costcoins = 0;
            product.costdiamonds = 1000;
            product.productnumattr1 = 0;
            product.productnumattr2 = 0;
            product.producttxtattr1 = "";
            product.producttxtattr2 = "";

            this.market.marketSet(product, delegate(int code, object data) {
                    Debug.Log(code);
                });
        }
        */

        /* marketConsume
        this.market.marketConsume("skin_00000001", 1, delegate(int code, object data) {
                Debug.Log(code);
            });
        */
        
        /* marketBuy
        this.market.marketBuy("skin_00000001", 1, delegate(int code, object data) {
                Debug.Log(code);
            });
        */

        /* getBuymentList
        this.market.getBuymentList(delegate(int code, object data) {
                if (code == ServErr.SERV_ERR_SUCCESSFUL) {
                    MarketBuymentJSON[] buyments = (MarketBuymentJSON[])data;
                    for (var i=0; i<buyments.Length; ++i) {
                        MarketBuymentJSON buyment = buyments[i];
                        Debug.Log("productguid: " + buyment.productguid + ", " +
                                  "quantity: " + buyment.quantity);
                    }
                }
                else {
                    Debug.Log(code);
                }
            });
        */
    }

    void onSessionVerifyPanelButtonLogoutClick()
    {
        if (this.authenticator.isLoaded()) {
            this.authenticator.sessionLogout(delegate(int code) {
                    Debug.Log(code);
                });
        }
    }
}
