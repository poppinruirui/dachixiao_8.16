﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CDust : MonoBehaviour {

    public SpriteRenderer _srLight;

    static Color colorTemp = new Color();
    static Vector3 vecTempScale = new Vector3();
    static Vector3 vecTempPos = new Vector3();
    static Quaternion vecTempRotation = new Quaternion();

    public Rigidbody2D _rigid;
    public Collider2D _collider;

    Vector2 m_vec2StartPos = new Vector2();
    Vector3 m_vecPosInOrbit = new Vector3();
    float m_fRotation = 0;
    Vector3 vecTempDirection = new Vector3();
    float m_fOrbitRaduis;

    float m_fDeltaX = 0f;
    float m_fDeltaY = 0f;

    public SpriteRenderer _srMain;

    int m_nId = 0;
    float m_fCollisionTime = 0f;

    // Use this for initialization
    void Start () {
        /*
        float r = ((float)UnityEngine.Random.Range(1, 10) ) / 10f;
        float g = ((float)UnityEngine.Random.Range(1, 10)) / 10f;
        float b = ((float)UnityEngine.Random.Range(1, 10)) / 10f;
        _srMain.color = new Color( r,g,b );
        */
        this.gameObject.layer = (int)CLayerManager.eLayerId.star_dust;

        
	}
	
	// Update is called once per frame
	void Update () {
        BlingLoop();
    }

    public void MoveToPosition( Vector2 pos )
    {
        //_rigid.MovePosition( pos );
        //_rigid.MoveRotation(0);
        SetPos( pos );
    }

    public void SetRadius( float fRadius )
    {
        m_fOrbitRaduis = fRadius;

        m_fDeltaX = m_fOrbitRaduis * CStarDust.s_Instance.m_fAngularVElocityX;
        m_fDeltaY = m_fOrbitRaduis * CStarDust.s_Instance.m_fAngularVElocityY;
    }

    public void SetStartPos( Vector2 pos, float fStartRotation)
    {
        m_vec2StartPos = pos;
        this.transform.position = pos;
        m_fRotation = fStartRotation;
    }

    public void SetSize( float fSize )
    {
        vecTempScale.x = fSize;
        vecTempScale.y = fSize;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    public void SetMass( float fMass )
    {
        _rigid.mass = fMass;
    }

    public void SetPosInOrbitDirectlly( Vector3 pos )
    {
        m_vecPosInOrbit = pos;
    }

    bool m_bActive = false;
    public void SetActive( bool bActive )
    {
        m_bActive = bActive;
        this.gameObject.SetActive(m_bActive);
    }

    public bool GetActive()
    {
        return m_bActive;
    }

    bool m_bInCamView = true;

    public void Move( float fDeltaRotation, bool bCheckIfInCamView)
    {
        float fCurRotation = 0;
         
        //if (bCheckIfInCamView )
       // {
            fCurRotation = m_fRotation + fDeltaRotation;
            m_vecPosInOrbit.x = Mathf.Cos(fCurRotation) * m_fOrbitRaduis;
            m_vecPosInOrbit.y = Mathf.Sin(fCurRotation) * m_fOrbitRaduis;
        //}

        
        /* 

        if (bCheckIfInCamView)
        {

            if (m_bInCamView)
            {
                m_bInCamView = CCameraManager.s_Instance.CheckIfDustInView(GetPos());// Main.s_Instance.IsInCamView(GetPos());
            }
            else
            {
                m_bInCamView = CCameraManager.s_Instance.CheckIfDustInView(m_vecPosInOrbit);// Main.s_Instance.IsInCamView(m_vecPosInOrbit);
            }


            if (m_bInCamView)
            {
                if ( !this.GetActive() )
                {
                    this.SetPos(m_vecPosInOrbit);
                }
                this.SetActive(true);
            }
            else
            {
                this.SetActive(false);
                return;
            }

        }
        
        if ( !m_bInCamView)
        {
            return;
        }
        */

        // poppin test 8.24
        float fDis = Vector2.Distance(m_vecPosInOrbit, this.transform.position);
        float fSpeed = 0.25f;//fDis * CStarDust.s_Instance.GetBackToOrbitSpeed();
        vecTempDirection = m_vecPosInOrbit - this.transform.position;
        vecTempDirection.Normalize();
        float fDeltaX = fSpeed * vecTempDirection.x * /*Time.deltaTime*/CStarDust.c_fMoveInterval;
        float fDeltaY = fSpeed * vecTempDirection.y * /*Time.deltaTime*/CStarDust.c_fMoveInterval;
        vecTempPos = this.transform.position;
        vecTempPos.x += fDeltaX;
        vecTempPos.y += fDeltaY;
        MoveToPosition(vecTempPos);
    }

    public void MoveDirectlly(float fDeltaRotation)
    {

        float fCurRotation = m_fRotation + fDeltaRotation;
        
        m_vecPosInOrbit.x = Mathf.Cos(fCurRotation) * m_fOrbitRaduis;
        m_vecPosInOrbit.y = Mathf.Sin(fCurRotation) * m_fOrbitRaduis;
        SetPos(m_vecPosInOrbit);
    }


    public Vector3 GetPos()
    {
        return this.transform.position;
    }

    public void SetPos( Vector3 pos )
    {
        this.transform.position = pos;
    }

    public void SetId( int nId )
    {
        m_nId = nId;
    }

    public int GetId()
    {
        return m_nId;
    }

    public void SetCollisionTime( float fCollisionTime )
    {
        m_fCollisionTime = fCollisionTime;
    }

    public float GetCollisionTime()
    {
        return m_fCollisionTime;
    }

    public float GetRotation()
    {
        return this.transform.localRotation.z;
    }

    public void SetRotation( float fRotation )
    {
        vecTempRotation = this.transform.localRotation;
        vecTempRotation.z = fRotation;
        this.transform.localRotation = vecTempRotation;
    }

    float m_fSyncedTime = 0f;
    public void SetSyncedTime( float fTime )
    {
        m_fSyncedTime = fTime;
    }

    public float GetSyncedTime()
    {
        return m_fSyncedTime;
    }

    float m_fBlingTime = 0f;
    public void BeginBling()
    {
        m_fBlingTime = 0.5f;
        _srLight.gameObject.SetActive(true);
    }

    int m_nOp = 1;
    int m_nOpScale = 1;
    const float COLOR_SPEED = 0.025f;
    const float SCALE_SPEED = 0.01f;
    void BlingLoop()
    {
        if (m_fBlingTime <= 0f)
        {
            return;
        }
        m_fBlingTime -= Time.deltaTime;
        if (m_fBlingTime <= 0f)
        {
            EndBling();
        }

        if (m_nOp == 1)
        {
            colorTemp = _srLight.color;
            colorTemp.a += COLOR_SPEED;
            _srLight.color = colorTemp;
            if (_srLight.color.a >= 1)
            {
                m_nOp = -1;
            }
        }
        else if (m_nOp == -1)
        {
            colorTemp = _srLight.color;
            colorTemp.a -= COLOR_SPEED;
            _srLight.color = colorTemp;
            if (_srLight.color.a <= 0 )
            {
                m_nOp = 1;
            }

        }


        if ( m_nOpScale == 1 )
        {
            vecTempScale = _srLight.transform.localScale;
            vecTempScale.x += SCALE_SPEED;
            vecTempScale.y += SCALE_SPEED;
            _srLight.transform.localScale = vecTempScale;
            if (vecTempScale.x >= 0.6f)
            {
                m_nOpScale = -1;
            }
        }
        else if (m_nOpScale == -1)
        {
            vecTempScale = _srLight.transform.localScale;
            vecTempScale.x -= SCALE_SPEED;
            vecTempScale.y -= SCALE_SPEED;
            _srLight.transform.localScale = vecTempScale;
            if (vecTempScale.x <= 0.2f)
            {
                m_nOpScale = 1;
            }
        }
    }

    void EndBling()
    {
        _srLight.gameObject.SetActive( false );
    }
}
