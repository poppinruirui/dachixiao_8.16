﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CVisionManaer : MonoBehaviour {

    public static CVisionManaer s_Instance = null;

    public UnityStandardAssets.ImageEffects.BlurOptimized _scriptBlur;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetBlurEnabled( bool bEnabled )
    {
        _scriptBlur.enabled = bEnabled;
    }
}
