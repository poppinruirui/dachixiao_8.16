﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using UnityEngine.Rendering;

public class CMonster : MonoBehaviour {

    public SortingGroup _sortingGroup;

	CMonsterEditor.sThornConfig m_Config;

    public bool m_bIsJueSheng = false;

  
	static Vector3 vecTempPos = new Vector3();
	static Vector3 vecTempScale = new Vector3();
	


    public Rigidbody2D _rigid;

    // “怪”的种类(逻辑功能层面)
    public enum eMonsterType
	{
        scene,         // 场景上统一生成的（要同步的）
		spit_spore,  // 吐的孢子
		spray,          // 喷泉
		la_baba,      // 拉的粑粑
	};

	long m_nGUID = 0; // 独一无二的实例编号
	string m_szConfigId = ""; // 配置数据编号

    public CircleCollider2D _Trigger;

	public SpriteRenderer _srMain;

	float m_fSize = 0f;
    float m_fRadius = 0f;


    public eMonsterType m_eMonsterType = eMonsterType.scene;
    public CMonsterEditor.eMonsterBuildingType  m_eMonsterBuildingType;

    int[] m_aryIntParam = new int[4];
	float[] m_aryFloatParam = new float[4];

	bool m_bDead = false;
	float m_fDeadOccurTime = 0f;
	float m_fRebornTime = 0f;

	Vector2 m_Dir = new Vector2();
	Vector3 m_Pos = new Vector3();

	Vector2 m_vEjectSpeed = new Vector2();
	float m_fjectTotalDis = 0f;
	bool m_bEjecting = false;
	Vector3 m_vEjectStartPos = new Vector3();

    Player m_Player = null;
    Ball m_Ball = null;

    public GameObject m_goContainer;

    protected Vector2 m_vecLastPos = new Vector2();

    private void Awake()
    {
        Init(); 
    }

    // Use this for initialization
    void Start () {


        
    }
	
	// Update is called once per frame
	void Update () {
		EjectingLoop ();
	}

    public void Reset()
    {
        SetPlayer(null);
        SetBall( null );
        m_bEjecting = false;
        m_vEjectSpeed = Vector2.zero;
        m_fRotationSpeed = 0;
    }

    public void Init()
    {
        
      
    }

   

    public void OnMouseDown()
    {
        CClassEditor.s_Instance.SelectMonster( this );
    }

    public void EatBall(Ball ball)
    {
        //ball.SetEaten(true);
        ball.SetActive( false );
        ball._Player.DestroyBall(ball.GetIndex());
    }


    public void SetActive( bool val )
    {
        //this.gameObject.SetActive( val );
        m_goContainer.gameObject.SetActive(val);



		_Trigger.enabled = val;
    }

    public void SetTriggerEnable( bool bEnable )
    {

    }


    bool m_bAutoReborn = false;
	public void SetAutoReborn( bool val )
	{
		m_bAutoReborn = val;
	}

	public bool GetAutoReborn()
	{
		return m_bAutoReborn;
	}

	public void SetGuid( long nId )
	{
		m_nGUID = nId;
	}

	public long GetGuid()
	{
		return m_nGUID;
	}

	public void SetConfigId( string szConfigId )
	{
		m_szConfigId = szConfigId;
		m_Config = CMonsterEditor.s_Instance.GetMonsterConfigById (m_szConfigId);
		SetSize ( CyberTreeMath.Radius2Scale( m_Config.fSelfSize ) );
		SetColor ( m_Config.szColor );
        SetMonsterBuildingType((CMonsterEditor.eMonsterBuildingType)m_Config.nType);

        /*
        
        if ( m_Config.fExplodeFormationId == 1) // 十字刺
        {
            SetSprite(CMonsterEditor.s_Instance.m_arySprites_Thorn[(int)m_Config.fExplodeFormationId]);
        }
        else if (m_Config.fExplodeFormationId == 2)// 三角刺
        {
            SetSprite(CMonsterEditor.s_Instance.m_arySprites_Thorn[(int)m_Config.fExplodeFormationId]);
        }
        else
        {
            SetSprite(CMonsterEditor.s_Instance.GetSpriteByMonsterType((CMonsterEditor.eMonsterBuildingType)m_Config.nType));
        }
        */

        SetSprite(CMonsterEditor.s_Instance.m_arySprites_Thorn[(int)m_Config.fExplodeFormationId]);
    }

    public CMonsterEditor.sThornConfig GetConfig()
    {
        return m_Config;
    }

    public string GetConfigId()
	{
		return m_szConfigId;
	}

	public void SetMonsterType( eMonsterType val )
	{
		m_eMonsterType = val;
	}

	public eMonsterType GetMonsterType()
	{
		return m_eMonsterType;
	}

    public void SetMonsterBuildingType( CMonsterEditor.eMonsterBuildingType type  )
    {
        m_eMonsterBuildingType = type;
    }

    public CMonsterEditor.eMonsterBuildingType GetMonsterBuildingType()
    {
        return m_eMonsterBuildingType;
    }

    public void SetDead( bool val, bool bShowImmediately = true )
	{
		m_bDead = val;
        _Trigger.enabled = !m_bDead;
        if (m_bDead)
        {
            m_goContainer.gameObject.SetActive(false);
        }
        else
        {
            if ( bShowImmediately )
            {
                m_goContainer.gameObject.SetActive(true);
            }
        }
    }

	public bool IsDead()
	{
		return m_bDead;
	}

	// 设置死亡发生时间，重生的时候好判断
	public void SetDeadOccurTime( float val )
	{
		m_fDeadOccurTime = val;
	}

	public float GetDeadOccurTime()
	{
		return m_fDeadOccurTime;
	}

	public void SetRebornTime( float val )
	{
		m_fRebornTime = val;
	}

	public float GetRebornTime()
	{
		return m_fRebornTime;
	}

	public void SetIntParam( int idx, int val )
	{
		m_aryIntParam [idx] = val;
	}

	public int GetIntParam( int idx )
	{
		return m_aryIntParam [idx];
	}

	public void SetFloatParam( int idx, float val )
	{
		m_aryFloatParam [idx] = val;
	}

	public float GetFloatParam( int idx )
	{
		return m_aryFloatParam [idx];
	}

	public void SetSize( float val )
	{
		m_fSize = val;
		vecTempScale.x = val;
		vecTempScale.y = val;
		vecTempScale.z = 1f;
		this.transform.localScale = vecTempScale;

        m_fRadius = CyberTreeMath.Scale2Radius(m_fSize);

        _sortingGroup.sortingLayerName = "Role";
        _sortingGroup.sortingOrder = CyberTreeMath.Radius2SortingOrder( m_fRadius );
    }

    public float GetTriggerSize()
    {
        return _Trigger.bounds.size.x;
    }

    public float GetSize()
	{
		return m_fSize;
	}

	public void SetSprite( Sprite sprite )
	{
		_srMain.sprite = sprite;
	}

	public void SetColor( Color color )
	{
		_srMain.color = color;
	}

	public void SetColor( string szColor )
	{
		Color color = Color.white;
		if ( !ColorUtility.TryParseHtmlString("#" + szColor, out color)) {
			color = Color.white;
		}
		_srMain.color = color;
	}

    public Color GetColor()
    {
        return _srMain.color;
    }

	public void SetDir( Vector2 dir )
	{
		m_Dir = dir;
	}

	public Vector2 GetDir()
	{
		return m_Dir;
	}

	public void SetPos( Vector3 pos )
	{
		m_Pos = pos;
		this.transform.position = pos;
	}

	public Vector3 GetPos()
	{
        return this.transform.position;
    }

	public float GetRadius()
	{
        //return _srMain.bounds.size.x / 2.0f;
        return m_fRadius;
	}

	public float GetDiameter()
	{
		return _srMain.bounds.size.x;
	}

    public void BeginEject_BySpeed( float fSpeed, float fRunTime)
    {
        float fDistance = fSpeed * fRunTime;
        m_vEjectSpeed.x = fSpeed * m_Dir.x;
        m_vEjectSpeed.y = fSpeed * m_Dir.y;
        m_fjectTotalDis = fDistance;
        m_bEjecting = true;
        m_vEjectStartPos = GetPos();
    }


    float m_fRotation = 0;
    float m_fRotationSpeed = 0;
    public void BeginEject( float fDistance, float fRunTime, float fRotationSpeed = 0)
	{
        float fSpeed = fDistance / fRunTime;
		m_vEjectSpeed.x += fSpeed * m_Dir.x;
		m_vEjectSpeed.y += fSpeed * m_Dir.y;
		m_fjectTotalDis = fDistance;
		m_bEjecting = true;
		m_vEjectStartPos = GetPos ();
        m_fRotation = 0;
        m_fRotationSpeed = fRotationSpeed;
    }
    

    void EjectingLoop()
	{
		if (!m_bEjecting) {
			return;
		}

        if (m_fRotationSpeed != 0f)
        {
            m_fRotation += m_fRotationSpeed;
            _rigid.MoveRotation(m_fRotation);
        }
       

        float fDeltaX = m_vEjectSpeed.x * Time.deltaTime;
		float fDeltaY = m_vEjectSpeed.y * Time.deltaTime;
		vecTempPos = GetPos ();
		vecTempPos.x += fDeltaX;
		vecTempPos.y += fDeltaY;
		SetPos ( vecTempPos );
		if (Vector2.Distance (vecTempPos, m_vEjectStartPos) >= m_fjectTotalDis) {
			EndEject ();
		}
	}

	public bool IsEjecting()
	{
		return m_bEjecting;
	}

	public void EndEject()
	{
		m_bEjecting = false;
        m_vEjectSpeed.x = 0;
        m_vEjectSpeed.y = 0;

    }

	bool m_bNeedSync = false;
	public void SetNeedSync( bool val )
	{
		m_bNeedSync = val;
	}

	public bool GetNeedSync()
	{
		return m_bNeedSync;
	}

  

    public void PushThorn( CMonster monster )
    {

    }

    public float GetSpeed()
    {
        return Mathf.Sqrt(m_vEjectSpeed.x * m_vEjectSpeed.x + m_vEjectSpeed.y * m_vEjectSpeed.y);
    }

    public void SetPlayer( Player player )
    {
        m_Player = player;
    }

    public Player GetPlayer()
    {
        return m_Player;
    }

    public void SetBall( Ball ball )
    {
        m_Ball = ball;
    }

    public Ball GetBall()
    {
        return m_Ball;
    }

    Vector3 m_vecInitPos = new Vector3();
    public void SetInitPos( Vector3 pos )
    {
        m_vecInitPos = pos;
    }

    public Vector3 GetInitPos()
    {
        return m_vecInitPos;
    }

    public bool IsRebornable()
    {
        switch( this.GetMonsterBuildingType() )
        {
            case CMonsterEditor.eMonsterBuildingType.bean:
            case CMonsterEditor.eMonsterBuildingType.pick_skill:
            case CMonsterEditor.eMonsterBuildingType.scene_ball:
            case CMonsterEditor.eMonsterBuildingType.thorn:
                {
                    return true;
                }
                break;

            case CMonsterEditor.eMonsterBuildingType.spore:
                {
                    return false;
                }
                break;
        }

        return false;
    }

    SkeletonAnimation m_aniCiChuXian = null;
    public void SetChuXianAni( SkeletonAnimation ani )
    {
        m_aniCiChuXian = ani;
        m_fCiChuXianTimeLeft = 0.5f;
    }

    public SkeletonAnimation GetCiChuXianAni()
    {
        return m_aniCiChuXian;
    }



    public void SetSelfMainVisible( bool bVisible )
    {
        m_goContainer.SetActive(bVisible);
    }

    float m_fCiChuXianTimeLeft = 0f;
    public bool CiChuXianLoop()
    {
        if (m_aniCiChuXian == null)
        {
           return true;
        }
        else
        {
            m_aniCiChuXian.gameObject.SetActive( true );
        }

        m_fCiChuXianTimeLeft -= Time.deltaTime;
        if (m_fCiChuXianTimeLeft <= 0)
        {
            return true;
        }


        return false;
    }

    public void SetLastPos( Vector3 pos )
    {
        m_vecLastPos = pos;
    }

    public bool IsPosChanged()
    {
        return ( m_vecLastPos.x != GetPos().x || m_vecLastPos.y != GetPos().y );
    }

} // end class
