using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CtrlMode
{
    //public const int CTRL_MODE_NONE          = 0;        
	public const int CTRL_MODE_HAND          = 0;
    public const int CTRL_MODE_JOYSTICK      = 1;    
    public const int CTRL_MODE_SCREEN_CURSOR = 2;        
    public const int CTRL_MODE_WORLD_CURSOR  = 3;
    
    public const int DIR_INDICATOR_NONE  = 0;
    public const int DIR_INDICATOR_ARROW = 1;
    public const int DIR_INDICATOR_LINE  = 2;

	public static int _ctrl_mode = CtrlMode.CTRL_MODE_JOYSTICK;
    
    public static int _dir_indicator_type = CtrlMode.DIR_INDICATOR_LINE;

    const float _view_port_overflow = -0.05f;
    
    const float _joystick_enlarge_factor = 5.0f;
    
	public static float _ball_scale_zoom_factor = 1;//2.2f;

    public static float _world_port_h = 0.0f;
    public static float _world_port_v = 0.0f;
    
    public const float MIN_ORTHOGRAPHIC_SIZE = 20.0f;

    public static float _zoom_velocity = 0.0f;
    public static float _zoom_time = 0.5f;

    public static void SetCtrlMode(int mode)
    {
        if (mode == CtrlMode.CTRL_MODE_JOYSTICK ||
            mode == CtrlMode.CTRL_MODE_WORLD_CURSOR ||
            mode == CtrlMode.CTRL_MODE_SCREEN_CURSOR) {
            ETCInput.SetControlActivated("Joystick", true);
        }
        else{
            ETCInput.SetControlActivated("Joystick", false);
        }
        CtrlMode._ctrl_mode = mode;
        CtrlMode.UpdateWorldPort();
    }

    public static void SetDirIndicatorType(int type)
    {
        CtrlMode._dir_indicator_type = type;
		if (Main.s_Instance && Main.s_Instance.m_MainPlayer) {
			Main.s_Instance.m_MainPlayer.UpdateIndicatorType (type);
		}
    }

    public static int GetDirIndicatorType()
    {
        return CtrlMode._dir_indicator_type;
    }
    
    public static void UpdateWorldPort()
    {
        if (CtrlMode._ctrl_mode == CtrlMode.CTRL_MODE_JOYSTICK) {
            CtrlMode.SetJoystickWorldPort();
        }
        else if (CtrlMode._ctrl_mode == CtrlMode.CTRL_MODE_WORLD_CURSOR ||
                 CtrlMode._ctrl_mode == CtrlMode.CTRL_MODE_SCREEN_CURSOR ||
                 CtrlMode._ctrl_mode == CtrlMode.CTRL_MODE_HAND) {
            CtrlMode.SetCursorWorldPort();
        }
    }
    
    public static void SetJoystickWorldPort()
    {
        const float enlarge_factor = 1.5f;
        Vector3 world_port_lb = new Vector3();
        Vector3 world_port_rt = new Vector3();        
        CtrlMode.CalcViewportToWorldPort(ref world_port_lb, ref world_port_rt);
        CtrlMode._world_port_h = Mathf.Abs(world_port_rt.x - world_port_lb.x) * CtrlMode._joystick_enlarge_factor;
        CtrlMode._world_port_v = Mathf.Abs(world_port_rt.y - world_port_lb.y) * CtrlMode._joystick_enlarge_factor;
	
    }

    public static void SetCursorWorldPort()
    {
        Vector3 world_port_lb = new Vector3();
        Vector3 world_port_rt = new Vector3();        
        CtrlMode.CalcViewportToWorldPort(ref world_port_lb, ref world_port_rt);
        CtrlMode._world_port_h = Mathf.Abs(world_port_rt.x - world_port_lb.x);
        CtrlMode._world_port_v = Mathf.Abs(world_port_rt.y - world_port_lb.y);
    }
    
    public static void CalcViewportToWorldPort(ref Vector3 world_port_lb, ref Vector3 world_port_rt)
    {
        Camera camera = Camera.main;
        
        float view_port_r = camera.rect.xMin - CtrlMode._view_port_overflow;
        float view_port_l = camera.rect.xMax + CtrlMode._view_port_overflow;
        float view_port_t = camera.rect.yMin - CtrlMode._view_port_overflow;
        float view_port_b = camera.rect.yMax + CtrlMode._view_port_overflow;
        
        world_port_lb =
            camera.ViewportToWorldPoint(new Vector3(view_port_l,
                                                    view_port_b,
                                                    0 - camera.transform.position.z));
        world_port_rt =
            camera.ViewportToWorldPoint(new Vector3(view_port_r,
                                                    view_port_t,
                                                    0 - camera.transform.position.z));
    }

    static Vector3 vecTempPos1 = new Vector3();
    static Vector3 vecTempPos2 = new Vector3();

    public const float CAM_XISHU_BELT = 10f;

    bool m_bFirstFuck = true;

    public enum eCamerMoveStatus
    {
        none,    // 无运动
        raise,   // 抬升
        shangfu, // 上浮部分
        guiwei,  // 归位
        down,    // 下降
    };

    static eCamerMoveStatus s_eCamMoveStatus = eCamerMoveStatus.none;

    // 镜头范围控制
    public static float m_fAcctackRadiusMultiple = 6f;
    public static float m_fSpitBallSpeed = 100f;

    static Ball ballLeft = null;
    static Ball ballRight = null;
    static Ball ballTop = null;
    static Ball ballBottom = null;
    static float fLeftBorderPos = 0f;
    static float fRightBorderPos = 0f;
    static float fTopBorderPos = 0f;
    static float fBottomBorderPos = 0f;

    static Vector3 s_balls_min_position;
    static Vector3 s_balls_max_position;

    static float s_fLastDisRaise = 0f;
    static float s_fLastDisDown = 0f;

    public static float s_fMaxCamSize = 100f;

    public static void UpdateCameraViewPort(ref Vector3 balls_min_position,
                                           ref Vector3 balls_max_position)
    {
        if ( CCameraManager.s_Instance.IsRebornCameraLooping() )
        {
            return;
        }

        if ( Main.s_Instance && Main.s_Instance.IsProcessingDead())
        {
            return;
        }

       

        if ( CCheat.s_Instance.IsMannualControlViewSize() )
        {
            Camera.main.orthographicSize = CCheat.s_Instance.GetMannualControlViewSize();
            return;
        }

        /*
        if ( Main.s_Instance.m_MainPlayer.IsUnfolding() && !Main.s_Instance._goCheat._toggleAffectCamWhenUnfold.isOn )
        {
            return;
        }
        */
             
        CamSizeLockLoop();
        if ( IsCamSizeLock() )
        {
            return;
        }


        if ( !MapEditor.s_Instance.IsMapInitCompleted() ||  !Main.s_Instance.m_MainPlayer.IsPlayerInitCompleted() )
        {
            return;
        }
        s_balls_min_position = balls_min_position;
        s_balls_max_position = balls_max_position;

        if (Main.s_Instance.IsCameraSizeProtecting())
        {
            return;
        }

        if (AccountManager.s_bObserve)
        {
            return;
        }

        /*
        if ( Camera.main.orthographicSize < Main.s_Instance.m_fCamMinSize )
        {
            Camera.main.orthographicSize = Main.s_Instance.m_fCamMinSize;
            return;
        }*/


        if ( CCameraManager.s_Instance.IsMaxCamLimit() )
        {
            if (Camera.main.orthographicSize > CCameraManager.s_Instance.GetMaxCamSizeLimit())
            {
                Camera.main.orthographicSize = CCameraManager.s_Instance.GetMaxCamSizeLimit();
            }
        }

        // balls_min_position 相当于World坐标系中队伍中最左下角的球球
        // balls_max_position;相当于World坐标系中队伍中最右上角的球球
       float fLeftBorderPos = balls_min_position.x ;
        float fRightBorderPos = balls_max_position.x;
        float fTopBorderPos = balls_max_position.y;
        float fBottomBorderPos = balls_min_position.y;

        vecTempPos1.x = fLeftBorderPos;
        vecTempPos1.y = fBottomBorderPos;
        vecTempPos2.x = fRightBorderPos;
        vecTempPos2.y = fTopBorderPos;

        vecTempPos1 = Camera.main.WorldToScreenPoint(balls_min_position);
        vecTempPos2 = Camera.main.WorldToScreenPoint(balls_max_position);

        
        if (IsCameraSizeProtecting())
        {
            CameraSizeProtectingLoop();
            return;
        }

        // 当整个队伍中只有一个球的时候，走一个专门的流程来计算队伍攻击范围。不然镜头会抖动
        


        if (vecTempPos1.x < 0 || vecTempPos1.y < 0 || vecTempPos2.x > Screen.width || vecTempPos2.y > Screen.height  )
        {
            s_lstTemp.Clear();
            s_lstTemp.Add(-vecTempPos1.x);
            s_lstTemp.Add(-vecTempPos1.y);
            s_lstTemp.Add(vecTempPos2.x - Screen.width);
            s_lstTemp.Add(vecTempPos2.y - Screen.height);
            float fDis = GetMaxValueInList(s_lstTemp);


	
               BeginRaiseCam(fDis);
               
            
        }

        
        if (vecTempPos1.x > 0 && vecTempPos1.y > 0 && vecTempPos2.x < Screen.width && vecTempPos2.y < Screen.height )
        {
            s_lstTemp.Clear();
            s_lstTemp.Add(vecTempPos1.x);
            s_lstTemp.Add(vecTempPos1.y);
            s_lstTemp.Add(Screen.width - vecTempPos2.x);
            s_lstTemp.Add(Screen.height - vecTempPos2.y);
            float fDis = GetMinValueInList(s_lstTemp);


           BeginDownCam(fDis);
       
        }


    }

    public enum eCamerSizeProtectStatus
    {
        none,
        stay,
        raise,
        stay_before_down,
        down,
    }

    static float s_fCameraProtectTime = 0;
    static float s_fCameraStayBeforeDownTime = 0;
    static eCamerSizeProtectStatus s_eProtectStatus = eCamerSizeProtectStatus.none;
    static float s_fShangFuDestCamSize = 0f;
    static float s_fRangeBeforeProtect = 0;
    static float s_fRangeBeforeProtect2 = 0;
    static float s_fShangFuXiShu = 0;

    static float s_fForecastCameraSizeChangeAmount = 0;
    public const float CHANGE_AMOUNT_XIHSU = 50f; 
    public static float GetChangeAmount()
    {
        return s_fForecastCameraSizeChangeAmount;
    }

    public static bool CheckCameraSizeProtectInfo( ref List<Vector3> lstTempDest )
    {
        float fExceedMinX = 0;
        float fExceedMinY = 0;
        float fExceedMaxX = 0;
        float fExceedMaxY = 0;
        s_fForecastCameraSizeChangeAmount = 0f;
        for (int i = 0; i < lstTempDest.Count; i++)
        {
            float fX = lstTempDest[i].x - lstTempDest[i].z;
            if (fX < s_balls_min_position.x)
            {
                fExceedMinX = s_balls_min_position.x - fX;
            }

            float fY = lstTempDest[i].y - lstTempDest[i].z;
            if (fY < s_balls_min_position.y)
            {
                fExceedMinY = s_balls_min_position.y - fY;
            }

            fX = lstTempDest[i].x + lstTempDest[i].z;
            if (fX > s_balls_max_position.x)
            {
                fExceedMaxX = fX - s_balls_max_position.x;
            }

            fY = lstTempDest[i].y + lstTempDest[i].z;
            if (fY > s_balls_max_position.y)
            {
                fExceedMaxY = fY - s_balls_max_position.y;
            }
        }

        if (fExceedMinX == 0 && fExceedMinY == 0 && fExceedMaxX == 0 && fExceedMaxY == 0)
        {
            Debug.Log("not exceed, so no camera size protect");
            return false;
        }

        s_lstTemp.Clear();
        s_lstTemp.Add(fExceedMinX);
        s_lstTemp.Add(fExceedMinY);
        s_lstTemp.Add(fExceedMaxX);
        s_lstTemp.Add(fExceedMaxY);
        s_fForecastCameraSizeChangeAmount = GetMaxValueInList(s_lstTemp);
        Debug.Log("ready to protect camsize，fExceedAmount = " + s_fForecastCameraSizeChangeAmount);

        return true;
    }

    static float m_fProtectTime = 0f;
    public static void BeginCamSizeLock(float fProtectTime)
    {
        m_fProtectTime = fProtectTime;
    }

    public static bool IsCamSizeLock()
    {
        return m_fProtectTime > 0f;
    }

    static void CamSizeLockLoop()
    {
        if (m_fProtectTime <= 0)
        {
            return;
        }

        m_fProtectTime -= Time.deltaTime;
    }

    public static void BeginCameraSizeProtect( float fProtectTime, float fRaiseSpeed, float fDownSpeed )
    {
        return;

        s_fCameraProtectRaiseSpeed = fRaiseSpeed;
        s_fCameraProtectDownSpeed = 0;// Main.s_Instance.m_fDownSpeed;

        s_fShangFuDestCamSize = -1;
        s_fRangeBeforeProtect2 = 0;
        s_fRangeBeforeProtect = Mathf.Max( s_balls_max_position.x - s_balls_min_position.x, s_balls_max_position.y - s_balls_min_position.y);

        if (s_eProtectStatus > eCamerSizeProtectStatus.none)
        {
         //   return;
        }

        s_eProtectStatus = eCamerSizeProtectStatus.stay;
        s_fCameraProtectTime = fProtectTime;

    
    }

    static void CameraSizeProtectingLoop()
    {
        CameraSizeProtect_StayLoop();
        CameraSizeProtect_RaiseLoop();
        CameraSizeProtect_StayBeforeDownLoop();
        CameraSizeProtect_DownLoop();
    }

    public static bool IsCameraSizeProtecting()
    {
        return s_eProtectStatus > eCamerSizeProtectStatus.none;
    }

    static void CameraSizeProtect_RaiseLoop()
    {
        if (s_eProtectStatus != eCamerSizeProtectStatus.raise)
        {
            return;
        }

             
        if (vecTempPos1.x > 0 && vecTempPos1.y > 0 && vecTempPos2.x < Screen.width  && vecTempPos2.y < Screen.height  )
        {
            s_fCameraProtectRaiseSpeed -= Main.s_Instance.m_fCamRaiseAccelerate * Time.deltaTime;
        }
        


        if (s_fCameraProtectRaiseSpeed <= 0f)
        {
            s_fCameraStayBeforeDownTime = Main.s_Instance.m_fTimeBeforeCamDown;
            s_eProtectStatus = eCamerSizeProtectStatus.stay_before_down;
            
        }
        Camera.main.orthographicSize += s_fCameraProtectRaiseSpeed * Time.deltaTime;
    }

    static void CameraSizeProtect_StayBeforeDownLoop()
    {

        if (s_eProtectStatus != eCamerSizeProtectStatus.stay_before_down)
        {
            return;
        }

        s_fCameraStayBeforeDownTime -= Time.deltaTime;
        if (s_fCameraStayBeforeDownTime <= 0)
        {
            s_eProtectStatus = eCamerSizeProtectStatus.down;
        }

    }

    // right here
    static void CameraSizeProtect_DownLoop()
    {
        if (s_eProtectStatus != eCamerSizeProtectStatus.down)
        {
            return;
        }

        s_fCameraProtectDownSpeed += Main.s_Instance.m_fCamDownAccelerate * Time.deltaTime;


        s_lstTemp.Clear();
        s_lstTemp.Add(vecTempPos1.x);
        s_lstTemp.Add(vecTempPos1.y);
        s_lstTemp.Add(Screen.width - vecTempPos2.x);
        s_lstTemp.Add(Screen.height - vecTempPos2.y);
        float fDis = GetMinValueInList(s_lstTemp);
        float fSpeed = s_fCameraProtectDownSpeed * Time.deltaTime;
        if (fDis < 10)
        {
            fSpeed = fSpeed * fDis / 10;
        }

        if (vecTempPos1.x < 0 || vecTempPos1.y < 0 || vecTempPos2.x > Screen.width || vecTempPos2.y > Screen.height)
        {
            s_eProtectStatus = eCamerSizeProtectStatus.none;
        }
        Camera.main.orthographicSize -= fSpeed * Time.deltaTime;
    }

    static void CameraSizeProtect_StayLoop()
    {
        if (s_eProtectStatus != eCamerSizeProtectStatus.stay)
        {
            return;
        }
        s_fCameraProtectTime -= Time.deltaTime;
        if (s_fCameraProtectTime <= 0f)
        {
            s_fShangFuDestCamSize = -1;
            s_fRangeBeforeProtect2 = 0;
            s_eProtectStatus = eCamerSizeProtectStatus.raise;
        }
    }

    static void EndCameraSizeProtect()
    {
       // Camera.main.orthographicSize = Camera.main.orthographicSize * 1.5f;
    }

    static void BeginRaiseCam(float fDis)
    {
        float fSpeed = Main.s_Instance.m_fCamRespondSpeed * Time.deltaTime;
        if (fDis < 20)
        {
            fSpeed = fSpeed * fDis / 20;
        }
        Camera.main.orthographicSize += fSpeed;
    }


    static void BeginDownCam(float fDis)
    {
        float fSpeed = Main.s_Instance.m_fCamRespondSpeed * Time.deltaTime;
        if (fDis < 20)
        {
            fSpeed = fSpeed * fDis / 20;
        }
        float fSize = Camera.main.orthographicSize - fSpeed;

        if (fSize <= Main.s_Instance.m_fCamMinSize)
        {
            fSize = Main.s_Instance.m_fCamMinSize;
        }

        Camera.main.orthographicSize = fSize;
    }


    static List<float> s_lstTemp = new List<float>();
    static float GetMaxValueInList(List<float> lst)
    {
        float fMax = 0f;
        for (int i = 0; i < lst.Count; i++)
        {
            if (i == 0)
            {
                fMax = lst[i];
                continue;
            }
            if (fMax < lst[i])
            {
                fMax = lst[i];
            }
        }

        return fMax;
    }

    static float GetMinValueInList(List<float> lst)
    {
        float fMin = 0f;
        for (int i = 0; i < lst.Count; i++)
        {
            if (i == 0)
            {
                fMin = lst[i];
                continue;
            }
            if (fMin > lst[i])
            {
                fMin = lst[i];
            }
        }

        return fMin;
    }

    static float s_fCameraSizeChangeSpeed = 30f;

    static float s_fCameraProtectRaiseSpeed = 4f;
    static float s_fCameraProtectDownSpeed = 2.5f;

    /*
public static void UpdateCameraViewPort(Vector3 balls_min_position,
                                        Vector3 balls_max_position)
{
    if (Main.s_Instance.IsCameraSizeProtecting ()) {
        return;
    }

    if (AccountManager.s_bObserve) {
        return;
    }

    Camera camera = Camera.main;
    Vector3 view_port_min = balls_min_position; // camera.WorldToViewportPoint(balls_min_position);
    Vector3 view_port_max = balls_max_position; // camera.WorldToViewportPoint(balls_max_position);
    float view_port_h = Mathf.Abs(view_port_max.x - view_port_min.x);
    float view_port_v = Mathf.Abs(view_port_max.y - view_port_min.y);

    float fMaxCamSize = MapEditor.s_Instance.GetMaxCameraSize();


    view_port_h *= Main.s_Instance.m_fCamSizeXiShu;
    view_port_v *= Main.s_Instance.m_fCamSizeXiShu;
    if (view_port_h > fMaxCamSize)
    {
        view_port_h = fMaxCamSize;
    }

    if (view_port_v > fMaxCamSize)
    {
        view_port_v = fMaxCamSize;
    }
    float orthographic_size = Mathf.Max(view_port_h, view_port_v);

    orthographic_size = Mathf.Max(orthographic_size,
                                  CtrlMode.MIN_ORTHOGRAPHIC_SIZE);




    if (!Mathf.Approximately(Camera.main.orthographicSize, orthographic_size)) {
        Camera.main.orthographicSize =
            Mathf.SmoothDamp(Camera.main.orthographicSize,
                             orthographic_size,
                             ref CtrlMode._zoom_velocity,
                             CtrlMode._zoom_time);
        CtrlMode.UpdateWorldPort();
    }
}    

*/

    public static int GetCtrlMode()
    {
        return CtrlMode._ctrl_mode;
    }
};
