﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISkillCastButton : MonoBehaviour {

    public CFrameAnimationEffect _effectColddownCompleted;

    public Button _btnAddPoint; // “加点”按钮
    public Text _txtCurLevel;   // 本技能的当前等级
    public Image _imgMain;             // 技能图标
    public Text _txtName;
    public Image _imgLevelUp;
    public Image _imgArrowAppear;
    public Text _txtColdDown;
    public Image _imgColdDown;
    public Image _imgLevelRing;

    public CFrameAnimationEffect _effectCanLevelUp;
    public CFrameAnimationEffect _effectAddBtnFly;

    int m_nCurNum = 0;
    int m_nMaxNum = 0;
    int m_nSkillId = 0;

    static Color tempColor = new Color();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        LevelUpEffectLoop();
        //ArrowAppearEffectLoop();
        //AddPointButtonBreatheEffectLoop();

        Flying();
    }

    public void Init(CSkillSystem.sSkillInfo info)
    {
        SetMaxNum(info.nMaxPoint);
        if (info.nSkillId < CSkillSystem.SELECTABLE_SKILL_START_ID)
        {
            m_nSkillId = info.nSkillId;
        }
        else
        {
            m_nSkillId = CSkillSystem.INVALID_SKILL_ID;
        }
    }


    public void SetSkillColdownActive( bool bActive)
    {
         _txtColdDown.gameObject.SetActive(bActive);
        _imgColdDown.gameObject.SetActive(bActive);
    }

    public void SetSkillColdownInfo( float fFillAmount, string szLeftTime )
    {
         _txtColdDown.text = szLeftTime;
         _imgColdDown.fillAmount = fFillAmount;
        
        if (fFillAmount <= 0.002f)
        {
            if (_effectColddownCompleted != null)
            {
                _effectColddownCompleted.BeginPlay(false);
            }

        }
    }


    public void SetPickSkillCounterInfo(CSkillSystem.sSkillInfo info)
    {
        SetMaxNum(info.nMaxPoint);
        m_nSkillId = info.nSkillId;
        /*
        _txtName.gameObject.SetActive( true );
        _txtName.text = CSkillSystem.s_Instance.GetPickSkillNameById(m_nSkillId);
        if (info.nSkillId < CSkillSystem.SELECTABLE_SKILL_START_ID)
        {
            _txtCurLevel.text = info.nCurPoint.ToString();
        }
        else
        {
            _txtCurLevel.text = CSkillSystem.s_Instance.GetSelectSkillCurPoint().ToString();
        }
        */
        _imgMain.sprite = CSkillSystem.s_Instance.GetSkillSprite(m_nSkillId);
    }
    

    void SetMaxNum( int nMaxNum )
    {
        m_nMaxNum = nMaxNum;
    }

    public void OnClickButton_AddPoint()
    {
        if ( m_nSkillId == CSkillSystem.INVALID_SKILL_ID )
        {
           // Main.s_Instance.g_SystemMsg.SetContent("技能ID无效");
            return;
        }


        CSkillSystem.s_Instance.AddPoint(m_nSkillId);

        PlayLevelUpEffect();
    }

    public void SetCurLevel( int nLevel )
    {
        _txtCurLevel.text = nLevel.ToString();
        m_nCurNum = nLevel;
       
        UpdateLevelRing();
    }

    void UpdateLevelRing()
    {
        if (_imgLevelRing == null)
        {
            return;
        }

        float fFillAmount = 0f;
        if ( m_nSkillId < CSkillSystem.SELECTABLE_SKILL_START_ID )
        {
            fFillAmount = (float)m_nCurNum / (float)CSkillSystem.MAIN_SKILL_MAX_POINT;
        }
        else
        {
            fFillAmount = (float)m_nCurNum / (float)CSkillSystem.SELECTABLE_SKILL_START_ID;
        }
        _imgLevelRing.fillAmount = fFillAmount;
    }

    bool m_bAddBtnVisible = false;
    public void SetAddPointButtonVisible(bool bVisible)
    {
        if (m_bAddBtnVisible == bVisible)
        {
            return;
        }

        if (bVisible)
        {
            if (_effectCanLevelUp)
            {
             //   _effectCanLevelUp.BeginPlay( false );
             //   _effectCanLevelUp.gameObject.SetActive(true);
            }

            if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.mobile)
            {
                if (_effectAddBtnFly)
                {
                    _effectAddBtnFly.BeginPlay(false);
                    _effectAddBtnFly.gameObject.SetActive(true);
                }
            }
            else
            {
                _btnAddPoint.gameObject.SetActive(true);
            }

            if (!m_bAddBtnVisible)
            {
                BeginFly();
            }
        }
        else
        {
            if (_effectCanLevelUp)
            {
                _effectCanLevelUp.gameObject.SetActive(false);
            }

            if (_effectAddBtnFly)
            {
                _effectAddBtnFly.gameObject.SetActive(false);
            }

            if (_btnAddPoint)
            {
                _btnAddPoint.gameObject.SetActive(false);
            }
        }

        m_bAddBtnVisible = bVisible;
    }

    bool m_bFlying = false;
    void BeginFly()
    {
        m_bFlying = true;
    }

    void Flying()
    {
        if (_effectAddBtnFly == null)
        {
            return;
        }

        if ( !m_bFlying )
        {
            return;
        }

        if (_effectAddBtnFly.isEnd())
        {
            EndFly();
        }
        
    }

    void EndFly()
    {
        m_bFlying = false;
       _btnAddPoint.gameObject.SetActive(true);
    }

    float m_fArrowAppearEffectTimeCount = 0f;
    int m_nArrowAppearEffectSpriteIdx = -1;
    public void PlayArrowAppearEffect()
    {

        return;
        if (_imgArrowAppear == null)
        {
            return;
        }

        _imgArrowAppear.gameObject.SetActive( true );
        m_nArrowAppearEffectSpriteIdx = 0;
        m_fArrowAppearEffectTimeCount = 0;
    }

    void ArrowAppearEffectLoop()
    {
        return;

        if (m_nArrowAppearEffectSpriteIdx < 0)
        {
            return;
        }
        if (CSkillSystem.s_Instance == null)
        {
            return;
        }


        if (_btnAddPoint == null || _imgArrowAppear == null )
        {
            return;
        }

        m_fArrowAppearEffectTimeCount += Time.deltaTime;
        if (m_fArrowAppearEffectTimeCount < 0.07f)
        {
            return;
        }


        if (m_nArrowAppearEffectSpriteIdx >= CSkillSystem.s_Instance._arySkillArrowAppearSprites.Length)
        {
            m_nArrowAppearEffectSpriteIdx = -1;
            _btnAddPoint.gameObject.SetActive( true );
            _imgArrowAppear.gameObject.SetActive( false );
            return;
        }

        m_fArrowAppearEffectTimeCount = 0;
        _imgArrowAppear.sprite = CSkillSystem.s_Instance._arySkillArrowAppearSprites[m_nArrowAppearEffectSpriteIdx];
        m_nArrowAppearEffectSpriteIdx++;
    }

    int m_nBreatheSpriteIdx = 0;
    float m_fBreatheTimeCount = 0f;
    void AddPointButtonBreatheEffectLoop()
    {
        if ( _btnAddPoint == null )
        {
            return;
        }

        if (CSkillSystem.s_Instance == null)
        {
            return;
        }

        m_fBreatheTimeCount += Time.deltaTime;
        if (m_fBreatheTimeCount < 0.05f )
        {
            return;
        }
        m_fBreatheTimeCount = 0f;

        if (m_nBreatheSpriteIdx >= CSkillSystem.s_Instance._arySkillArrowBreatheSprites.Length)
        {
            m_nBreatheSpriteIdx = 0;
        }

        _btnAddPoint.image.sprite = CSkillSystem.s_Instance._arySkillArrowBreatheSprites[m_nBreatheSpriteIdx];
        m_nBreatheSpriteIdx++;

    }

    // 设置该技能按钮此刻可用不
    public void SetAvailable( bool bAvailable )
    {
        if (_imgMain == null)
        {
            return;
        }

        if (bAvailable)
        {
            tempColor = _imgMain.color;
            tempColor.a = 1f;
            _imgMain.color = tempColor;

            tempColor = _txtCurLevel.color;
            tempColor.a = 1f;
            _txtCurLevel.color = tempColor;

            if ( _txtName )
            {
                tempColor = _txtName.color;
                tempColor.a = 1f;
                _txtName.color = tempColor;
            }
        }
        else
        {
            tempColor = _imgMain.color;
            tempColor.a = 0.5f;
            _imgMain.color = tempColor;

            tempColor = _txtCurLevel.color;
            tempColor.a = 0.5f;
            _txtCurLevel.color = tempColor;

            if (_txtName)
            {
                tempColor = _txtName.color;
                tempColor.a = 0.5f;
                _txtName.color = tempColor;
            }
        }
    }

    public void RefreshStatus()
    {
        int nShit = m_nSkillId;

        // 未激活
        // SkillId为0的时候是“吐孢子”技能，是常态有效技能，不需要激活
        if (m_nSkillId != 0 && m_nCurNum == 0) 
        {
            SetAvailable(false);
            return;
        }

        if ( m_nSkillId == CSkillSystem.INVALID_SKILL_ID ) // 技能ID无效
        {
            SetAvailable(false);
            return;
        }


        if ( CSkillSystem.s_Instance.IsSkillColdDowning((CSkillSystem.eSkillId)m_nSkillId) )
        {
            SetAvailable(false);
            return;

        }


        // 符合了所有的条件，技能图标才亮起
        SetAvailable(true);
    }

    int m_nLevelUpEffectSpriteIdx = -1;
    float m_fLevelUpEffectTimeCount = 0f;
    public void PlayLevelUpEffect()
    {
        m_nLevelUpEffectSpriteIdx = 0;
        m_fLevelUpEffectTimeCount = 0f;
      //  _imgLevelUp.gameObject.SetActive(true);
    }

    void LevelUpEffectLoop()
    {
        return;

        if (CSkillSystem.s_Instance == null)
        {
            return;
        }


        if (_imgLevelUp == null)
        {
            return;
        }

        if (m_nLevelUpEffectSpriteIdx < 0)
        {
            return;
        }
        m_fLevelUpEffectTimeCount += Time.deltaTime;
        if (m_fLevelUpEffectTimeCount < 0.05f)
        {
            return;
        }
   

        if (m_nLevelUpEffectSpriteIdx >= CSkillSystem.s_Instance._arySkillLevelUpSprites.Length)
        {
            m_nLevelUpEffectSpriteIdx = -1;
            _imgLevelUp.gameObject.SetActive( false );
            return;
        }

        m_fLevelUpEffectTimeCount = 0;
        _imgLevelUp.sprite = CSkillSystem.s_Instance._arySkillLevelUpSprites[m_nLevelUpEffectSpriteIdx];
        m_nLevelUpEffectSpriteIdx++;
    }
}
