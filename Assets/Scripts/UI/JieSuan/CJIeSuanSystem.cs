﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CJIeSuanSystem : MonoBehaviour {

    public static CJIeSuanSystem s_Instance = null;

    private void Awake()
    {
        s_Instance = this;
    }

    public enum eJieSuanType
	{
		e_gerenjiesuan, // 个人结算
	};

    public GameObject[] m_aryJieSuanPanels;
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetJieSuanPanelVisible( eJieSuanType ePanelType, bool bVisible )
    {
        m_aryJieSuanPanels[(int)ePanelType].SetActive(bVisible);
    }

    public void OnClickButton_BackToLobby()
    {
        Main.s_Instance.groupHashtable();
        StartCoroutine("LoadScene", "SelectCaster");
    }

    //异步加载场景  
    IEnumerator LoadScene(string scene_name)
    {
        AccountData.s_Instance.asyncLoad = SceneManager.LoadSceneAsync(scene_name);

        while (!AccountData.s_Instance.asyncLoad.isDone)
        {
            yield return null;
        }


    }
                                         
}
