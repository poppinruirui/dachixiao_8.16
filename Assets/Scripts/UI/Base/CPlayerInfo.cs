﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CPlayerInfo : MonoBehaviour {

    public Text _txtPlayerName;
    public Text _txtPlayerLevel;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetPlayerName( string szPlayerName )
    {
        _txtPlayerName.text = szPlayerName;
    }

    public void SetPlayerLevel( int nPlayerLevel )
    {
        _txtPlayerLevel.text = "lv." + nPlayerLevel;
    }
}
