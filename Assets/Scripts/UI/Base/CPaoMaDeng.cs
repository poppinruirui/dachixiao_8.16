﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CPaoMaDeng : MonoBehaviour {

    static Vector3 VecTempPos = new Vector3();

    public float m_fSpeed = 0f;
    public float m_fStartPos = 0f;
    public float m_fDestPos = 0f;
    public float m_fInterval = 0;

    /// <summary>
    /// /UI
    /// </summary>
    public Text _txtContent;

    List<string> m_lstContents = new List<string>();
    public string[] m_aryContents;

    bool m_bWaiting = true;
    bool m_bRunnig = false;
    int m_nIndex = 0;
	// Use this for initialization
	void Start () {
       for ( int i = 0; i < m_aryContents.Length; i++ )
        {
            m_lstContents.Add(m_aryContents[i]);
        }
    }
	
	// Update is called once per frame
	void Update () {
        RunLoop();
        WaitLoop();

    }

    public void SetContent( int nIndex, string szContent )
    {
        m_aryContents[nIndex] = szContent;
    }

    float m_fTimeElapse = 1000f;

    void RunLoop()
    {
        if (!m_bRunnig)
        {
            return;
        }

        VecTempPos = _txtContent.transform.localPosition;
        VecTempPos.x += m_fSpeed * Time.deltaTime;
        _txtContent.transform.localPosition = VecTempPos;
        if (m_fSpeed < 0)
        {
            if (VecTempPos.x < m_fDestPos )
            {
                m_bWaiting = true;
                m_bRunnig = false;
            }
        }
    }



    void WaitLoop()
    {
        if (!m_bWaiting)
        {
            return;
        }

        if (m_lstContents.Count == 0)
        {
            return;
        }

        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse < m_fInterval)
        {
            return;
        }
        m_fTimeElapse = 0f;

        if (m_nIndex >= m_lstContents.Count)
        {
            m_nIndex = 0;
        }
        _txtContent.text = m_lstContents[m_nIndex++];
        VecTempPos.x = m_fStartPos;
        VecTempPos.y = 0;
        VecTempPos.z = 0;
        _txtContent.transform.localPosition = VecTempPos;


        m_bWaiting = false;
        m_bRunnig = true;
    }

}
