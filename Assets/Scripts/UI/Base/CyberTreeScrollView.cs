﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberTreeScrollView : MonoBehaviour {

    static Vector3 vecTempScale = new Vector3();

    public GameObject m_goContent;

    List<GameObject> m_lstItems = new List<GameObject>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AddItem( GameObject obj, float fScale = 1f )
    {
        obj.transform.SetParent(m_goContent.transform);
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1;
        obj.transform.localScale = vecTempScale;
        m_lstItems.Add(obj);
    }

    public void ClearItems()
    {
        for ( int i = m_lstItems.Count - 1; i >= 0; i-- )
        {
            m_lstItems[i].SetActive( false );
        }
        m_lstItems.Clear();
    }
}
