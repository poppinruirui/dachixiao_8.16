﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CCosmosButton : Button
{
    static Vector3 vecTempScale = new Vector3();



    // Use this for initialization
    void Start()
    {
        this.onClick.AddListener(OnClickMe);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
       
    }

    public void OnClickMe()
    {
        // 播放默认的按钮音效
        if (CAudioManager.s_Instance)
        {
            CAudioManager.s_Instance.PlaySE(CAudioManager.eSE.anniu);
        }
    }

    Vector3 GetScale()
    {
        return this.transform.localScale;
    }

    void SetScale( Vector3 scale )
    {
 
        this.transform.localScale = scale;
    }

   






}