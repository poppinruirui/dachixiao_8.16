﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshShape : MonoBehaviour
{
    public static Matrix4x4 matrix = Matrix4x4.TRS(new Vector2(0.5f, 0.5f), Quaternion.identity, new Vector2(0.5f, 0.5f));

    public float springForce = 20.0f;
    public float damping = 5.0f;
    public float uniformScale;
    //public GameObject gameObject;
    public Mesh mesh1;
    public MeshRenderer meshRenderer;
    public MeshFilter meshFilter;
    public List<Vector3> vertices;
    public List<int> triangles;
    public List<Color> colors;
    public List<Vector2> uvs;
    public bool isFillPolygon;
    public bool isFillColor;
    public float animationAngle;
    public Vector3[] animationVertices;
    public List<Vector3> orignalVertices;
    public Vector3[] distortVertices;
    public Vector3[] distortVerticesVelocities;
    public Vector2[] animationUVs;
    public float animationOuterRadius;
    public float animationInnerRadius;
    public float animationStarrines;
    public float animationNoiseOffset;
    public float animationNoiseSpeed;
    public float animationNoiseFrequency;
    public float animationNoiseAmplitude;

    
    void Awake()
    {
        if (this.meshRenderer != null)
        {
            return;
        }

        // this.gameObject = new GameObject("MeshShape");
        this.meshRenderer =  this.gameObject.AddComponent<MeshRenderer>();

        /*
        if (this.meshRenderer == null)
        {
            return;
        }
        */

        this.meshFilter =  this.gameObject.AddComponent<MeshFilter>();
        this.meshFilter.mesh = this.mesh1 = new Mesh();
        this.mesh1.MarkDynamic();
        this.orignalVertices = new List<Vector3>();
        this.distortVertices = null;
        this.distortVerticesVelocities = null;
        this.vertices = new List<Vector3>();
        this.triangles = new List<int>();
        this.colors = new List<Color>();
        this.uvs = new List<Vector2>();
        this.isFillPolygon = false;
        this.isFillColor = false;
        this.animationVertices = this.mesh1.vertices;
        this.animationUVs = this.mesh1.uv;
        this.animationStarrines = 0.05f;
        this.animationNoiseOffset = 0.0f;
        this.animationNoiseSpeed = 0.8f;
        this.animationNoiseFrequency = 4.0f;
        this.animationNoiseAmplitude = 0.15f;

        /*
        SetCoreParams(ResourceManager.s_Instance.m_Perlin,
                                false,
                                null,
                                ResourceManager.NUM_OF_SEGMENTS,
                                3.0f,
                                0.0f,
                                360,
                                0,
                                0,
                                0.3f
            );
        drawPolygon();
        */
    }

    private void FixedUpdate()
    {
        
    }

    public void drawDashedLine(Color color, int numOfSegments, float lineWidth, float segmentLength)
    {
        this.mesh1.Clear();
        this.vertices.Clear();
        this.triangles.Clear();
        this.colors.Clear();
        int numOfVisibleSegments = 0;
        for (int i = 0; i < numOfSegments; ++i)
        {
            if (i % 2 == 0)
            {
                this.vertices.Add(new Vector3(0, 0 + segmentLength * i, 0));
                this.vertices.Add(new Vector3(lineWidth, 0 + segmentLength * i, 0));
                this.vertices.Add(new Vector3(0, 0 + segmentLength * (i + 1), 0)); ;
                this.vertices.Add(new Vector3(lineWidth, 0 + segmentLength * (i + 1), 0));
                this.colors.Add(color);
                this.colors.Add(color);
                this.colors.Add(color);
                this.colors.Add(color);
                this.triangles.Add(0 + 4 * numOfVisibleSegments);
                this.triangles.Add(2 + 4 * numOfVisibleSegments);
                this.triangles.Add(1 + 4 * numOfVisibleSegments);
                this.triangles.Add(2 + 4 * numOfVisibleSegments);
                this.triangles.Add(3 + 4 * numOfVisibleSegments);
                this.triangles.Add(1 + 4 * numOfVisibleSegments);
                numOfVisibleSegments++;
            }
        }
        this.mesh1.SetVertices(this.vertices);
        this.animationVertices = this.mesh1.vertices;
        this.animationUVs = this.mesh1.uv;
        this.mesh1.SetColors(this.colors);
        this.mesh1.SetTriangles(this.triangles, 0);
        this.mesh1.RecalculateNormals();
    }

    
    public ImprovedPerlin m_Perlin;
    public bool m_bDahsed;
    public Color? m_Color;
    public int m_numOfSegments;
    public float m_outterRadius;
    public float m_innerRadius = 0.0f;
    public float m_angle1 = 360.0f;
    public float m_angle2 = 0.0f;
    public float m_starrines = 0.0f;
    public float m_rotation = 0.0f;
    public void SetCoreParams(ImprovedPerlin perlin,
                            bool dashed,
                            Color? color,
                            int numOfSegments,
                            float outterRadius,
                            float innerRadius = 0.0f,
                            float angle1 = 360.0f,
                            float angle2 = 0.0f,
                            float starrines = 0.0f,
                            float rotation = 0.0f)
    {
        m_Perlin = perlin;
        m_bDahsed = dashed;
        m_numOfSegments = numOfSegments;
        m_outterRadius = outterRadius;
        m_innerRadius = innerRadius;
        m_angle1 = angle1;
        m_angle2 = angle2;
        m_starrines = starrines;
        m_rotation = rotation;
        m_Color = color;
    }

    public void SetSortingOrder( int nSortingId )
    {
        if (this.meshRenderer == null)
        {
            return;
        }

        this.meshRenderer.sortingOrder = nSortingId;
    }

    public void SetSortingLayer( string szLayer )
    {
        if (this.meshRenderer == null)
        {
            return;
        }

        this.meshRenderer.sortingLayerName = szLayer;
    }

    public void SetColor( Color color )
    {
        m_Color = color;
    }

    public void SetAngle1( float fAngle1 )
    {
        m_angle1 = fAngle1;
    }

    public void drawPolygon()
    {
        float radiusPlus = 0.0f, radian = 0.0f, angle = 0.0f;
        int realNumOfSegments = m_numOfSegments;
        if (m_angle1 != 360.0f)
        {
            ++realNumOfSegments;
        }
        this.mesh1.Clear();
        this.orignalVertices.Clear();
        this.vertices.Clear();
        if (m_Color != null)
        {
            this.colors.Clear();
            this.isFillColor = true;
        }
        else
        {
            this.uvs.Clear();
            this.isFillColor = false;
        }
        this.triangles.Clear();
        this.uniformScale = this.gameObject.transform.localScale.x;
        this.animationStarrines = m_starrines;
        this.animationOuterRadius = m_outterRadius;
        this.animationInnerRadius = m_innerRadius;
        this.animationAngle = m_angle1;
        // center 
        if (this.animationInnerRadius == 0)
        {
            this.isFillPolygon = true;
            this.orignalVertices.Add(Vector3.zero);
            this.vertices.Add(Vector3.zero);
            if (m_Color != null)
            {
                this.colors.Add(m_Color.Value);
            }
            else
            {
                this.uvs.Add(MeshShape.matrix.MultiplyPoint3x4(Vector2.zero));
            }
        }
        else
        {
            this.isFillPolygon = false;
        }
        for (int i = 0; i < realNumOfSegments; i++)
        {
            // outter 
            radiusPlus = 0.0f;
            angle = m_angle1 / m_numOfSegments * i + m_rotation;
            radian = angle * Mathf.Deg2Rad;
            float cosRadian = Mathf.Cos(radian);
            float sinRadian = Mathf.Sin(radian);
            float perlinNoise = 
                m_Perlin.Noise(cosRadian * this.animationNoiseFrequency + 1.0f,
                             sinRadian * this.animationNoiseFrequency + 1.0f,
                             this.animationNoiseOffset) * this.animationNoiseAmplitude;
            if (this.animationStarrines > 0)
            {
                radiusPlus = this.animationOuterRadius * this.animationStarrines * perlinNoise;
            }
            this.orignalVertices.Add(new Vector2(cosRadian * (this.animationOuterRadius),
                                                 sinRadian * (this.animationOuterRadius)));
            this.vertices.Add(new Vector2(cosRadian * (this.animationOuterRadius + radiusPlus),
                                          sinRadian * (this.animationOuterRadius + radiusPlus)));
            if (m_Color != null)
            {
                this.colors.Add(m_Color.Value);
            }
            else
            {
                this.uvs.Add(MeshShape.matrix.MultiplyPoint3x4(new Vector2((cosRadian * this.animationOuterRadius + cosRadian * radiusPlus) /
                                                                           (this.animationOuterRadius * (1.0f + this.animationStarrines * 0.5f * this.animationNoiseAmplitude)),
                                                                           (sinRadian * this.animationOuterRadius + sinRadian * radiusPlus) /
                                                                           (this.animationOuterRadius * (1.0f + this.animationStarrines * 0.5f * this.animationNoiseAmplitude)))));
            }
            // inner 
            radiusPlus = 0.0f;
            if (this.animationInnerRadius != 0)
            {
                if (this.animationStarrines > 0)
                {
                    radiusPlus = this.animationInnerRadius * this.animationStarrines * perlinNoise;
                }
                this.orignalVertices.Add(new Vector2(cosRadian * (this.animationInnerRadius),
                                                     sinRadian * (this.animationInnerRadius)));
                this.vertices.Add(new Vector2(cosRadian * (this.animationInnerRadius + radiusPlus),
                                              sinRadian * (this.animationInnerRadius + radiusPlus)));
                if (m_Color != null)
                {
                    this.colors.Add(m_Color.Value);
                }
                else
                {
                    this.uvs.Add(MeshShape.matrix.MultiplyPoint3x4(new Vector2(cosRadian *
                                                                               ((this.animationInnerRadius + radiusPlus) /
                                                                                (this.animationInnerRadius * (1.0f + this.animationStarrines * 0.5f * this.animationNoiseAmplitude))),
                                                                               sinRadian *
                                                                               ((this.animationInnerRadius + radiusPlus) /
                                                                                (this.animationInnerRadius * (1.0f + this.animationStarrines * 0.5f * this.animationNoiseAmplitude))))));
                }
            }
            if (!m_bDahsed || angle <= m_angle2 || i % 2 == 0)
            {
                if (m_angle1 != 360.0f && i == m_numOfSegments)
                {
                    continue;
                }
                else
                {
                    if (this.animationInnerRadius == 0)
                    {
                        this.triangles.Add(0);
                        this.triangles.Add((i == realNumOfSegments - 1) ? 1 : (i + 2));
                        this.triangles.Add((i + 1));
                    }
                    else
                    {
                        this.triangles.Add((i * 2 + 0));
                        this.triangles.Add((i * 2 + 1));
                        this.triangles.Add((i == realNumOfSegments - 1) ? 0 : (i * 2 + 2));
                        this.triangles.Add((i * 2 + 1));
                        this.triangles.Add((i == realNumOfSegments - 1) ? 1 : (i * 2 + 3));
                        this.triangles.Add((i == realNumOfSegments - 1) ? 0 : (i * 2 + 2));
                    }
                }
            }
        }
        this.distortVertices = this.vertices.ToArray();
        this.distortVerticesVelocities = new Vector3[this.distortVertices.Length];
        this.mesh1.SetVertices(this.vertices);
        this.mesh1.SetTriangles(this.triangles, 0);
        if (m_Color != null)
        {
            this.mesh1.SetColors(this.colors);
        }
        else
        {
            this.mesh1.SetUVs(0, this.uvs);
            this.animationUVs = this.mesh1.uv;
        }
        this.mesh1.RecalculateNormals();
    }

    public void AddDistortForce(Vector3 point, float force)
    {
        for (int i = 0; i < this.distortVertices.Length; ++i)
        {
            Vector3 pointToVertex = this.distortVertices[i] - point;
            pointToVertex *= this.uniformScale;
            float attenuatedForce = force / (1.0f + pointToVertex.sqrMagnitude);
            float velocity = attenuatedForce * Time.deltaTime;
            this.distortVerticesVelocities[i] = pointToVertex.normalized * velocity;
        }
    }

    public void animateDistortVertices()
    {
        if (this.isFillPolygon)
        {
            for (int i = 1; i < this.distortVertices.Length; ++i)
            {
                Vector3 velocity = this.distortVerticesVelocities[i];
                Vector3 displace = this.distortVertices[i] - this.orignalVertices[i];
                displace *= this.uniformScale;
                velocity -= displace * springForce * Time.deltaTime;
                velocity *= 1f - damping * Time.deltaTime;
                this.distortVerticesVelocities[i] = velocity;
                this.distortVertices[i] += velocity * (Time.deltaTime / this.uniformScale);
            }
        }
        else
        {
            for (int i = 0; i < this.distortVertices.Length; ++i)
            {
                Vector3 velocity = this.distortVerticesVelocities[i];
                Vector3 displace = this.distortVertices[i] - this.orignalVertices[i];
                displace *= this.uniformScale;
                velocity -= displace * springForce * Time.deltaTime;
                velocity *= 1f - damping * Time.deltaTime;
                this.distortVerticesVelocities[i] = velocity;
                this.distortVertices[i] += velocity * (Time.deltaTime / this.uniformScale);
            }
        }
    }

    public void animate(ImprovedPerlin perlin)
    {
        this.animateDistortVertices();
        this.animationVertices = this.distortVertices;
        /*        
        this.animationNoiseOffset += Time.deltaTime * this.animationNoiseSpeed;
        float radiusPlus = 0.0f, radian = 0.0f, angle = 0.0f;
        if (this.isFillPolygon) {
            for (int i=1; i<this.animationVertices.Length; ++i) {
                radiusPlus = 0.0f;
                angle = this.animationAngle / (this.animationVertices.Length - 1) * i;
                radian = angle * Mathf.Deg2Rad;
                float cosRadian = Mathf.Cos(radian);
                float sinRadian = Mathf.Sin(radian);
                float perlinNoise =
                    perlin.Noise(cosRadian * this.animationNoiseFrequency + 1.0f, sinRadian * this.animationNoiseFrequency + 1.0f, this.animationNoiseOffset) * this.animationNoiseAmplitude;
                if (this.animationStarrines > 0) {
                    radiusPlus = this.animationOuterRadius * this.animationStarrines * perlinNoise;
                    // Debug.Log(radiusPlus);
                }
                this.animationVertices[i] = new Vector2(cosRadian * this.animationOuterRadius + cosRadian * radiusPlus,
                                                        sinRadian * this.animationOuterRadius + sinRadian * radiusPlus);
                if (!this.isFillColor) {
                    this.animationUVs[i] = MeshShape.matrix.MultiplyPoint3x4(new Vector2((cosRadian * this.animationOuterRadius + cosRadian * radiusPlus) /
                                                                                         (this.animationOuterRadius * (1.0f + this.animationStarrines * 0.5f * this.animationNoiseAmplitude)),
                                                                                         (sinRadian * this.animationOuterRadius + sinRadian * radiusPlus) /
                                                                                         (this.animationOuterRadius * (1.0f + this.animationStarrines * 0.5f * this.animationNoiseAmplitude))));
                }
            }
        }
        else {
            for (int i=0; i<this.animationVertices.Length; i+=2) {
                radiusPlus = 0.0f;
                angle = this.animationAngle / (this.animationVertices.Length - 1) * i;
                radian = angle * Mathf.Deg2Rad;
                float cosRadian = Mathf.Cos(radian);
                float sinRadian = Mathf.Sin(radian);                
                float perlinNoise = perlin.Noise(cosRadian * this.animationNoiseFrequency + 1.0f,
                                                 sinRadian * this.animationNoiseFrequency + 1.0f,
                                                 this.animationNoiseOffset) * this.animationNoiseAmplitude;
                if (this.animationStarrines > 0) {
                    radiusPlus = this.animationOuterRadius * this.animationStarrines * perlinNoise;
                    // Debug.Log(radiusPlus);
                }
                this.animationVertices[i] = new Vector2(cosRadian * this.animationOuterRadius + cosRadian * radiusPlus,
                                                        sinRadian * this.animationOuterRadius + sinRadian * radiusPlus);
                if (!this.isFillColor) {
                 
                    this.animationUVs[i] = MeshShape.matrix.MultiplyPoint3x4(new Vector2((cosRadian * this.animationOuterRadius + cosRadian * radiusPlus) /
                                                                                         (this.animationOuterRadius * (1.0f + this.animationStarrines * 0.5f * this.animationNoiseAmplitude)),
                                                                                         (sinRadian * this.animationOuterRadius + sinRadian * radiusPlus) /
                                                                                         (this.animationOuterRadius * (1.0f + this.animationStarrines * 0.5f * this.animationNoiseAmplitude))));
                }
                if (this.animationStarrines > 0) {
                    radiusPlus = this.animationInnerRadius * this.animationStarrines * perlinNoise;
                }
                this.animationVertices[i + 1] = new Vector2(cosRadian * this.animationInnerRadius + cosRadian * radiusPlus,
                                                            sinRadian * this.animationInnerRadius + sinRadian * radiusPlus);
                if (!this.isFillColor) {
                    this.animationUVs[i + 1] = MeshShape.matrix.MultiplyPoint3x4(new Vector2((cosRadian * this.animationInnerRadius + cosRadian * radiusPlus) /
                                                                                             (this.animationInnerRadius * (1.0f + this.animationStarrines * 0.5f * this.animationNoiseAmplitude)),
                                                                                             (sinRadian * this.animationInnerRadius + sinRadian * radiusPlus) /
                                                                                             (this.animationInnerRadius * (1.0f + this.animationStarrines * 0.5f * this.animationNoiseAmplitude))));
                }
            }
        }
*/
        this.mesh1.vertices = this.animationVertices;
        if (!this.isFillColor)
        {
            this.mesh1.uv = this.animationUVs;
        }
        this.mesh1.RecalculateNormals();
    }
};