﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBoWenGrid : MonoBehaviour
{
    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    // Use this for initialization
    void Start()
    {
        /*
        for ( int i = 0; i < 6; i++ )
        {
           
            switch( i )
            {
                case 0:
                    {
                        vecTempPos.x = 0.45f;
                        vecTempPos.y = 0.479f;
                    }
                    break;
                case 1:
                    {
                        continue;
                        vecTempPos.x = -0.55f;
                        vecTempPos.y = 0.479f;
                    }
                    break;
                case 2:
                    {
                        continue;
                        vecTempPos.x = -0.306f;
                        vecTempPos.y = 0.479f;
                    }
                    break;
                case 3:
                    {
                        vecTempPos.x = -0.053f;
                        vecTempPos.y = 0.479f;
                    }
                    break;
                case 4:
                    {
                        vecTempPos.x = 0.205f;
                        vecTempPos.y = 0.479f;
                    }
                    break;
                case 5:
                    {
                        vecTempPos.x = 0.694f;
                        vecTempPos.y = 0.479f;
                    }
                    break;
            } // end switch
            CBoWenGroup group = GameObject.Instantiate(CStripeManager.s_Instance.m_preBoWenGroup).GetComponent<CBoWenGroup>();
            group.transform.SetParent( this.transform );
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            group.transform.localScale = vecTempScale;
            group.transform.localPosition = vecTempPos;
        }
        */
        for (int i = 0; i < 4; i++ )
        {
            for (int j = 0; j < 4; j++)
            {
                CBoWen bowen = GameObject.Instantiate(CStripeManager.s_Instance.m_preBoWen).GetComponent<CBoWen>();
               
               
                bowen.transform.SetParent(this.transform) ;
                vecTempScale.x = 0.09766f;
                vecTempScale.y = 0.09766f;
                vecTempScale.z = 1f;

                bowen.transform.localScale = vecTempScale;
                vecTempPos.x = i * bowen.GetSize();
                vecTempPos.y = j * bowen.GetSize();
                bowen.transform.localPosition = vecTempPos;
                 
            } // end for j
        } // end for i
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void SetPos(Vector3 pos)
    {
        this.transform.localPosition = pos;
    }

    public void SetScale(float fScale)
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    public void SetColor(Color color)
    {
        foreach (Transform child in this.transform)
        {
            CBoWenGroup bowengroup = child.gameObject.GetComponent<CBoWenGroup>();
            bowengroup.SetColor(color);
        }
    }


}