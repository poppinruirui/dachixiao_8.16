﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTriggerForEatSpore : MonoBehaviour {

	public Ball _ball;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		CSpore spore = other.gameObject.GetComponent<CSpore>();
		this._ball.PreEatSpore(spore);
	}
}
