﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTriggerForDust : MonoBehaviour {

    public Ball _ball;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        CDust dust = other.gameObject.GetComponent<CDust>();
        if ( dust != null )
        {
            _ball._Player.AddDustToSyncPool( dust );
            if ( _ball.IsEjecting() )
            {
                _ball.SlowDownEject();
            }
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        CDust dust = other.gameObject.GetComponent<CDust>();
        if (dust != null)
        {
            _ball._Player.RemoveDustFromSyncPool(dust);
        }
    }


}
