﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YinLangTrigger : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempPos1= new Vector3();

    public Ball _ball;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        Ball ballOpponent = other.transform.parent.gameObject.GetComponent<Ball>();
        vecTempPos = ballOpponent.GetPos() - _ball.GetPos();
        vecTempPos.Normalize();

        if ( Ball.IsTeammate( ballOpponent, _ball ) )
        {
            return;
        }

        if (_ball._Player.IsYinLangBaoFa())
        {
            float S = 30f;//CSkillSystem.s_Instance.m_fYinLangTanKaiXiShu 
            float t = 0.5f;
            float A = CyberTreeMath.GetA(S, t);
            float V0 = CyberTreeMath.GetV0(S, t);
            if (ballOpponent._Player.IsMainPlayer() )
            {
                if ( ballOpponent._Player.IsSneaking() ) // 潜行
                {
                    //Debug.Log( "潜行，不能弹开1111" );
                }
                else if ( ballOpponent._Player.IsMagicShielding() ) // 光子屏障
                {
                    ballOpponent._Player.photonView.RPC("PRC_TiaoZi_GuangZiPingZhangMianYi", PhotonTargets.All, _ball._Player.GetOwnerId(), ballOpponent._Player.GetOwnerId(), ballOpponent.GetPos().x, ballOpponent.GetPos().y);
       
                }
                else
                {
                    ballOpponent._Player.AddYinLangTanKai(ballOpponent.GetIndex(), vecTempPos.x, vecTempPos.y, A, V0);
                }

               
            }
        }
        else
        {
            if (ballOpponent._Player.IsSneaking())
            {

            }
            else if (ballOpponent._Player.IsMagicShielding()) // 光子屏障
            {
                ballOpponent._Player.photonView.RPC("PRC_TiaoZi_GuangZiPingZhangMianYi", PhotonTargets.All, _ball._Player.GetOwnerId(), ballOpponent._Player.GetOwnerId(), ballOpponent.GetPos().x, ballOpponent.GetPos().y);

            }
            else
            {
                
                _ball.Local_Begin_Shell_Temp();
                ballOpponent.Local_Begin_Shell_Temp();

                ballOpponent.ChangeRoleFace(CRoleManager.eRoleFaceType.being_affected);


                if (_ball.CanShowTiaoZi(Ball.eTiaoZiType.e_tankai))
                {
                    //// “弹开”跳字
                    CTiaoZi tiaozi = CTiaoZiManager.s_Instance.NewTiaoZi();

                    vecTempPos1 = ballOpponent.GetPos() - _ball.GetPos();
                    vecTempPos1.Normalize();
                    vecTempPos = _ball.GetPos();
                    vecTempPos.x += vecTempPos1.x * _ball.GetSize() * 0.5f;
                    vecTempPos.y += vecTempPos1.y * _ball.GetSize() * 0.5f;


                    tiaozi.SetPos(vecTempPos);

                    float fTiaoZiScale = CTiaoZiManager.s_Instance.m_fTiaoZiCamThreshold_TanKai * CCameraManager.s_Instance.GetCurCameraSize();
                    tiaozi.SetScale(fTiaoZiScale);
                    tiaozi.SetTextAlign(0, TextAnchor.MiddleCenter);
                    //  tiaozi.SetText(0, "弹开!");
                    tiaozi.ShowSpriteContent(CTiaoZiManager.s_Instance.GetSprContentByType(CTiaoZiManager.eTiaoZiSprContentType.tankai));
                    tiaozi.SetTextColor(0, Color.white);
                    tiaozi.SetTitleSprVisible(false);
                    tiaozi.SetParams_Type4(CTiaoZiManager.s_Instance.m_fShengJi_ChiXuTime);
                    _ball.SetTiaoZiTime(Ball.eTiaoZiType.e_tankai, Time.time);


                }
            }
        
           
        }




    }

    void OnTriggerExit2D(Collider2D other)
    {
      

    }

}
